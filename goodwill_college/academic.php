<?php include('include/session.php');?>   
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Goodwill college |Academic</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

    <!-- Font awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">   
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">          
    <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

    <!-- Main style sheet -->
    <link href="assets/css/style.css" rel="stylesheet">    

   
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>

  <?php include('include/header.php'); ?>
  
  <!-- Hero-area -->
		<div class="hero-area section">

			<!-- Backgound Image -->
			<div class="bg-image bg-parallax overlay" style="background-image:url(assets/img/counter-bg.jpg)"></div>
			<!-- /Backgound Image -->

			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1 text-center">
						<ul class="hero-area-tree">
							<li><a href="index.php">Home</a></li>
							<li>Academic Calendar</li>
						</ul>
						<h1 class="white-text">Academic Calendar</h2>

					</div>
				</div>
			</div>

		</div>
		<!-- /Hero-area -->

  

<!--<div class="parallax">dsaasdsda</div>
-->
 <!-- End breadcrumb -->
 <section id="mu-course-content">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
              <div class="col-md-9">
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                        <figure class="mu-latest-course-img">
                          <a href="#"><img src="images/seminar.jpg" alt="img"></a>
                         
                        </figure>
                        
                        <div class="mu-title">
                      <h2> Academic Calendar</h2>
                      
                                            </div>
                       
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
              <div class="col-md-3">
                <!-- start sidebar -->
                <aside class="mu-sidebar">
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Quick Links</h3>
                  <ul class="mu-sidebar-catg">
                  <li><a href="depart.php">Departments</a></li>
    <li><a href="faculty.php">Faculty Details</a></li>
    <li><a href="academic.php">Academic Calendar</a></li>
    <li><a href="result.php">RESULTS</a></li>     
</ul>  
                  </div>
                  <!-- end single sidebar -->
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Latest News</h3>
                    <div class="mu-sidebar-popular-courses">
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/1.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Christmas Day</a></h4>                      
                        
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/2.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Annual day</a></h4>                      
                          
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/3.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Students showcasing talent</a></h4>                      
                         
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end single sidebar -->
                 
                  
                </aside>
                <!-- / end sidebar -->
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
 
 
 
  <section>
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
         
          <div class="mu-latest-course-single">
           <div class="mu-title">
         
       
        
   <table class="table table-striped">
<tbody>
<tr>
<td valign="top" ><p><b>JUNE 1</b></p></td>
<td valign="top" ><p><b>Friday</b></p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 2</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 3</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 4</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 5</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 6</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 7</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 8</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 9</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 10</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 11</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 12</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 13</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 14</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 15</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 16</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>RAMZAN</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 17</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 18</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 19</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 20</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 21</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 22</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 23</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 24</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 25</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 26</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 27</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>COMMENCEMENT OF I,III,V SEMESTER CLASSES</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 28</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 29</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JUNE 30</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 1</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 2</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 3</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 4</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 5</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 6</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 7</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 8</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 9</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 10</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 11</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>BRIDGE COURSE BCA/BSC CHEMISTRY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 12</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>BRIDGE COURSE BCA/BSC /WORKSHOP – DEPARTMENT OF JOURNALISM</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 13</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>BRIDGE COURSE BCA/BSC </p></td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 14</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 15</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 16</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>WORKSHOP – DEPARTMENT OF CHEMISTRY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 17</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" ><p>GUEST LECTURE – DEPARTMENT OF PSYCHOLOGY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 18</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>TALENTS WEEK DAY 1</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 19</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>TALENTS WEEK DAY 2</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 20</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>TALENTS WEEK DAY 3</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 21</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>FRESHERS DAY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 22</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 23</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>FDP – DEPARTMENT OF BUSNIESS ADMINISTRATION /NOMINATIONS</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 24</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" ><p>CLASS PPT -MICROBIOLOGY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 25</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>OUTREACH PROGRAM- DEPARTMENT OF ENGLISH </p></td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 26</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>MINI PROJECT –BSC MICROBIOLOGY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 27</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>ELECTION</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 28</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>ALUMNI MEET</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 29</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 30</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>GUEST LECTURE DEPARTMENT OF CHEMISTRY/CERTIFICATE COURSE-DEPARTMENT OF BOTANY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JULY 31</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" ><p>SHORTAGE OF ATTENDANCE/ SUBMISSION OFWORK RECORD</p></td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 1 </b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>INDUSTRIAL VISIT-DEPARTMENT OF ENGLISH</p></td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 2</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 3</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>INDUSTRIAL VISIT-DEPARTMENT OF COMMERCE</p></td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 4</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 5</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 6</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>GUEST LECTURE-DEPARTMENT OF TAMIL</p></td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 7</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 8 </b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>INDUSTRIAL VISIT –DEPARTMENT OF BUSINESS ADMINSTRATION</p></td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 9</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>ARTS-CLUB ACTIVITY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 10</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>RELEASE OF NEWSLETTER-DEPARTMENT OF MICROBIOLOGY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 11</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 12</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 13</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>FIELD WORK-DEPARTMENT OF MICROBIOLOGY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 14</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 15</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>INDEPENDENCE DAY-HOLIDAY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 16 </b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 17</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>CLASS PPT FOR STUDENTS- DEPARTMENT OF CHEMISTRY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 18</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 19</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 20</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>GUEST LECTURE-DEPARTMENT OF KANNADA/URDU/PHOTOGRAPHY DAY-DEPARTMENT OF JOURNALISM</p></td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 21</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 22</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>FDP(GST)-DEPARTMENT OF COMMERCE</p></td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 23</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 24</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>MINI PROJECT-DEPARTMENT OF CHEMISTRY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 25</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 26</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 27</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>TEST 1</p></td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 28</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" ><p>TEST 1 / GROUP DISCUSSION- DEPARTMENT OF CHEMISTRY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 29</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>TEST 1</p></td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 30</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>TEST 1</p></td>
</tr>

<tr>
<td valign="top" ><p><b>AUGUST 31</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>TEST 1 /SHORTAGE OF ATTENDANCE/SUBMISSION OF WORK RECORD</p></td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 1</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 2</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 3</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>DEPARTMENT OF COMMERCE INTER COLLEGEIATE FEST</p></td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 4</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" ><p>ONE DAY TRIP-DEPARTMENT OF URDU/GUEST LECTURE –DEPARTMENT OF ECONOMICS</p></td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 5</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 6</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>BOTANICAL TRIP</p></td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 7</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>ONE DAY TRIP-DEPARTMENT OF KANNADA</p></td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 8</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 9</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 10</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>ONE DAY TRIP-DEPARTMENT OF TAMIL</p></td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 11</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" ><p>INDUSTRIAL VISIT-DEPARTMENT OF JOURNALISM/GUEST LECTURE-DEPARTMENT OF BUSINESS ADMINSTRATION</p></td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 12 </b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>SEMINAR-DEPARTMENT OF ENGLISH</p></td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 13</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 14</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 15</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 16</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 17</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>INDUSTRIAL VISIT-DEPARTMENT OF CHEMISTRY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 18</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" ><p>SEMINAR ON CURRENT TECHNOLOGIES- DEPARTMENT OF COMPUTER SCIENCE</p></td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 19</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>UNDERSTANDING YOUR MIND DEPARTMENT OF PSYCHOLOGY D-1</p></td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 20</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>UNDERSTANDING YOUR MIND DEPARTMENT OF PSYCHOLOGY D-2</p></td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 21</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>UNDERSTANDING YOUR MIND DEPARTMENT OF PSYCHOLOGY D-3</p></td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 22</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>PARENT TEACHERS MEET-DEPARTMENT OF COMMERCE</p></td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 23</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 24</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>TEST 2</p></td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 25</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" ><p>TEST 2</p></td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 26</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>TEST 2 /OUT REACH PROGRAM –III B.COM</p></td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 27</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>TEST 2</p></td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 28</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>TEST 2 /INDUSTRIAL VISIT –DEPARTMENT OF PSYCHOLOGY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 29</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>SHORTAGE OF ATTENDANCE/SUBMISSION OF WORK RECORD</p></td>
</tr>

<tr>
<td valign="top" ><p><b>SEPTEMBER 30</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 1</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>BHASHAPOORTY- DEPARTMENT OF LANGUAGES</p></td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 2</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" ><p>GANDHI JAYANTHI- HOLIDAY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 3 </b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>GUEST LECTURE- DEPARTMENT OF ENGLISH</p></td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 4</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 5</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 6</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>PARENT TEACHERS MEET –DEPARTMENT OF SCIENCE</p></td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 7</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 8</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>MAHALAYA AMAVASYA-HOLIDAY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 9</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 10</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>GUEST LECTURE-DEPARTMENT OF HINDI/WORK SHOP DEPARTMENT OF COMPUTER SCIENCE</p></td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 11</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 12</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 13</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>PARENT TEACHERS MEET –DEPARTMENT OF BUSINESS ADMINSTRATION</p></td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 14</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 15</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 16</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 17</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>


<tr>
<td valign="top" ><p><b>OCTOBER 18</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>AYUDHA POOJA-HOLIDAY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 19</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>VIJAYA DASHAMI-HOLIDAY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 20</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>PARENT TEACHERS MEET DEPARTMENT OF ARTS</p></td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 21</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 22</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 23 </b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 24</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>MAHARSHI VALMIKI JAYANTHI-HOLIDAY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 25</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 26 </b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>INDUSTRIAL VISIT-DEPARTMENT OF MICROBIOLOGY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 27 </b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>PARENT TEACHERS MEET-DEPARTMENT OF COMPUTER SCIENCE</p></td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 28</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 29</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 30</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>OCTOBER 31</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>SHORT AGE OFATTENDANCE/SUBMISSION OF WORK RECORD</p></td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 1</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>RAJYOTSAVA -HOLIDAY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 2</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>EVENTS FOR RAJYOTSAVA CELEBRATIONS D-1</p></td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 3</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>EVENTS FOR RAJYOTSAVA CELEBRATIONS D-2</p></td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 4</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 5 </b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>RAJYOTSAVA CELEBRATION</p></td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 6</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 7</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>NARAKA CHATURDASI</p></td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 8</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>LAST WORKING DAY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 9</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 10 </b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 11</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 12</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>PRACTICAL EXAMINATION BEGINS</p></td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 13</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 14</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 15</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 16</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 17 </b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 18</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 19</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>LAST DATE FOR INTERNAL ASSESMENT/THEORY EXAMINATION BEGINS</p></td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 20</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 21</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>ID-MEELAD</p></td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 22</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 23</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 24</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>COMMENCEMENT OF VALUATION</p></td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 25</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 26</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>KANAKADASA JAYANTHI- HOLIDAY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 27</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 28</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 29</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>NOVEMBER 30</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 1 </b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 2</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 3</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 4</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 5</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 6</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 7</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 8</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 9</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 10</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 11</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 12</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 13</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 14</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 15</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 16</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 17</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 18</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 19</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 20</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 21</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>CHRISTMAS PROGRAM</p></td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 22</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 23</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 24</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 25</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" ><p>CHRISTMAS</p></td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 26</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>COMMENCEMENT OF II,IV,VI SEMESTER CLASSES</p></td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 27</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 28</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 29</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 30</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>DECEMBER 31</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 1</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 2</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 3</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 4</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 5</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 6</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 7</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 8</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 9</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 10</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>SEMINAR-DEPARTMENT OF LANGUAGES</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 11</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 12</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>NATIONAL YOUTH CELEBRATIONS-DEPARTMENT OF JOURNALISM</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 13</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 14</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>PONGAL/SANKRANTHI-HOLIDAY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 15</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" ><p>EVENTS FOR TAMIL DAY CELEBRATIONS D-1</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 16</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>ONE DAY TRIP- DEPARTMENT OF TAMIL</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 17</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 18</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 19</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>SPORTS DAY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 20</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 21</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 22</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 23</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>CERTIFICATE COURSE BEGINS FOR DEPARTMENT OF COMPUTER SCIENCE</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 24</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 25</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>STUDENT CHRISTIAN MOVEMENT-INTERCOLLEGIATE FEST</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 26</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>REPUBLIC DAY HOLIDAY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 27</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 28</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 29</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 30</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>JANUARY 31</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>SHORT AGE OF ATTENDANCE OF ATTENDANCE /SUBMISSION OF WORK RECORD </p></td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 1 </b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 2</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>ALUMNI MEET</p></td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 3</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 4</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>WORK SHOP-DEPARTMENT OF PSYCHOLOGY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 5</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 6 </b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 7</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>EVENTS FOR URDU DAY CELEBRATION D-1</p></td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 8</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>EVENTS FOR URDU DAY CELEBRATION D-2</p></td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 9</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>URDU CELEBRATION</p></td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 10</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 11</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>CURA –CLUB ACTIVITY DEPARTMENT OF BUSINESS ADMINSTRATION</p></td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 12</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 13</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>WORLD RADIO DAY----PPT-DEPARTMENT OF JOURNALISM</p></td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 14 </b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>ONE DAY TRIP- DEPARTMENT OF LANGUAGES</p></td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 15</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>WORK SHOP-DECCAN HERALD DEPARTMENT OF JOURNALISM D-1</p></td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 16</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>WORK SHOP-DECCAN HERALD DEPARTMENT OF JOURNALISM D-2</p></td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 17</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 18</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>TEST 1</p></td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 19 </b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" ><p>TEST 1 /HINDI SEMINAR FOR STUDENTS</p></td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 20</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>TEST1 /GUEST LECTURE- DEPARTMENT OF BUSINESS ADMINSTRATION</p></td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 21</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>TEST 1</p></td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 22</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>TEST 1</p></td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 23</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 24</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 25</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 26</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 27</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>GUEST LECTURE- DEPARTMENT OF SCIENCE</p></td>
</tr>

<tr>
<td valign="top" ><p><b>FEBRUARY 28</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>SCIENCE DAY CELEBRATION / SHORT AGE OF ATTENDANCE/ SUBMISSION OF WORK RECORD</p></td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 1</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 2</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>WORK SHOP- DEPARTMENT OF COMPUTER SCIENCE</p></td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 3</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 4</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>MAHASHIVRATI -HOLIDAY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 5</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 6 </b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>CLASS PPT-DEPARTMENT OF CHEMISTRY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 7</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 8</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 9</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>PARENTS TEACHERS MEET –DEPARTMENT OF COMPUTER SCIENCE</p></td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 10</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 11</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>TEST 2 /INDUSTRIAL VIST- DEPARTMENT OD BOTANY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 12</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" ><p>TEST 2</p></td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 13</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>TEST 2</p></td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 14</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>TEST 2 /QUIZ COMPETITION- DEPARTMENT OF JOURNALISM</p></td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 15</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>TEST 2</p></td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 16</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>PARENTS TEACHERS MEET –DEPARTMENTS OF SCIENCE/ARTS</p></td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 17</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 18</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>MINI PROJECT-DEPARTMENT OF CHEMISTRY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 19</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 20</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>CAREER GUIDANCE FOR BSC FINAL YEAR</p></td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 21</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>GROUP DISCUSSION- DEPARTMENT OF CHEMISTRY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 22</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>ANNUAL PRIZE DAY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 23</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>PARENTS TEACHERS MEET-DEPARTMENT OF BUSINESS ADMINSTRAION</p></td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 24</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 25</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>CAREER GUIDANCE FOR BSC- CHEMISTRY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 26</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 27</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>FARWEELL FOR OUTGOING STUDENTS</p></td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 28</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 29</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 30</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>CERTIFICATE COURSE ENDS FOR DEPARTMENT OF COMPUTER SCIENCE/SHORT AGE OF ATTENDANCE /SUBMISSION OF WORK RECORD</p></td>
</tr>

<tr>
<td valign="top" ><p><b>MARCH 31</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 1</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>REMEDIAL CLASSES FOR CHEMISTRY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 2</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 3</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>GRADUATION</p></td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 4</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 5</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 6</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>UGADI- HOLIDAY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 7</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 8</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 9</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 10</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 11</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>EVENTS FOR AMBEDKAR STUDT CENTRE D-1</p></td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 12</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>EVENTS FOR AMBEDKAR STUDY CENTRE D-2</p></td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 13</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" ><p>EVENTS FOR AMBEDKAR STUDY CENTER D- 3/PARENTS TEACHERS MEET –DEPARTMENT OF COMMERCE</p></td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 14</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" ><p>AMBEDKAR JAYANTHI</p></td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 15</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" ><p>AMBEDKAR STUDY CENTRE PROGRAM</p></td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 16</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" ><p>BASAVA JAYANTHI HOLIDAY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 17</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" ><p>MAHAVIR JAYANTHI</p></td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 18</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>LAST WORKING DAY/ PRACTICAL EXAMINATION BEGINS </p></td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 19</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" ><p>GOOD FRIDAY</p></td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 20</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 21</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 22</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 23</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 24</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 25</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" ><p>LAST DATE FOR INTERNAL ASSESMENT/THEORY EXAMINATION BEGINS</p></td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 26</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 27</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 28</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 29</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>APRIL 30</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 1</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 2</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 3</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 4</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 5</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 6</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 7</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 8</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 9</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 10</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 11</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 12</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 13</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 14</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 15</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 16</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 17</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 18</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 19</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 20</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 21</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 22</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 23</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 24</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 25</b></p></td>
<td valign="top" ><p>Saturday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 26</b></p></td>
<td valign="top" ><p>Sunday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 27</b></p></td>
<td valign="top" ><p>Monday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 28</b></p></td>
<td valign="top" ><p>Tuesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 29</b></p></td>
<td valign="top" ><p>Wednesday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 30</b></p></td>
<td valign="top" ><p>Thursday</p></td>
<td valign="top" > </td>
</tr>

<tr>
<td valign="top" ><p><b>MAY 31</b></p></td>
<td valign="top" ><p>Friday </p></td>
<td valign="top" > </td>
</tr>

</tbody>
</table>



</div>

</div>
           
         </div>
       </div>
     </div>
   </div>
 </section>
 
 
 

 <?php include('include/footer.php'); ?>
  
  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>  
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>   
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>  

  <!-- Custom js -->
  <script src="assets/js/custom.js"></script> 

  </body>
</html>