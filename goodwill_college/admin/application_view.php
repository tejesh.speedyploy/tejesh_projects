<?php include('include/session.php');
if (!isset($_SESSION['admin_user'])) {
  header("Location: login.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Goodwill college |Application</title>

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

  <!-- Font awesome -->
  <link href="assets/css/font-awesome.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <!-- Slick slider -->
  <link rel="stylesheet" type="text/css" href="assets/css/slick.css">
  <!-- Theme color -->
  <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

  <!-- Main style sheet -->
  <link href="../assets/css/style.css" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
  <style>
    #targetLayer {
      float: left;
      width: 150px;
      height: 150px;
      text-align: center;
      line-height: 150px;
      font-weight: bold;
      color: #C0C0C0;
      background-color: #F0E8E0;
      border-bottom-left-radius: 4px;
      border-top-left-radius: 4px;
    }

    #uploadFormLayer {
      float: left;
      padding: 20px;
    }
    @media print { 
               .noprint { 
                  visibility: hidden; 
               } 
            } 
  </style>


  <!-- Google Fonts -->
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

  <?php include('include/header.php'); ?>
  <!-- End breadcrumb -->
  <section id="mu-course-content">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="mu-course-content-area">
            <div class="row">
              <div class="col-md-9">
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                        <figure class="mu-latest-course-img">
                          <a href="#"><img src="images/application.jpg" alt="img"></a>

                        </figure>
                        <div class="mu-title">
                          <h2>Application Form</h2>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
                <!-- end course content container -->



              </div>
              <div class="col-md-3">
                <!-- start sidebar -->
                <aside class="mu-sidebar">
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Quick Links</h3>
                    <ul class="mu-sidebar-catg">
                      <li><a href="course.php">Courses</a></li>
                      <li><a href="application.php">Application Form</a></li>
                      <li><a href="procedure.php">Rules & Regulations</a></li>
                      <li><a href="scholarship.php">Scholarship</a></li>
                    </ul>
                  </div>
                  <!-- end single sidebar -->
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Latest News</h3>
                    <div class="mu-sidebar-popular-courses">
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/1.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Christmas Day</a></h4>

                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/2.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Annual day</a></h4>

                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/3.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Students showcasing talent</a></h4>

                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end single sidebar -->


                </aside>
                <!-- / end sidebar -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>




  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="mu-course-content-area">

            <div class="row">


              <div class="col-md-12">

                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">


                        <div class="mu-latest-course-single-content" id="application_view">




                          <form class="form-horizontal" method="post" action="#" id="application_form" name="application_form" role="form" enctype="multipart/form-data">

                            <!--- Image Header Name---->
                            <div class="form-group">

                              <img src="images/admissionheader.jpg">

                            </div>
                            <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">Note:</label>
                              <div class="col-sm-9">
                                <label for="Note">
                                  <font color="black"><b>All Fields Marked with </b> </font>
                                  <font color="red"><b>* </b></font>
                                  <font color="black"><b>are mandatory to fill-in or application won't be considered/registered with us!!!! </b> <b> Only We accept .jpg,.png,.bmp,.jpeg file extensions for all uploads
                                </label>
                              </div>
                            </div>
                            <span id="reset_error" style="display: none; color:red;"></span>
                            <span id="reset_message" style="display: none; color:#17B6BB;"></span>
                            <?php
                            ini_set('display_errors', 1);
                            ini_set('display_startup_errors', 1);
                            error_reporting(E_ALL);
                            require_once "functions/config.php";
                            //print_r($_POST['email']);
                            $student_email = mysqli_real_escape_string($conn,$_POST['email']);
                            $sql = "SELECT * FROM application WHERE student_email = '$student_email'";
                            $result = mysqli_query($conn, $sql);
                            $count = mysqli_num_rows($result);

                            if ($count == 1) {
                              while ($row = mysqli_fetch_assoc($result)) {
                                //echo "Name: " . $row["first_name"]. "<br>";
                                $application_id = $row['application_id'];
                                $student_email = $row['student_email'];
                                $student_name = $row['student_name'];
                                $admission_year = $row['admission_year'];
                                $father_name = $row['father_name'];
                                $guardian_name = $row['guardian_name'];
                                $mother_name = $row['mother_name'];
                                $father_occupation = $row['father_occupation'];
                                $mother_occupation = $row['mother_occupation'];
                                $guardian_occupation = $row['guardian_occupation'];
                                $father_email = $row['father_email'];
                                $mother_email = $row['mother_email'];
                                $guardian_email = $row['guardian_email'];
                                $father_mobile = $row['father_mobile'];
                                $mother_mobile = $row['mother_mobile'];
                                $guardian_mobile = $row['guardian_mobile'];
                                $father_telephone = $row['father_telephone'];
                                $mother_telephone = $row['mother_telephone'];
                                $guardian_telephone = $row['guardian_telephone'];
                                $father_annual_income = $row['father_annual_income'];
                                $mother_annual_income = $row['mother_annual_income'];
                                $guardian_annual_income = $row['guardian_annual_income'];
                                $father_office_address = $row['father_office_address'];
                                $mother_office_address = $row['mother_office_address'];
                                $guardian_office_address = $row['guardian_office_address'];
                                $current_address = $row['current_address'];
                                $permanent_address = $row['permanent_address'];
                                $date_of_birth = $row['date_of_birth'];
                                $nationality = $row['nationality'];
                                $religion = $row['religion'];
                                $christian_denomination = $row['christian_denomination'];
                                $category = $row['category'];
                                $course = $row['course'];
                                $majors = $row['majors'];
                                $second_language = $row['second_language'];
                                $examination_passed = $row['examination_passed'];
                                $subject_1 = $row['subject_1'];
                                $subject_2 = $row['subject_2'];
                                $subject_3 = $row['subject_3'];
                                $subject_4 = $row['subject_4'];
                                $subject_5 = $row['subject_5'];
                                $subject_6 = $row['subject_6'];
                                $max_marks_1 = $row['max_marks_1'];
                                $max_marks_2 = $row['max_marks_2'];
                                $max_marks_3 = $row['max_marks_3'];
                                $max_marks_4 = $row['max_marks_4'];
                                $max_marks_5 = $row['max_marks_5'];
                                $max_marks_6 = $row['max_marks_6'];
                                $marks_1 = $row['marks_1'];
                                $marks_2 = $row['marks_2'];
                                $marks_3 = $row['marks_3'];
                                $marks_4 = $row['marks_4'];
                                $marks_5 = $row['marks_5'];
                                $marks_6 = $row['marks_6'];
                                $total_max_marks = $row['total_max_marks'];
                                $total_marks = $row['total_marks'];
                                $sport_name_1 = $row['sport_name_1'];
                                $sport_name_2 = $row['sport_name_2'];
                                $sport_name_3 = $row['sport_name_3'];
                                $sport_level_1 = $row['sport_level_1'];
                                $sport_level_2 = $row['sport_level_2'];
                                $sport_level_3 = $row['sport_level_3'];
                                $sport_cert_1_path = $row['sport_cert_1_path'];
                                $sport_cert_2_path = $row['sport_cert_2_path'];
                                $sport_cert_3_path = $row['sport_cert_3_path'];
                                $scm_activity = $row['scm_activity'];
                                $choir_activity = $row['choir_activity'];
                                $civil_defense_activity = $row['civil_defense_activity'];
                                $nss_defense_activity = $row['nss_defense_activity'];
                                $music_activity = $row['music_activity'];
                                $sports_activity = $row['sports_activity'];
                                $previous_institution_name = $row['previous_institution_name'];
                                $previous_institution_address = $row['previous_institution_address'];
                                $year_of_passing = $row['year_of_passing'];
                                $self_declaration_name = $row['self_declaration_name'];
                                $place_of_submission = $row['place_of_submission'];
                                $profile_picture_path = $row['profile_picture_path'];
                                $father_signature_picture_path = $row['father_signature_picture_path'];
                                $mother_signature_picture_path = $row['mother_signature_picture_path'];
                                $guardian_signature_picture_path = $row['guardian_signature_picture_path'];
                                $student_signature_picture_path = $row['student_signature_picture_path'];
                                $marks_card_picture_path = $row['marks_card_picture_path'];
                                $transfer_certificate_picture_path = $row['transfer_certificate_picture_path'];
                                $obc_certificate_picture_path = $row['obc_certificate_picture_path'];
                                $foreign_eligible_picture_path = $row['foreign_eligible_picture_path'];
                                $csi_membership_picture_path = $row['csi_membership_picture_path'];
                                $application_status = $row['application_status'];
                                $date = $row['date'];
                                //print_r($_SESSION);
                              }
                            }
                            else{
                              echo "Please contact System Administrator and looks like there is some corruption in database";
                            }
                            //mysqli_close($conn);
                            ?>


                            <h3 class="center" style="color: #008000; font-style:normal;">Important Details</h3>
                            <!--- Upload Photo---->
                            <div class="form-group">

                              <label for="Admission" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Student Profile photo:
                              </label>
                              <div class="col-sm-3 noprint">
                              <a href="../<?php echo $profile_picture_path; ?>" target="_blank"><button type="button"><i class="fa fa-download"></i> Download</button></a>
                              </div>

                              <div id="targetLayer"><img height="150" width="150" id="output" src="../<?php echo $profile_picture_path; ?>" />
                              
                            </div>
                              
                            </div>


                            <!--- Application No---->
                            <div class="form-group">


                              <label for="Admission" class="col-sm-3 control-label">Year of Admission:</label>
                              <div class="col-sm-3">

                                <input type="text" id="admission_year" name="admission_year" value="<?php echo $admission_year;  ?>" class="form-control" readonly="readonly">
                              </div>
                              <div class="col-sm-6">
                                <label for="ApplicationNo" class="col-sm-6 control-label">Application No:</label>
                                <div class="col-sm-3">
                                  <input type="text" id="application_number" name="application_number" value="<?php echo $application_id; ?>" class="form-control" readonly="readonly">
                                  
                                </div>

                              </div>
                            </div>


                            <div class="form-group">

                              <!--- Student Name---->

                              <label for="StudentName" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Student's Name:
                              </label>
                              <div class="col-sm-9">
                                <span class="help-block">(As per certificate in BLOCK LETTERS)</span>
                                <input type="text" id="StudentName" name="StudentName" placeholder="Student's Name" class="form-control" onkeyup="this.value = this.value.toUpperCase();" readonly="readonly" value="<?php echo $student_name; ?>">

                              </div>
                            </div>
                            <div class="form-group">

                              <label for="AnnualIncome" class="col-sm-3 control-label">Student Email:</label>
                              <div class="col-sm-9">

                                <input type="email" class="form-control" name="StudentEmail" id="StudentEmail" placeholder="Enter Email" readonly="readonly" value="<?php echo $student_email; ?>">

                              </div>
                            </div>

                            <div class="form-group">

                              <label for="AnnualIncome" class="col-sm-3 control-label">Application Status:</label>
                              <div class="col-sm-9">

                                <input type="email" class="form-control" name="ApplicationStatus" id="ApplicationStatus" placeholder="Status" readonly="readonly" value="<?php echo $application_status; ?>">

                              </div>
                            </div>


                            <hr style="border:3px solid #eee" class="separate">
                            <h3 class="center" style="color: #008000; font-style:normal;">Father Details</h3>
                            <font color="red"><b id="parental_detail"></b></font>
                            <font color="red"><b id="name_error"></b></font>
                            <font color="red"><b id="occupation_error"></b></font>
                            <font color="red"><b id="annual_income_error"></b></font>
                            <font color="red"><b id="mobile_error"></b></font>


                            <!--- Fathers Name---->
                            <div class="form-group">

                              <label for="FathersName" class="col-sm-3 control-label">Father's Name:</label>
                              <div class="col-sm-9">
                                <span class="help-block">(In BLOCK LETTERS)</span>
                                <input type="text" id="FathersName" name="FathersName" placeholder="Father's Name" class="form-control" onkeyup="this.value = this.value.toUpperCase();" readonly="readonly" value="<?php echo $father_name; ?>">
                                <font color="red"><b id="father_name_error"></b></font>
                              </div>
                            </div>





                            <!--- Father's Address---->
                            <div class="form-group">
                              <label for="Occupation" class="col-sm-3 control-label">Occupation:</label>
                              <div class="col-sm-9">

                                <input type="text" id="FatherOccupation" name="FatherOccupation" placeholder="Occupation" class="form-control" readonly="readonly" value="<?php echo $father_occupation; ?>">

                              </div>

                            </div>
                            <div class="form-group">

                              <label for="Office Address" class="col-sm-3 control-label">Office Address:</label>
                              <div class="col-sm-9">

                                <textarea class="form-control" id="FatherOfficeAddress" name="FatherOfficeAddress" rows="3" placeholder="Do not exceed more than 160 characters" maxlength="160" title="Only 160 Characters!!!" readonly="readonly" ><?php echo $father_office_address; ?></textarea>

                              </div>

                            </div>


                            <!--- Annual Income---->
                            <div class="form-group">

                              <label for="AnnualIncome" class="col-sm-3 control-label">Annual Income:</label>
                              <div class="col-sm-9">

                                <input type="number" id="FatherAnnualIncome" name="FatherAnnualIncome" placeholder="Annual Income" max="999999999" Title="Only Numbers & in rupees only" class="form-control" readonly="readonly" value="<?php echo $father_annual_income; ?>">

                              </div>
                            </div>
                            <div class="form-group">

                              <label for="AnnualIncome" class="col-sm-3 control-label">Email:</label>
                              <div class="col-sm-9">

                                <input type="email" class="form-control" name="FatherEmail" id="FatherEmail" placeholder="Enter Email" readonly="readonly" value="<?php echo $father_email; ?>">

                              </div>
                            </div>




                            <!--- Mobile No---->
                            <div class="form-group">




                              <label for="MobileNo" class="col-sm-3 control-label">Mobile No:</label>
                              <div class="col-sm-3">

                                <input type="text" id="FatherMobileNo" name="FatherMobileNo" placeholder="Mobile No" class="form-control" pattern="(6|7|8|9)\d{9}" title="Mobile number should only contain 10 numbers & starting with 6,7,8,9 eg:6123456789" readonly="readonly" value="<?php echo $father_mobile; ?>">

                              </div>



                              <div class="col-sm-6">
                                <label for="TelephoneNo" class="col-sm-3 control-label">Tel No (Office):</label>
                                <div class="col-sm-9">

                                  <input type="text" id="FatherTelNo" name="FatherTelNo" placeholder="Telephone No" class="form-control" readonly="readonly" value="<?php echo $father_telephone; ?>">

                                </div>

                              </div>
                            </div>


                            <hr style="border:3px solid #eee">
                            <h3 class="center" style="color: #008000; font-style:normal;">Mother Details</h3>


                            <!--- Mothers Name---->
                            <div class="form-group">

                              <label for="MothersName" class="col-sm-3 control-label">Mother's Name:</label>
                              <div class="col-sm-9">
                                <span class="help-block">(In BLOCK LETTERS)</span>
                                <input type="text" id="MothersName" name="MothersName" placeholder="Mother's Name" class="form-control" onkeyup="this.value = this.value.toUpperCase();" readonly="readonly" value="<?php echo $mother_name; ?>">
                                <font color="red"><b id="mother_name_error"></b></font>
                              </div>
                            </div>






                            <!---Address---->
                            <div class="form-group">
                              <label for="Occupation" class="col-sm-3 control-label">Occupation:</label>
                              <div class="col-sm-9">

                                <input type="text" id="MotherOccupation" name="MotherOccupation" placeholder="Occupation" class="form-control" readonly="readonly" value="<?php echo $mother_occupation; ?>">

                              </div>

                            </div>

                            <div class="form-group">
                              <label for="Office Address" class="col-sm-3 control-label">Office Address:</label>
                              <div class="col-sm-9">
                                <textarea class="form-control" id="MotherOfficeAddress" name="MotherOfficeAddress" rows="3" placeholder="Do not exceed more than 160 characters" maxlength="160" title="Only 160 Characters!!!"readonly="readonly"><?php echo $mother_office_address; ?></textarea>
                              </div>
                            </div>



                            <!--- Annual Income---->
                            <div class="form-group">

                              <label for="AnnualIncome" class="col-sm-3 control-label">Annual Income:</label>
                              <div class="col-sm-9">

                                <input type="text" id="MotherAnnualIncome" name="MotherAnnualIncome" placeholder="Annual Income" class="form-control" readonly="readonly" value="<?php echo $mother_annual_income; ?>">

                              </div>
                            </div>

                            <div class="form-group">

                              <label for="AnnualIncome" class="col-sm-3 control-label">Email:</label>
                              <div class="col-sm-9">

                                <input type="email" class="form-control" name="MotherEmail" id="MotherEmail" placeholder="Enter Email" readonly="readonly" value="<?php echo $mother_email; ?>">

                              </div>
                            </div>


                            <!--- Mobile No---->
                            <div class="form-group">

                              <label for="MobileNo" class="col-sm-3 control-label">Mobile No:</label>
                              <div class="col-sm-3">

                                <input type="text" id="MotherMobileNo" name="MotherMobileNo" placeholder="Mobile No" class="form-control" pattern="(6|7|8|9)\d{9}" title="Mobile number should only contain 10 numbers & starting with 6,7,8,9 eg:6123456789" readonly="readonly" value="<?php echo $mother_mobile; ?>">

                              </div>

                              <div class="col-sm-6">
                                <label for="TelephoneNo" class="col-sm-3 control-label">Tel No (Office):</label>
                                <div class="col-sm-9">

                                  <input type="text" id="MotherTelNo" name="MotherTelNo" placeholder="Telephone No" class="form-control" readonly="readonly" value="<?php echo $mother_telephone; ?>">

                                </div>

                              </div>
                            </div>


                            <hr style="border:3px solid #eee">

                            <h3 class="center" style="color: #008000; font-style:normal;">Guardian Details</h3>

                            <!--- Guardians Name---->
                            <div class="form-group">

                              <label for="GuardiansName" class="col-sm-3 control-label">Guardian's Name:</label>
                              <div class="col-sm-9">
                                <span class="help-block">(In BLOCK LETTERS)</span>
                                <input type="text" id="GuardiansName" name="GuardiansName" placeholder="Guardian's Name" class="form-control" onkeyup="this.value = this.value.toUpperCase();" readonly="readonly" value="<?php echo $guardian_name; ?>">
                                <font color="red"><b id="guardian_name_error"></b></font>
                              </div>
                            </div>




                            <div class="form-group">
                              <label for="Occupation" class="col-sm-3 control-label">Occupation:</label>
                              <div class="col-sm-9">

                                <input type="text" id="GuardianOccupation" name="GuardianOccupation" placeholder="Occupation" class="form-control" readonly="readonly" value="<?php echo $guardian_occupation; ?>">

                              </div>

                            </div>

                            <!---Address---->
                            <div class="form-group">

                              <label for="Office Address" class="col-sm-3 control-label">Office Address:</label>
                              <div class="col-sm-9">

                                <textarea class="form-control" id="GuardianOfficeAddress" name="GuardianOfficeAddress" rows="3" placeholder="Do not exceed more than 160 characters" maxlength="160" title="Only 160 Characters!!!" readonly="readonly"><?php echo $guardian_office_address; ?></textarea>
                              </div>

                            </div>


                            <div class="form-group">

                              <label for="AnnualIncome" class="col-sm-3 control-label">Annual Income:</label>
                              <div class="col-sm-9">

                                <input type="text" id="GuardianAnnualIncome" name="GuardianAnnualIncome" placeholder="Annual Income" class="form-control" readonly="readonly" value="<?php echo $guardian_annual_income; ?>">

                              </div>
                            </div>

                            <div class="form-group">

                              <label for="AnnualIncome" class="col-sm-3 control-label">Email:</label>
                              <div class="col-sm-9">

                                <input type="email" class="form-control" name="GuardianEmail" id="GuardianEmail" placeholder="Enter Email" readonly="readonly" value="<?php echo $guardian_email; ?>">

                              </div>
                            </div>


                            <!--- Mobile No---->
                            <div class="form-group">

                              <label for="MobileNo" class="col-sm-3 control-label">Mobile No:</label>
                              <div class="col-sm-3">

                                <input type="text" id="GuardianMobileNo" name="GuardianMobileNo" placeholder="Mobile No" class="form-control" pattern="(6|7|8|9)\d{9}" title="Mobile number should only contain 10 numbers & starting with 6,7,8,9 eg:6123456789" readonly="readonly" value="<?php echo $guardian_mobile; ?>">

                              </div>

                              <div class="col-sm-6">
                                <label for="TelephoneNo" class="col-sm-3 control-label">Tel No (Office):</label>
                                <div class="col-sm-9">

                                  <input type="text" id="GuardianTelNo" name="GuardianTelNo" placeholder="Telephone No" class="form-control" readonly="readonly" value="<?php echo $guardian_telephone; ?>">

                                </div>

                              </div>
                            </div>

                            <hr style="border:3px solid #eee">
                            <h3 class="center" style="color: #008000; font-style:normal;">Student Details</h3>

                            <!---Address---->
                            <div class="form-group">

                              <label for="Address" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Current/Local Address:
                              </label>
                              <div class="col-sm-9">

                                <textarea class="form-control" id="CurrentAddress" name="CurrentAddress" rows="3" placeholder="Do not exceed more than 160 characters" maxlength="160" title="Only 160 Characters!!!" readonly="readonly"><?php echo $current_address; ?></textarea>
                              </div>

                            </div>
                            <div class="form-group">
                              <label for="Address" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Permanent Address:
                              </label>
                              <div class="col-sm-9">
                                <textarea class="form-control" id="PermanentAddress" name="PermanentAddress" rows="3" placeholder="Do not exceed more than 160 characters" maxlength="160" title="Only 160 Characters!!!" readonly="readonly"><?php echo $permanent_address; ?></textarea>
                              </div>
                            </div>

                            <!--- Student Date of Birth--->


                            <div class="form-group">
                              <label for="birthDate" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Date of Birth of Student(As per X Std Certified):
                              </label>
                              <div class="col-sm-9">
                                <div class='input-group date'>
                                  <input type='date' id="DOB" name="DOB" class="form-control" max="<?php echo date('Y-m-d', strtotime('-14 years')); ?>" readonly="readonly" value="<?php echo $date_of_birth; ?>">
                                </div>
                              </div>
                            </div>



                            <!--- Nationality Religion--->


                            <div class="form-group">
                              <label for="email" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Nationality:
                              </label>
                              <div class="col-sm-3">
                                <input type="Nationality" id="Nationality" name="Nationality" placeholder="Nationality" class="form-control" readonly="readonly" value="<?php echo $nationality; ?>">
                              </div>

                              <label for="email" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Religion:
                              </label>
                              <div class="col-sm-3">
                                <input type="Religion" id="Religion" name="Religion" placeholder="Religion" class="form-control" required readonly="readonly" value="<?php echo $religion; ?>">
                              </div>

                            </div>





                            <div class="form-group">
                              <label for="Denomination" class="col-sm-3 control-label">If Christian (mention Denomination):</label>
                              <div class="col-sm-9">
                                <input type="Denomination" id="Denomination" name="Denomination" placeholder="Denomination" class="form-control" readonly="readonly" value="<?php echo $christian_denomination; ?>">
                              </div>
                            </div>

                            <!--- f--->

                            <div class="form-group">
                              <label for="Category" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Category:
                              </label>
                              <div class="col-sm-9">
                                <select id="category" name="category" class="form-control" onchange='DisplayCategoryTextBox(this.value);' required readonly="readonly" disabled>
                                  <option selected value="<?php echo $category; ?>"><?php echo $category; ?></option>
                                  <option value="SC">SC</option>
                                  <option value="ST">ST</option>
                                  <option value="BC">BC</option>
                                  <option value="Others">Others</option>
                                  <option value="Category 1">Category 1</option>
                                  <option value="2A">2A</option>
                                  <option value="2B">2B</option>
                                  <option value="3A">3A</option>
                                  <option value="3B">3B</option>
                                </select>
                                <font color="red"><b id="category_other_text_error"></b></font>
                                <input type="text" name="category-textbox" id="category-textbox" style='display:none;' placeholder="Others, please specify here" onfocusout='CategoryOtherTextBox(this.value);' class="form-control" />
                              </div>
                            </div> <!-- /.form-group -->


                            <!-- /.Courses Offered -->


                            <div class="form-group">
                              <label for="Courses Offered" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Select your Course:
                              </label>
                              <div class="col-sm-9">
                                <select id="course-selector" name="course-selector" class="form-control" required readonly="readonly" disabled>
                                  <option selected value="<?php echo $course; ?>"><?php echo $course; ?></option>
                                  <option value="BA">B.A</option>
                                  <option value="BSc">B.Sc</option>
                                  <option value="BCom">B.Com</option>
                                  <option value="BBA">BBA</option>
                                  <option value="BCA">BCS</option>
                                </select>
                              </div>
                            </div>

                            <div class="form-group">

                              <label for="Courses Offered" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Select Major Subject:
                              </label>
                              <div class="col-sm-9">
                                <select id="majors-selector" name="majors-selector" class="form-control" required readonly="readonly" disabled>
                                <option selected value="<?php echo $majors; ?>"><?php echo $majors; ?></option>
                                </select>
                              </div>
                            </div>



                            <!--- second language--->

                            <div class="form-group">
                              <label for="Second Languages Offered" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Select your Second Language:
                              </label>
                              <div class="col-sm-9">
                                <select id="second-language-selector" name="second-language-selector" class="form-control" required readonly="readonly" disabled>
                                  <option selected value="<?php echo $second_language; ?>"><?php echo $second_language; ?> </option>
                                  <option value="Kannada">Kannada</option>
                                  <option value="Hindi">Hindi</option>
                                  <option value="Tamil">Tamil</option>
                                  <option value="Urdu">Urdu</option>
                                  <option value="Additional English">Additional English (only if you have previous study records)</option>
                                  <option value="French">French (only if you have previous study records)</option>
                                </select>
                                <font color="brown">Its good to select the previously studied languages for considering the application</font>
                              </div>
                            </div>


                            <hr style="border:3px solid #eee">
                            <h3 class="center" style="color: #008000; font-style:normal;">Student's Profile</h3>



                            <!--- Student Profile--->


                            <div class="form-group">
                              <label class="control-label col-sm-3">
                                <font color="red"><b>* </b></font>Examination Passed:
                              </label>
                              <div class="col-sm-9">
                                <select id="examination-passed" name="examination-passed" class="form-control" onchange='DisplayTextBox(this.value);' required readonly="readonly" disabled>
                                  <option selected value="<?php echo $examination_passed; ?>"><?php echo $examination_passed; ?></option>
                                  <option value="PUC">PUC</option>
                                  <option value="ISC">ISC</option>
                                  <option value="CBSE">CBSE</option>
                                  <option value="Others">Others</option>
                                </select>
                                <font color="red"><b id="exam_other_text_error"></b></font>
                                <input type="text" name="examination-passed-textbox" id="examination-passed-textbox" style='display:none;' placeholder="Others, please specify here" onfocusout="ExamOtherTextBox(this.value);" class="form-control" />
                              </div>
                            </div> <!-- /.form-group -->



                            <div class="form-group">
                              <label for="Marks Sheet" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Fill Your Marks:
                              </label>
                              <div class="col-sm-9">
                                <font color="red"><b id="marks_error"></b></font>

                                <!--<a class="btn btn-primary pull-right add-record" data-added="0"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add Subject</a>-->
                                <div style="display: none;">
                                  <table id="sample_table">
                                    <tr id="">
                                      <td> <input type="text" name="subject[]" id="subject[]" placeholder="Enter Subject Name" class="form-control" ></td>
                                      <td> <input type="number" name="MaxMarks[]" id="MaxMarks[]" placeholder="Enter Max Marks" class="form-control max_marks" max="125"></td>
                                      <td> <input type="number" name="MarksObtained[]" id="MarksObtained[]" placeholder="Enter Marks Obtained" class="form-control sub_marks" max="125"></td>
                                      <td><a class="btn btn-xs delete-record" data-id="0"><i class="glyphicon glyphicon-trash"></i></a></td>
                                    </tr>
                                  </table>
                                </div>
                                <table class="table table-bordered" id="tbl_posts">
                                  <thead>
                                    <tr>
                                      <th>Subject</th>
                                      <th>Max. Marks</th>
                                      <th>Marks Obtained</th>
                                    </tr>
                                    <tr id="marks_1">
                                      <td> <input type="text" name="subject_1" id="subject_1" placeholder="Enter Subject Name" class="form-control" readonly="readonly" value="<?php echo $subject_1; ?>"></td>
                                      <td> <input type="number" name="MaxMarks_1" id="MaxMarks_1" placeholder="Enter Max Marks" class="form-control max_marks" max="125" readonly="readonly" value="<?php echo $max_marks_1; ?>"></td>
                                      <td> <input type="number" name="MarksObtained_1" id="MarksObtained_1" placeholder="Enter Marks Obtained" class="form-control sub_marks" max="125" readonly="readonly" value="<?php echo $marks_1; ?>"></td>
                              
                                    </tr>
                                    <tr id="marks_2">
                                      <td> <input type="text" name="subject_2" id="subject_2" placeholder="Enter Subject Name" class="form-control" readonly="readonly" value="<?php echo $subject_2; ?>"></td>
                                      <td> <input type="number" name="MaxMarks_2" id="MaxMarks_2" placeholder="Enter Max Marks" class="form-control max_marks" max="125" readonly="readonly" value="<?php echo $max_marks_2; ?>"></td>
                                      <td> <input type="number" name="MarksObtained_2" id="MarksObtained_2" placeholder="Enter Marks Obtained" class="form-control sub_marks" max="125" readonly="readonly" value="<?php echo $marks_2; ?>"></td>
                                     
                                    </tr>
                                    <tr id="marks_3">
                                      <td> <input type="text" name="subject_3" id="subject_3" placeholder="Enter Subject Name" class="form-control" readonly="readonly" value="<?php echo $subject_3; ?>"></td>
                                      <td> <input type="number" name="MaxMarks_3" id="MaxMarks_3" placeholder="Enter Max Marks" class="form-control max_marks" max="125" readonly="readonly" value="<?php echo $max_marks_3; ?>"></td>
                                      <td> <input type="number" name="MarksObtained_3" id="MarksObtained_3" placeholder="Enter Marks Obtained" class="form-control sub_marks" max="125" readonly="readonly" value="<?php echo $marks_3; ?>"></td>
                                     
                                    </tr>
                                    <tr id="marks_4">
                                      <td> <input type="text" name="subject_4" id="subject_4" placeholder="Enter Subject Name" class="form-control" readonly="readonly" value="<?php echo $subject_4; ?>"></td>
                                      <td> <input type="number" name="MaxMarks_4" id="MaxMarks_4" placeholder="Enter Max Marks" class="form-control max_marks" max="125" readonly="readonly" value="<?php echo $max_marks_4; ?>"></td>
                                      <td> <input type="number" name="MarksObtained_4" id="MarksObtained_4" placeholder="Enter Marks Obtained" class="form-control sub_marks" max="125" readonly="readonly" value="<?php echo $marks_4; ?>"></td>
                                     
                                    </tr>
                                    <tr id="marks_5">
                                      <td> <input type="text" name="subject_5" id="subject_5" placeholder="Enter Subject Name" class="form-control" readonly="readonly" value="<?php echo $subject_5; ?>"></td>
                                      <td> <input type="number" name="MaxMarks_5" id="MaxMarks_5" placeholder="Enter Max Marks" class="form-control max_marks" max="125" readonly="readonly" value="<?php echo $max_marks_5; ?>"></td>
                                      <td> <input type="number" name="MarksObtained_5" id="MarksObtained_5" placeholder="Enter Marks Obtained" class="form-control sub_marks" max="125" readonly="readonly" value="<?php echo $marks_5; ?>"></td>
                                     
                                    </tr>
                                    <tr id="marks_6">
                                      <td> <input type="text" name="subject_6" id="subject_6" placeholder="Enter Subject Name" class="form-control" readonly="readonly" value="<?php echo $subject_6; ?>"></td>
                                      <td> <input type="number" name="MaxMarks_6" id="MaxMarks_6" placeholder="Enter Max Marks" class="form-control max_marks" max="125" readonly="readonly" value="<?php echo $max_marks_6; ?>"></td>
                                      <td> <input type="number" name="MarksObtained_6" id="MarksObtained_6" placeholder="Enter Marks Obtained" class="form-control sub_marks" max="125" readonly="readonly" value="<?php echo $marks_6; ?>"></td>
                                     
                                    </tr>
                                  </thead>
                                  <tbody id="tbl_posts_body">
                                    <div class="field_wrapper">

                                      <!---Table Generates here for Subjects --->
                                    </div>

                                  </tbody>
                                </table>

                              </div>
                            </div>

                            <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">Total Max. Marks (changes as you add subject):</label>
                              <div class="col-sm-9">
                                <input type="text" id="total_max_marks" name="total_max_marks" class="form-control" readonly="readonly" value="<?php echo $total_max_marks; ?>">
                              </div>
                            </div>

                            <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">Total Marks (changes as you add subject):</label>
                              <div class="col-sm-9">
                                <input type="text" id="total_marks" name="total_marks" class="form-control" readonly="readonly" value="<?php echo $total_marks; ?>">
                              </div>
                            </div>


                            <div class="form-group">
                              <label for="Sports Acheivement" class="col-sm-3 control-label">Sports Represented:</label>
                              <div class="col-sm-9">
                                <!--<a class="btn btn-primary pull-right add-sports-record" data-added="0"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add Details:</a> -->
                                <div style="display: none;">
                                  <table id="sports_table">
                                    <tr id="">
                                      <td> <input type="text" name="sport_name[]" id="sport_name[]" placeholder="Enter Sport Name" class="form-control"></td>
                                      <td> <select id="sports-level[]" name="sports-level[]" class="form-control" readonly="readonly" disabled>
                                          <option selected>Select your Level Acheived</option>
                                          <option value="district">District Level</option>
                                          <option value="state">State Level</option>
                                          <option value="national">National Level</option>
                                        </select></td>
                                      <td> <input type="file" name="certificates[]" id="certificates[]" placeholder="Upload your certificate scan copy" class="form-control" accept=".jpg,.png,.bmp,.jpeg"></td>
                                      <td><a class="btn btn-xs delete-sports-record" data-id="0"><i class="glyphicon glyphicon-trash"></i></a></td>
                                    </tr>
                                  </table>
                                </div>
                                <table class="table table-bordered" id="tbl_sports_posts">
                                  <thead>
                                    <tr>
                                      <th>Sport Name</th>
                                      <th>Level Acheived</th>
                                      <th>Download Certificate</th>
                                    </tr>
                                    <tr id="sports_1">
                                      <td> <input type="text" name="sport_name_1" id="sport_name_1" placeholder="Enter Sport Name" class="form-control" readonly="readonly" value="<?php echo $sport_name_1; ?>"></td>
                                      <td> <select id="sports-level-1" name="sports-level-1" class="form-control" readonly="readonly" disabled>
                                          <option selected value="<?php echo $sport_level_1; ?>"><?php echo $sport_level_1; ?></option>
                                          <option value="district">District Level</option>
                                          <option value="state">State Level</option>
                                          <option value="national">National Level</option>
                                        </select></td>
                                      <td><a href="../<?php echo $sport_cert_1_path; ?>"><button class="btn"><i class="fa fa-download"></i> Download</button></a> </td>
                                      
                                    </tr>
                                    <tr id="sports_2">
                                      <td> <input type="text" name="sport_name_2" id="sport_name_2" placeholder="Enter Sport Name" class="form-control" readonly="readonly" value="<?php echo $sport_name_2; ?>"></td>
                                      <td> <select id="sports-level-1" name="sports-level-1" class="form-control" readonly="readonly" disabled>
                                          <option selected value="<?php echo $sport_level_2; ?>"><?php echo $sport_level_2; ?></option>
                                          <option value="district">District Level</option>
                                          <option value="state">State Level</option>
                                          <option value="national">National Level</option>
                                        </select></td>
                                      <td><a href="../<?php echo $sport_cert_2_path; ?>"><button class="btn"><i class="fa fa-download"></i> Download</button></a> </td>
                                      
                                    </tr>
                                    <tr id="sports_3">
                                      <td> <input type="text" name="sport_name_3" id="sport_name_3" placeholder="Enter Sport Name" class="form-control" readonly="readonly" value="<?php echo $sport_name_3; ?>"></td>
                                      <td> <select id="sports-level-1" name="sports-level-1" class="form-control" readonly="readonly" disabled>
                                          <option selected value="<?php echo $sport_level_3; ?>"><?php echo $sport_level_3; ?></option>
                                          <option value="district">District Level</option>
                                          <option value="state">State Level</option>
                                          <option value="national">National Level</option>
                                        </select></td>
                                      <td><a href="../<?php echo $sport_cert_3_path; ?>"><button class="btn"><i class="fa fa-download"></i> Download</button></a> </td>
                                      
                                    </tr>
                                  </thead>
                                  <tbody id="tbl_sports_posts_body">
                                    <div class="field_sports_wrapper">

                                      <!---Table Generates here for Subjects --->
                                    </div>

                                  </tbody>
                                </table>

                              </div>
                            </div>



                            <!--- sports---->


                            <div class="form-group">
                              <label for="Courses Offered" class="col-sm-3 control-label">Intereseted Activities: (Only Checked Activities)</label>
                              <div class="col-sm-9">
                                <table class="table table-bordered">

                                  <tbody>
                                    <tr>
                                      <td>
                                        <div class="checkbox">
                                          <?php if($scm_activity == "SCM") {
                                                echo '<label><input type="checkbox" name="SCM" id="SCM" value="SCM" checked disabled>SCM </label>';
                                          }
                                          else {
                                                echo '<label><input type="checkbox" name="SCM" id="SCM" value="SCM" disabled>SCM </label>';
                                          }

                                          ?>
                                        </div>
                                      </td>
                                      <td>
                                        <div class="checkbox">
                                        <?php if($choir_activity == "Choir") {
                                                echo '<label><input type="checkbox" name="Choir" id="Choir" value="Choir" checked disabled>Choir </label>';
                                          }
                                          else {
                                                echo '<label><input type="checkbox" name="Choir" id="Choir" value="Choir" disabled>Choir </label>';
                                          }

                                          ?>
                                        </div>
                                      </td>
                                      <td>
                                        <div class="checkbox">
                                        <?php if($civil_defense_activity == "Civil Defense") {
                                                echo '<label><input type="checkbox" name="civil_defense" id="civil_defense" value="civil_defense" checked disabled>Civil Defense </label>';
                                          }
                                          else {
                                                echo '<label><input type="checkbox" name="civil_defense" id="civil_defense" value="civil_defense" disabled>Civil Defense</label>';
                                          }

                                          ?>
                                        </div>
                                      </td>

                                      <td>
                                        <div class="checkbox">
                                        <?php if($nss_defense_activity == "NSS") {
                                                echo '<label><input type="checkbox" name="nss_defense" id="nss_defense" value="nss_defense" checked disabled>NSS </label>';
                                          }
                                          else {
                                                echo '<label><input type="checkbox" name="nss_defense" id="nss_defense" value="nss_defense" disabled>NSS</label>';
                                          }

                                          ?>
                                        </div>
                                      </td>
                                      <td>
                                        <div class="checkbox">
                                        <?php if($music_activity == "Music") {
                                                echo '<label><input type="checkbox" name="music" id="music" value="Music" checked disabled>Music </label>';
                                          }
                                          else {
                                                echo '<label><input type="checkbox" name="music" id="music" value="Music" disabled>Music</label>';
                                          }

                                          ?>
                                        </div>
                                      </td>
                                      <td>
                                        <div class="checkbox">
                                        <?php if($sports_activity == "Sports") {
                                                echo '<label><input type="checkbox" name="sports" id="sports" value="Sports" checked disabled>Sports </label>';
                                          }
                                          else {
                                                echo '<label><input type="checkbox" name="sports" id="sports" value="Sports" disabled>Sports</label>';
                                          }

                                          ?>
                                        </div>
                                      </td>

                                    </tr>



                                  </tbody>
                                </table>


                              </div>
                            </div>




                            <!---Address---->
                            <div class="form-group">

                              <label for="Office Address" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Name of the School/ Institution last studied:
                              </label>
                              <div class="col-sm-9">
                                <input type="Denomination" id="previous_institute" name="previous_institute" placeholder="Previous Institution Name" class="form-control" required readonly="readonly" value="<?php echo $previous_institution_name; ?>">
                              </div>
                            </div>

                            <div class="form-group">
                              <label for="Office Address" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Address of the School/ Institution last studied:
                              </label>
                              <div class="col-sm-9">
                                <textarea class="form-control" id="PreviousInstituteAddress" name="PreviousInstituteAddress" rows="3" placeholder="Do not exceed more than 160 characters" maxlength="160" title="Only 160 Characters!!!" required readonly="readonly"><?php echo $previous_institution_address; ?> </textarea> </div>
                            </div>
                            <div class="form-group">

                              <label for="Office Address" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Year of Passing:
                              </label>
                              <div class="col-sm-9">
                                <input type='number' id="year_of_passing" name="year_of_passing" min="<?php echo date('Y', strtotime('-35 years')); ?>" max="<?php echo date('Y', strtotime('0 years')); ?>" value="<?php echo $year_of_passing; ?>" class="form-control" required readonly="readonly">
                              </div>
                            </div>
                            <hr style="border:3px solid #eee">
                            <h3 class="center" style="color: #008000; font-style:normal;">DECLARATION</h3>

                            <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label"><input type="checkbox" required checked disabled><strong>
                                  <font color="red"><b>* </b></font>Declaration:</label>
                              <div class="col-sm-9">
                                I hereby promise to take responsibility in matters of academics, discipline and attendance for the holistic development of my ward and declare that the particulars furnished above are true and correct to the best of my knowledge.I promise to abide by the rules and regulations of the college.
                                I will accept the decision of the Principal and the Management in all matters of academic as final.</a>
                              </div>
                            </div>





                            <div class="form-group">
                              <div class="checkbox">
                                <div class="col-sm-12">
                                  <label>
                                    <div class="col-sm-6">
                                      <font color="red"><b>* </b></font>I,<input type="textbox" class="form-control" name="self_declaration" id="self_declaration" placeholder="Declare your name here" required readonly="readonly" value="<?php echo $self_declaration_name; ?>">
                                    </div>
                                    shall be responsible for adhering minimum attendance of 75% in each subject, to appear for the Bangalore university exam,
                                    falling which, I do agree that my daughter/ward will repeat the course (class) for the next academic year <?php echo date('Y', strtotime('+1 years')); ?> .
                                    I will also agree that the admission will be cancelled if the furnished details in the application are found to be false or manipulated

                                  </label>
                                </div>
                              </div>
                            </div> <!-- /.form-group -->
                            <div class="form-group">
                              <div class="col-sm-12">
                                <div class="col-sm-6">
                                  <strong>Date & Time of Submission:</strong> <?php echo $date; ?>
                                </div>
                                <div class="col-sm-6">
                                  <strong>
                                    <font color="red"><b>* </b></font>Place:
                                  </strong> <input type="textbox" class="form-control" name="place_declaration" id="place_declaration" placeholder="Your Place name" required readonly="readonly" value="<?php echo $place_of_submission; ?>">
                                </div>
                              </div>
                            </div>

                            <hr style="border:3px solid #eee">
                            <h3 class="center" style="color: #008000; font-style:normal;">Download All Enclosures</h3>
                            <font color="red"><b id="signature_error"></b></font>

                            <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">Father Signature:</label>
                              <div class="col-sm-3 noprint">
                                        <a href="../<?php echo $father_signature_picture_path; ?>" target="_blank"><button class="btn" type="button"><i class="fa fa-download"></i> Download</button></a>
                              </div>

                              <div id="Father_signature"><img height="150" width="150" id="father_signature" src="../<?php echo $father_signature_picture_path; ?>" />
                                        </div>
                            </div>
                            <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">Mother Signature:</label>
                              <div class="col-sm-3 noprint">
                              <a href="../<?php echo $mother_signature_picture_path; ?>" target="_blank"><button class="btn" type="button"><i class="fa fa-download"></i> Download</button></a>
                              </div>

                              <div id="Mother_signature"><img height="150" width="150" id="mother_signature" src="../<?php echo $mother_signature_picture_path; ?>" />
                                        </div>
                            </div>
                            <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">Guardian Signature:</label>
                              <div class="col-sm-3 noprint">
                              <a href="../<?php echo $guardian_signature_picture_path; ?>" target="_blank"><button class="btn" type="button"><i class="fa fa-download"></i> Download</button></a>
                              </div>

                              <div id="guardian_signature"><img height="150" width="150" id="guardian_sign" src="../<?php echo $guardian_signature_picture_path; ?>" />
                                        </div>
                            </div>
                            <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">Student Signature:</label>
                              <div class="col-sm-3 noprint">
                              <a href="../<?php echo $student_signature_picture_path; ?>" target="_blank"><button class="btn" type="button"><i class="fa fa-download"></i> Download</button></a>
                              </div>

                              <div id="student_signature"><img height="150" width="150" id="student_sign" src="../<?php echo $student_signature_picture_path; ?>" />
                                        </div>
                            </div>
                            <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">Marks Card:</label>
                              <div class="col-sm-3 noprint">
                              <a href="../<?php echo $marks_card_picture_path; ?>" target="_blank"><button class="btn" type="button"><i class="fa fa-download"></i> Download</button></a>
                              </div>

                              <div id="marks_card"><img height="150" width="150" id="marks_cert" src="../<?php echo $marks_card_picture_path; ?>" />
                                        </div>
                            </div>
                            <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">TC (Transfer Certificate)/Migration :</label>
                              <div class="col-sm-3 noprint">
                              <a href="../<?php echo $transfer_certificate_picture_path; ?>" target="_blank"><button class="btn" type="button"><i class="fa fa-download"></i> Download</button></a>
                              </div>

                              <div id="tc_cert"><img height="150" width="150" id="tc_certificate" src="../<?php echo $transfer_certificate_picture_path; ?>" />
                                        </div>
                            </div>
                            <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">If SC/ST/BC, Please attach certificate here:</label>
                              <div class="col-sm-3 noprint">
                              <a href="../<?php echo $obc_certificate_picture_path; ?>" target="_blank"><button class="btn" type="button"><i class="fa fa-download"></i> Download</button></a>
                              </div>

                              <div id="obc_cert"><img height="150" width="150" id="obc" src="../<?php echo $obc_certificate_picture_path; ?>" />
                                        </div>
                            </div>
                            <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">Eligibility Criteria (Foreign Students):</label>
                              <div class="col-sm-3 noprint">
                              <a href="../<?php echo $foreign_eligible_picture_path; ?>" target="_blank"><button class="btn" type="button"><i class="fa fa-download"></i> Download</button></a>
                              </div>

                              <div id="eligibility_criteria"><img height="150" width="150" id="foreign_eligible_cert" src="../<?php echo $foreign_eligible_picture_path; ?>" />
                                        </div>
                            </div>
                            <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">If CSI, Membership letter attested by the presbyter in-charge:</label>
                              <div class="col-sm-3 noprint">
                              <a href="../<?php echo $csi_membership_picture_path; ?>" target="_blank"><button class="btn" type="button"><i class="fa fa-download"></i> Download</button></a>
                              </div>

                              <div id="csi_cert_path"><img height="150" width="150" id="csi_cert" src="../<?php echo $csi_membership_picture_path; ?>" />
                                        </div>
                            </div>
                            <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">Note:</label>
                              <div class="col-sm-9">
                                <label for="Note">1. Incomplete forms will not be considered for Admission.</label>
                                <label for="Note">2. Fee once paid, will not be refunded.</label>
                                <label for="Note">3. Admission is subject to the approval of bangalore university.</label>
                              </div>
                            </div>

                            <div class="form-group">
                              <label for="Second Languages Offered" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Change Status of this application (Only for Admin use):
                              </label>
                              <div class="col-sm-9">
                                <select id="application-status-submitted" name="application-status-submitted" class="form-control" required>
                                  <option selected value="">Change Application Status</option>
                                  <option value="Admitted">Admitted</option>
                                  <option value="Declined">Declined</option>
                                </select>
                                <font color="brown">IT is required to change the status after review of the application here:</font>
                              </div>
                            </div>


                            <div class="form-group">
                              <div class="col-sm-6">

                              <button type="submit" name="submit_application" id="submit_application" class="btn btn-primary btn-block">Update Application</button>
                                
                              </div>
                              <div class="col-sm-6">

                              <button id="print_application" class="btn btn-primary btn-block" onclick="printDiv('application_view')">Print Application</button>
                                
                              </div>

                            </div>
                          </form>
                        </div> <!-- /.form-group -->


                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- end course content container -->



            </div>

          </div>
        </div>
      </div>
    </div>
    </div>
  </section>




  <?php include('include/footer.php'); ?>
  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>
  <!--<script type="text/javascript" src="assets/js/validate-form-fields.js"></script>-->


  <!-- Custom js -->

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet" />

  
  <script>
		function printDiv(divName){
			var printContents = document.getElementById(divName).innerHTML;
			var originalContents = document.body.innerHTML;

			document.body.innerHTML = printContents;

			window.print();

			document.body.innerHTML = originalContents;

		}
  </script>
  
  <script type="text/javascript">
  $(document).ready(function () {
  $('#application_form').submit(function(e){
        e.preventDefault();
       //var application_data = document.getElementById("application_number").value;
        //console.log(application_data);
        // ajax
        $.ajax({
          type: "POST",
          url: "functions/update_application.php",
          data: $(this).serialize(), // get all form field value in serialize form
          success: function(result) {
            console.log(result);
            var json = $.parseJSON(result);
            if (json.response.code == "1") {
              $("#reset_message").fadeIn().text(json.response.message);
              setTimeout(function() {
                window.location.href = "applications.php";
              }, 5000);
            } else
              $("#reset_error").fadeIn().text(json.response.status + ": " + json.response.message);
              setTimeout(function() {
                $("#reset_error").fadeOut().empty();
              }, 5000);
          }
        });
    });
  });
  </script>
</body>

</html>