<?php include('include/session.php'); 
if (!isset($_SESSION['admin_user'])) {
  header("Location: login.php"); 
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Goodwill college | Home</title>

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

  <!-- Font awesome -->
  <link href="assets/css/font-awesome.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <!-- Slick slider -->
  <link rel="stylesheet" type="text/css" href="assets/css/slick.css">
  <!-- Theme color -->
  <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

  <!-- Main style sheet -->
  <link href="assets/css/style.css" rel="stylesheet">
  
  
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
  


  <!-- Google Fonts -->
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>

</head>

<body>
  
  <?php include('include/header.php'); ?>

  <!--START SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#">
    <i class="fa fa-angle-up"></i>
  </a>
  <!-- END SCROLL TOP BUTTON -->
  

  <!--Header File Included here-->

   <!-- Hero-area -->
   <div class="hero-area section">

<!-- Backgound Image -->
<div class="bg-image bg-parallax overlay" style="background-image:url(assets/img/counter-bg.jpg)"></div>
<!-- /Backgound Image -->


  <div class="container">
      <div class="row">
        <div class="col-md-10 col-md-offset-1 text-center">
          <ul class="hero-area-tree">
            <li><a href="home.php">Home</a></li>
          </ul>
          <h1 class="white-text">Student Applications with Goodwill Christian College.</h1>

        </div>
      </div>
    </div>
</div>

<div class="container">
<div class="mu-title">
                          <h2>Applications</h2>
                        </div>
                        <div id="alert_message"></div>
<table id='applications' class='display dataTable'>

<thead>
  <tr>
    <th>Application ID</th>
    <th>Student Email</th>
    <th>Student Name</th>
    <th>Application Status</th>
    <th>Action</th>
  </tr>
</thead>
</table>
</div>



  <?php include('include/footer.php'); ?>
  
 



  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="assets/js/redirect.js"> </script>

  <script type="text/javascript">
  $(document).ready(function(){
  //fetch_data();
  //function fetch_data() {
   var dataTable = $('#applications').DataTable({
      'processing': true,
      'serverSide': true,
      'serverMethod': 'post',
      'ajax': {
          'url':'functions/applications_fetch.php'
      },
      'columns': [
         { data: 'student_name' },
         { data: 'application_id' },
         { data: 'email' },
         { data: 'application_status' },
         {"defaultContent": "<button type='button' class='btn btn-success view'>View</button>"},
      ],
      "order": [[1, 'asc']]
   });
  //}
  $('#applications tbody').on( 'click', 'button', function (){
      var data = dataTable.row( $(this).parents('tr') ).data();
      $.redirect("application_view.php", data, "POST");
       
  });		
});
  
</script>


</body>

</html>