//Load Photo dynamically.
var loadFile = function(event) {
    var image = document.getElementById('output');
    image.src = URL.createObjectURL(event.target.files[0]);
};
$(document).ready(function() {
    jQuery(document).delegate('a.add-record', 'click', function(e) {
      e.preventDefault();
      var content = jQuery('#sample_table tr'),
        size = jQuery('#tbl_posts >tbody >tr').length + 1,
        element = null,
        element = content.clone();
      if (size < 7) {
        element.attr('id', 'rec-' + size);
        element.find('.delete-record').attr('data-id', size);
        element.appendTo('#tbl_posts_body');
      } else {
        alert("Only 6 subjects can be added!!!!")
      }
    });
    jQuery(document).delegate('a.delete-record', 'click', function(e) {
      e.preventDefault();
      var didConfirm = confirm("Are you sure You want to delete");
      if (didConfirm == true) {
        var id = jQuery(this).attr('data-id');
        var targetDiv = jQuery(this).attr('targetDiv');
        jQuery('#rec-' + id).remove();

        //regnerate index number on table
        $('#tbl_posts_body tr').each(function(index) {});
        return true;
      } else {
        return false;
      }
    });
    jQuery(document).delegate('a.add-sports-record', 'click', function(e) {
        e.preventDefault();
        var content = jQuery('#sports_table tr'),
          size = jQuery('#tbl_sports_posts >tbody >tr').length + 1,
          element = null,
          element = content.clone();
        if (size < 4) {
          element.attr('id', 'rec-sports-' + size);
          element.find('.delete-sports-record').attr('data-id', size);
          element.appendTo('#tbl_sports_posts_body');
        } else {
          alert("Only 3 Sports can be added!!!!")
        }
      });
      jQuery(document).delegate('a.delete-sports-record', 'click', function(e) {
        e.preventDefault();
        var didConfirm = confirm("Are you sure You want to delete");
        if (didConfirm == true) {
          var id = jQuery(this).attr('data-id');
          var targetDiv = jQuery(this).attr('targetDiv');
          jQuery('#rec-sports-' + id).remove();

          //regnerate index number on table
          $('#tbl_sports_posts_body tr').each(function(index) {});
          return true;
        } else {
          return false;
        }
      });
});

$("#course-selector").change(function() {
    var $dropdown = $(this);

    $.getJSON("assets/js/dependent-dropdown.json", function(data) {

      var key = $dropdown.val();
      var vals = [];

      switch (key) {
        case 'BA':
          vals = data.BA.split(",");
          break;
        case 'BSc':
          vals = data.BSc.split(",");
          break;
        case 'BCom':
          vals = data.BCom.split(",");
          break;
        case 'BBA':
          vals = data.BBA.split(",");
          break;
        case 'BCA':
          vals = data.BCA.split(",");
          break;
        case 'base':
          vals = ['Please choose from above'];
      }

      var $secondChoice = $("#majors-selector");
      $secondChoice.empty();
      $.each(vals, function(index, value) {
        $secondChoice.append("<option>" + value + "</option>");
      });

    });
    
});

function DisplayTextBox(val){
    var element=document.getElementById('examination-passed');
    var textelement=document.getElementById('examination-passed-textbox');
    if(val=='Others'){
      element.style.display='none';
      textelement.style.display='block';
      textelement.focus();
    }
    else{  
      textelement.style.display='none';
      element.style.display='block';
    }
}

function ExamOtherTextBox(val){
  if(val==''){
    $("#exam_other_text_error").fadeIn().text("You have to mention Exam name or you cannot proceed further!!!");
    $("#examination-passed-textbox").focus();
    setTimeout(function() {
      $("#exam_other_text_error").fadeOut().empty();
    }, 5000);            
    return false;
  }
  else{  
    console.log("Filled Something in Exam Other Text Box");
  }
}

function DisplayCategoryTextBox(val){
    var element=document.getElementById('category');
    var textelement=document.getElementById('category-textbox');
    if(val=='Others'){
      element.style.display='none';
      textelement.style.display='block';
      textelement.focus();
    }
    else{  
      textelement.style.display='none';
      element.style.display='block';
    }
}

function CategoryOtherTextBox(val){
  if(val==''){
    $("#category_other_text_error").fadeIn().text("You have to mention Category name or you cannot proceed further!!!");
    $("#category-textbox").focus();
    setTimeout(function() {
      $("#category_other_text_error").fadeOut().empty();
    }, 5000);            
    return false;
  }
  else{  
    console.log("Filled Something in Category Other Text Box");
  }
}

$("#same_as_current_address").change(function(){
    if(this.checked) {
          //Do stuff
          $("#PermanentAddress").hide(); 
      }
    else{
          $("#PermanentAddress").show(); 
    }
});


$("#submit_application").click(function() {
    var fathersName = $("#FathersName").val();
    var fatherOccupation = $("#FatherOccupation").val();
    var fatherAnnualIncome = $("#FatherAnnualIncome").val();
    var fatherMobileNo =$("#FatherMobileNo").val();
    var mothersName = $("#MothersName").val();
    var motherOccupation = $("#MotherOccupation").val();
    var motherAnnualIncome = $("#MotherAnnualIncome").val();
    var motherMobileNo =$("#MotherMobileNo").val();
    var guardiansName = $("#GuardiansName").val();
    var guardianOccupation = $("#GuardianOccupation").val();
    var guardianAnnualIncome = $("#GuardianAnnualIncome").val();
    var guardianMobileNo =$("#GuardianMobileNo").val();
    var otherCategoryBox=$("#category-textbox");
    var otherExamPassed=$("#examination-passed-textbox");
   // var guardianSignature=$("#guardian_signature");

    if(fathersName.length != "" || fatherOccupation.length != "" || fatherAnnualIncome.length != "" || fatherMobileNo.length != "" || mothersName.length != "" || motherOccupation.length != "" || motherAnnualIncome.length != "" || motherMobileNo.length != "" || guardiansName.length != "" || guardianOccupation.length != "" || guardianAnnualIncome.length != "" || guardianMobileNo.length != "")
      {
          if (fathersName.length != "" || mothersName.length != "" || guardiansName.length != ""){
            console.log("Some Name is filled in");
          }
          else{
            $("#name_error").fadeIn().text("Atleast fill Name of either Father's, Mother's or Guardian's please!!!!");
            $("#FathersName").focus();
            setTimeout(function() {
              $("#name_error").fadeOut().empty();
            }, 5000);            
            return false;
          }
          if (fatherOccupation.length != "" || motherOccupation.length != "" || guardianOccupation.length != ""){
            console.log("Some Occupation is filled in");
          }
          else{
            $("#occupation_error").fadeIn().text("Atleast fill Occupation of either Father's, Mother's or Guardian's please!!!!");
            $("#FatherOccupation").focus();
            setTimeout(function() {
              $("#occupation_error").fadeOut().empty();
            }, 5000);
            return false;
          }
          if (fatherAnnualIncome.length != "" || motherAnnualIncome.length != "" || guardianAnnualIncome.length != ""){
            console.log("Some Annual Income is filled in");
          }
          else{
            $("#annual_income_error").fadeIn().text("Atleast fill Annual Income of either Father's, Mother's or Guardian's please!!!!");
            $("#FatherAnnualIncome").focus();
            setTimeout(function() {
              $("#annual_income_error").fadeOut().empty();
            }, 5000);
            return false;
          }
          if (fatherMobileNo.length != "" || motherMobileNo.length != "" || guardianMobileNo.length != ""){
            console.log("Some Mobile number is filled in");
          }
          else{
            $("#mobile_error").fadeIn().text("Atleast fill Mobile Number of either Father's, Mother's or Guardian's please!!!!");
            $("#FatherMobileNo").focus();
            setTimeout(function() {
              $("#mobile_error").fadeOut().empty();
            }, 5000);
            return false;
          }
      }
    else{
        $("#parental_detail").fadeIn().text("Atleast fill Name, Occupation, Annual Income & Mobile Number of either Father's, Mother's or Guardian's please!!!!");
        $("#FathersName").focus();
        setTimeout(function() {
          $("#parental_detail").fadeOut().empty();
        }, 5000);
        return false;
    }
    /*if($('#father_signature')[0].files.length === 0 && $('#mother_signature')[0].files.length === 0 && $('#guardian_signature')[0].files.length === 0){
        $("#signature_error").fadeIn().text("Atleast Upload Father's or Mother's or Guardian's Signature to go forward!!!!");
        $("#father_signature").focus();
        setTimeout(function() {
          $("#signature_error").fadeOut().empty();
        }, 5000);
        return false;
    }*/
    if(otherCategoryBox.length != ""){
      console.log("Some Category is filled in");
    }
    else{
      $("#category_other_text_error").fadeIn().text("Please fill-in Category!!!!");
      $("#category-textbox").focus();
      setTimeout(function() {
        $("#category_other_text_error").fadeOut().empty();
      }, 5000);
      return false;
    }
    if(otherExamPassed.length != ""){
      console.log("Some Exam is filled in");
    }
    else{
      $("#exam_other_text_error").fadeIn().text("Please fill-in Category!!!!");
      $("#examination-passed-textbox").focus();
      setTimeout(function() {
        $("#exam_other_text_error").fadeOut().empty();
      }, 5000);
      return false;
    }
    var subjects = $("input[name='subject[]']")
              .map(function(){return $(this).val();}).get();
    var max_marks = $("input[name='MaxMarks[]']")
              .map(function(){return $(this).val();}).get();
    var marks_obtained = $("input[name='MarksObtained[]']")
              .map(function(){return $(this).val();}).get();
    console.log(max_marks);
    if(subjects.length < 7 || max_marks.length < 7 || marks_obtained.length < 7){
      $("#marks_error").fadeIn().text("You have to fill marks of all 6 subjects!!!");
      $("#marks_error").focus();
      setTimeout(function() {
        $("#marks_error").fadeOut().empty();
      }, 5000);
      return false;
    }
    else{
      console.log("Now, all subjects are filled");
    }
  
});


//var marks = $("input[name='MaxMarks[]']").map(function(){return $(this).val();}).get();
//console.log(marks);

//function getSum(total, num) {
//  return total + Math.round(num);
//}
//function sumupMarks(item) {
//  document.getElementById("total_marks").innerHTML = marks.reduce(getSum, 0);
//}

//calculation script
var $form = $('#application_form'),
    $sumDisplay = $('#total_max_marks'), $sumMarksDisplay = $('#total_marks');

$form.delegate('.max_marks', 'change', function ()
{
    var $summands = $form.find('.max_marks');
    var sum = 0;
    $summands.each(function ()
    {
        var value = Number($(this).val());
        if (!isNaN(value)) sum += value;
    });

    $sumDisplay.val(sum);
});

$form.delegate('.sub_marks', 'change', function ()
{
    var $summands = $form.find('.sub_marks');
    var sum = 0;
    $summands.each(function ()
    {
        var value = Number($(this).val());
        if (!isNaN(value)) sum += value;
    });

    $sumMarksDisplay.val(sum);
});