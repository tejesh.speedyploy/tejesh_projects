$("#error_signup").hide();
    $("#show_message_signup").hide();
 
    // on submit...
    $('#signup_form').submit(function(e){
 
        e.preventDefault();
 
 
        $("#error_signup").hide();
 
        //name required
        var name = $("input#signup_first_name").val();
        if(name == ""){
            $("#error_signup").fadeIn().text("Name required.");
            $("input#signup_first_name").focus();
            return false;
        }
 
        // email required
        var email = $("input#signup_email").val();
        if(email == ""){
            $("#error_signup").fadeIn().text("Email required");
            $("input#signup_email").focus();
            return false;
        }
 
 
        // ajax
        $.ajax({
            type:"POST",
            url: "functions/signup.php",
            data: $(this).serialize(), // get all form field value in serialize form
            success: function(result){
              var json = $.parseJSON(result);
              if (json.response.code =="1"){
                $("#show_message_signup").fadeIn();
                setTimeout(function(){ location.reload()}, 3000);
              }
              else
                $("#error_signup").fadeIn().text(json.response.status + ": " + json.response.message);
                setTimeout(function() {
                  $("#error_signup").fadeOut().empty();
                }, 5000);
            }
        });
    });

    $('#header_login').submit(function(e) {
        e.preventDefault();
    
        // ajax
        $.ajax({
          type: "POST",
          url: "functions/header_login_authenticate.php",
          data: $(this).serialize(), // get all form field value in serialize form
          success: function(result) {
            var json = $.parseJSON(result);
            if (json.response.code == "1") {
              $("#show_header").fadeIn().text(json.response.message);
              setTimeout(function() {
                location.reload();
              }, 3000);
            } else
              $("#header_error").fadeIn().text(json.response.status + ": " + json.response.message);
              setTimeout(function() {
                $("#header_error").fadeOut().empty();
              }, 5000);
          }
        });
       });