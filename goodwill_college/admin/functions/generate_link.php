<?php
    include('../include/session.php');
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    require_once "config.php";
    require_once "mail_config.php";

 
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    
    
 
    $user_details = mysqli_query($conn, "SELECT * from admin_user where email='" . $email . "';");
    if(mysqli_num_rows($user_details) > 0){
        while($detail = mysqli_fetch_assoc($user_details)){
        
        
        $mail->AddAddress($email, $detail['first_name']);
        $mail->Subject  =  'One time Link Generated!!!';
        $mail->Body = 'Please do reset your password at http://34.107.85.215/admin/reset_password.php?otd=' . $detail['password'] . '&email=' . $email . '';
        if($mail->Send()){
            $result = array(
                'response' => array(
                  'status' => 'success',
                  'code' => '1', // whatever you want
                  'message' => 'Success!!!, please check confirmation mail in your spam if you do not see mail from us in your inbox.'
                )
            );
        }
        else{
            echo "Error Sending mail"; 
        }
        }
    } else {
        $result = array(
            'response' => array(
              'status' => 'Failed',
              'code' => '2', // whatever you want
              'message' => mysqli_error($conn)
            )
        );
        $mail->AddAddress($email);
        $mail->Subject  =  'Password Reset Failure!!!';
        $mail->Body = 'Looks like you dont have admin account with us. Please do signup at http://www.goodwillchristiancollege.com/admin/login.php';
        $mail->Send();
    }
    mysqli_close($conn);
    echo json_encode($result);

?>