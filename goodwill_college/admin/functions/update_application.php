<?php
    include('../include/session.php');
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    require_once "config.php";
    require_once "mail_config.php";

    $app_num = mysqli_real_escape_string($conn, $_POST['application_number']);

    if (isset($_POST['application_number'])) {   
    

        $application_number = mysqli_real_escape_string($conn, $_POST['application_number']);
        $application_status_submitted = mysqli_real_escape_string($conn, $_POST['application-status-submitted']);
        $email = mysqli_real_escape_string($conn, $_POST['StudentEmail']);
        if(mysqli_query($conn, "UPDATE application SET application_status = '" . $application_status_submitted . "' where application_id='" . $application_number . "';")) {
            $result = array(
                'response' => array(
                  'status' => 'success',
                  'otd' => $_POST['application_number'],
                  'code' => '1', // whatever you want
                  'message' => 'Success!!!, please ask the student to check application status update mail in their spam folder incase, if they do not see mail from us in their inbox.'
                )
            );
            $message = "Your Application number '" . $application_number ."' has been '" . $application_status_submitted ."'";
            $mail->AddAddress($email);
            $mail->Subject  =  'Application Status Updated.';
            $mail->Body = $message;
            $mail->Send(); 
        } else {
            $result = array(
                'response' => array(
                  'status' => 'Failed',
                  'code' => '2', // whatever you want
                  'message' => mysqli_error($conn)
                )
            );
            $last_id = mysqli_query($conn, "SELECT id from application;");
            if (mysqli_num_rows($last_id) > 0) {
                // output data of each row
                while($last_insert_id = mysqli_fetch_assoc($last_id)) {
                  //echo "id: '" . $last_insert_id["id"]. "'";
                  $increment_value =(int) $last_insert_id['id'];
                  if(mysqli_query($conn,"ALTER TABLE application AUTO_INCREMENT =  $increment_value;"));
              } 
            }
            $mail->AddAddress($email);
            $mail->Subject  =  'Update Status Failure!!!';
            $mail->Body = 'Looks like something happened in application status!!!. Please do contact us over mail or telephone.';
            $mail->Send();
        }
    }
    else{
      $result = array(
        'response' => array(
          'status' => 'Failed',
          'code' => '3', // whatever you want
          'message' => mysqli_error($conn)
        )
    );
    }
    mysqli_close($conn);
    echo json_encode($result);

?>