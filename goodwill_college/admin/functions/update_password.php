<?php
    include('../include/session.php');
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    require_once "config.php";
    require_once "mail_config.php";

    $signup_password = mysqli_real_escape_string($conn, $_POST['signup_password']);

    if (isset($_POST['otd'])) {   
    

        $otd = mysqli_real_escape_string($conn, $_POST['otd']);
        $email = mysqli_real_escape_string($conn, $_POST['reset_email']);
        if(mysqli_query($conn, "UPDATE admin_user SET password = '" . md5($signup_password) ."' where email='" . $email . "';")) {
            $result = array(
                'response' => array(
                  'status' => 'success with otd',
                  'otd' => $_POST['otd'],
                  'code' => '1', // whatever you want
                  'message' => 'Success!!!, please check confirmation mail in your spam if you do not see mail from us in your inbox.'
                )
            );
            $mail->AddAddress($email);
            $mail->Subject  =  'Password Reset Confirmation';
            $mail->Body = 'You have reset your password successfully. Please do login again at http://www.goodwillchristiancollege.com/';
            $mail->Send(); 
        } else {
            $result = array(
                'response' => array(
                  'status' => 'Failed',
                  'code' => '2', // whatever you want
                  'message' => mysqli_error($conn)
                )
            );
            $last_id = mysqli_query($conn, "SELECT id from admin_user;");
            if (mysqli_num_rows($last_id) > 0) {
                // output data of each row
                while($last_insert_id = mysqli_fetch_assoc($last_id)) {
                  //echo "id: '" . $last_insert_id["id"]. "'";
                  $increment_value =(int) $last_insert_id['id'];
                  if(mysqli_query($conn,"ALTER TABLE admin_user AUTO_INCREMENT =  $increment_value;"));
              } 
            }
            $mail->AddAddress($email);
            $mail->Subject  =  'Password Reset Failure!!!';
            $mail->Body = 'Looks like something happened!!!. Please do try again or contact us over mail or telephone.';
            $mail->Send();
        }
    }
    else{
      $mail->AddAddress($_SESSION['admin_email'], $_SESSION['admin_user']);
 
      if(mysqli_query($conn, "UPDATE admin_user SET password = '" . md5($signup_password) ."' where email='" . $_SESSION['admin_email'] . "';")) {
          $result = array(
              'response' => array(
                'status' => 'success without otd',
                'otd' => 'no otd',
                'code' => '1', // whatever you want
                'message' => 'Success!!!, please check confirmation mail in your spam if you do not see mail from us in your inbox.'
              )
          );
          $mail->Subject  =  'Password Reset Confirmation';
          $mail->Body = 'You have reset your password successfully. Please do login again at http://www.goodwillchristiancollege.com/';
          $mail->Send(); 
      } else {
          $result = array(
              'response' => array(
                'status' => 'Failed',
                'code' => '2', // whatever you want
                'message' => mysqli_error($conn)
              )
          );
          $last_id = mysqli_query($conn, "SELECT id from admin_user;");
          if (mysqli_num_rows($last_id) > 0) {
              // output data of each row
              while($last_insert_id = mysqli_fetch_assoc($last_id)) {
                //echo "id: '" . $last_insert_id["id"]. "'";
                $increment_value =(int) $last_insert_id['id'];
                if(mysqli_query($conn,"ALTER TABLE admin_user AUTO_INCREMENT =  $increment_value;"));
            } 
          }
          $mail->Subject  =  'Password Reset Failure!!!';
          $mail->Body = 'Looks like something happened!!!. Please do try again or contact us over mail or telephone.';
          $mail->Send();
      }

    }
    mysqli_close($conn);
    echo json_encode($result);

?>