<!-- Start footer -->

<footer id="mu-footer">
    <!-- start footer top -->
    
    <div class="mu-footer-bottom">
      <div class="container">
        <div class="mu-footer-bottom-area">
          <p><?php echo date('Y');?> &copy; All Right Reserved. <a href="http://www.gooddwillchristiancollege.com/" rel="nofollow">Goodwill Christian College</a></p>
        </div>
      </div>
    </div>
    <!-- end footer bottom -->
  </footer>
  <!-- End footer -->
