

<header id="header">
<div class="container">

    <div class="navbar-header">
        <!-- Logo -->
        <div class="navbar-brand">
            <a class="logo" href="index.php">
                <img src="assets/img/logo.png" alt="logo">
            </a>
        </div>
    </div>
   </div>
</header>

<section id="mu-menu">
<nav class="navbar navbar-default navbar-expand-lg" role="navigation">  

<div class="navbar-header">
<!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>

</div>
<div id="navbar" class="navbar-collapse collapse">
<ul id="top-menu" class="nav navbar-nav main-nav">

<?php if(!isset($_SESSION['admin_user'])){
echo'Something Went Wrong with Login.';
}
else{
echo '
<li><a href="home.php">Registrations</a></li>
<li><a href="applications.php">Applications</a></li>   

  <li class="nav-item">
    <a data-toggle="dropdown" class="nav-link dropdown-toggle" href="#">Hi, ' . $_SESSION['admin_user'] . '</a> <li>
    <li><a href="reset_password.php">Update Password</a></li>			
    <li><a href="functions/sign_out.php">Sign Out</a></li>			

  
    ';
}
?>
</ul>
</div><!--/.nav-collapse -->        

</nav>
</section>

<!-- End menu -->
