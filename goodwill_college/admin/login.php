<?php include('include/session.php'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Goodwill college |Contact</title>

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

  <!-- Font awesome -->
  <link href="assets/css/font-awesome.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <!-- Slick slider -->
  <link rel="stylesheet" type="text/css" href="assets/css/slick.css">
  <!-- Theme color -->
  <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

  <!-- Main style sheet -->
  <link href="assets/css/style.css" rel="stylesheet">


  <!-- Google Fonts -->
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

  <header id="header">
    <div class="container">

      <div class="navbar-header">
        <!-- Logo -->
        <div class="navbar-brand">
          <a class="logo" href="index.php">
            <img src="assets/img/logo.png" alt="logo">
          </a>
        </div>
      </div>
    </div>
  </header>

  <!-- Hero-area -->
  <div class="hero-area section">

    <!-- Backgound Image -->
    <div class="bg-image bg-parallax overlay" style="background-image:url(assets/img/counter-bg.jpg)"></div>
    <!-- /Backgound Image -->

    <div class="container">
      <div class="row">
        <div class="col-md-10 col-md-offset-1 text-center">
          <ul class="hero-area-tree">
            <li><a href="login.php">Login</a></li>
          </ul>
          <h1 class="white-text">Admin Login Page</h1>

        </div>
      </div>
    </div>

  </div>
  <!-- /Hero-area -->

  <!-- End breadcrumb -->

  <!-- Start contact  -->
  <section id="mu-contact">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="mu-contact-area">
            <!-- start title -->
            <div class="mu-title">
              <h2>Enter your credentials:</h2>
            </div>
            <!-- end title -->
            <!-- start contact content -->
            <div class="mu-contact-content">
              <div class="row">
                <div class="col-md-6">
                  <div class="mu-contact-left">
                    <span id="admin_error" style="display: none; color:red;"></span>
                    <span id="admin_message" style="display: none; color:#17B6BB;"></span>
                    <form class="contactform" class="form-horizontal" method="post" action="" id="admin_login_form" name="admin_login_form" role="form">
                      <div class="form-group">

                        <!--- Usernamee---->

                        <label for="StudentName" class="col-sm-3 control-label">
                          <font color="red"><b>* </b></font>Email:
                        </label>
                        <div class="col-sm-9">
                          <input type="email" id="email" name="email" placeholder="email" class="form-control" autofocus required>
                        </div>
                      </div>
                      <div class="form-group">

                        <!--- Password---->

                        <label for="StudentName" class="col-sm-3 control-label">
                          <font color="red"><b>* </b></font>Password:
                        </label>
                        <div class="col-sm-9">
                          <input type="password" id="password" name="password" class="form-control" required>
                        </div>
                      </div>
                      <div class="form-group">

                        <!--- Password---->

                        
                        <div class="col-sm-9">
                          <br/>
                        </div>
                      </div>
                      <div class="form-group">
                          <div class="col-sm-12">
                      <button type="submit" id="submit_application" class="btn btn-primary btn-block">Login</button>
                          </div>
                      </div>
                      <div class="form-group">
                      <div class="col-sm-12">
                      <a href="forgot_password.php">Forgot Your password?</a>
                      </div>

                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <!-- end contact content -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End contact  -->


  <?php include('include/footer.php'); ?>

  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>

  <!-- Custom js -->
  <script src="assets/js/custom.js"></script>
  <script type="text/javascript">
    $('#admin_login_form').submit(function(e) {
        e.preventDefault();
    
        // ajax
        $.ajax({
          type: "POST",
          url: "functions/login_authenticate.php",
          data: $(this).serialize(), // get all form field value in serialize form
          success: function(result) {
            var json = $.parseJSON(result);
            if (json.response.code == "1") {
              $("#admin_message").fadeIn().text(json.response.message);
              setTimeout(function() {
                window.location.replace("home.php");
              }, 3000);
            } else
              $("#admin_error").fadeIn().text(json.response.status + ": " + json.response.message);
              setTimeout(function() {
                $("#admin_error").fadeOut().empty();
              }, 5000);
          }
        });
       });
  </script>

</body>

</html>