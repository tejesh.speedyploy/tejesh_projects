<?php include('include/session.php');
if (!isset($_SESSION['admin_user'])) {
  header("Location: login.php"); 
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Goodwill college |Contact</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

    <!-- Font awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">   
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">          
    <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

    <!-- Main style sheet -->
    <link href="assets/css/style.css" rel="stylesheet">    

   
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>

<?php include('include/header.php');?>     
  
  <!-- Hero-area -->
		<div class="hero-area section">

			<!-- Backgound Image -->
			<div class="bg-image bg-parallax overlay" style="background-image:url(assets/img/counter-bg.jpg)"></div>
			<!-- /Backgound Image -->

			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1 text-center">
						<ul class="hero-area-tree">
							<li><a href="index.php">Home</a></li>
							<li>Reset Password</li>
						</ul>
						<h1 class="white-text">Reset Password</h1>

					</div>
				</div>
			</div>

		</div>
		<!-- /Hero-area -->

 <!-- End breadcrumb -->

 <!-- Start contact  -->
 <section id="mu-contact">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-contact-area">
          <!-- start title -->
          <div class="mu-title">
            <h2>Reset Your Password:</h2>
            
          </div>
          <!-- end title -->
          <!-- start contact content -->
          <div class="mu-contact-content">           
            <div class="row">
              <div class="col-md-6">
                <div class="mu-contact-left">
                <span id="reset_error" style="display: none; color:red;"></span>
                <span id="reset_message" style="display: none; color:#17B6BB;"></span>
                  <form class="contactform" id="reset_password">                  
                  <div class="form-group">
            <input type="password" id="signup_password" name="signup_password" class="form-control" placeholder="Password" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" onchange="if(this.checkValidity()) form.signup_confirm_password.pattern = this.value;" title="The string must contain at least 1 lowercase alphabetical character, 1 uppercase alphabetical character, 1 numeric character, one special character & eight characters or longer" required>
          </div>
          <div class="form-group">
            <input type="password" id="signup_confirm_password" name="signup_confirm_password" class="form-control" placeholder="Confirm Password" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="Match the above password" required>
          </div>
            <?php
            if (!empty($_GET['otd'])){
             echo '<input type="hidden" id="otd" name="otd" value="'.$_GET['otd'] .'" >';
            echo '<input type="hidden" id="reset_email" name="reset_email" value="'.$_GET['email'] .'" >';
            }
            ?>
                    <p class="form-submit">
                      <input type="submit" value="Reset Password" class="mu-post-btn" name="submit">
                    </p>        
                  </form>
                </div>
              </div>
            </div>
          </div>
          <!-- end contact content -->
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- End contact  -->
 

 <?php include('include/footer.php'); ?>
   
  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>  
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>   
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>  

  <!-- Custom js -->
  <script src="assets/js/custom.js"></script> 
  <script type="text/javascript">
  $('#reset_password').submit(function(e) {
        e.preventDefault();
    
        // ajax
        $.ajax({
          type: "POST",
          url: "functions/update_password.php",
          data: $(this).serialize(), // get all form field value in serialize form
          success: function(result) {
            console.log(result);
            var json = $.parseJSON(result);
            if (json.response.code == "1") {
              $("#reset_message").fadeIn().text(json.response.message);
              setTimeout(function() {
                window.location.href = "functions/sign_out.php";
              }, 5000);
            } else
              $("#reset_error").fadeIn().text(json.response.status + ": " + json.response.message);
              setTimeout(function() {
                $("#reset_error").fadeOut().empty();
              }, 5000);
          }
        });
    });
  </script>

  </body>
</html>