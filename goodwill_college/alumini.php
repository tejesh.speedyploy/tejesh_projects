<?php include('include/session.php');?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Goodwill college | Alumni </title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

    <!-- Font awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">   
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">          
    <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

    <!-- Main style sheet -->
    <link href="assets/css/style.css" rel="stylesheet">    

   
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>

<?php include('include/header.php');?>   
 <!-- End breadcrumb -->
 <section id="mu-course-content">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
              <div class="col-md-9">
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                        <figure class="mu-latest-course-img">
                          <a href="#"><img src="images/alumni1.jpg" alt="img"></a>
                         
                        </figure>
                        
                        <div class="mu-title">
                        <h2>Alumni</h2>
 <br>                             
<h4 style="text-decoration:underline">A brief overview of the Alumni Association.</h4>

<p>The Alumni Association of Goodwill Christian College was formed in 2009 with the aim of keeping alive in the hearts of the Alumni memories of the College, its ideals and values and also to remind the College of its daughters who are scattered all over the world. </p><p>  It also aims to strengthen the interaction between the College and its Alumni and to establish a mutually beneficial relationship that would provide a forum for career networking and mentoring to both the Alumni and the current students.</p>

<p><strong>The Objectives of the association are as follows:</strong></p>
<ul style="line-height:30px">
<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> 	To promote effectively the welfare and interest of the College through its Alumni.</li>
<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> 	To support the vision and mission of the College and uphold its reputation  as ambassadors of the College.</li>
<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> 	To organize and co-ordinate re-union activities   of the Alumni.</li>
</ul>
<br>
<img src="images/alumni2.jpg" alt="img">
<br>
<br>
<h4 style="text-decoration:underline">Contributions of   the Alumni in the last five years:</h4>
<p>The Alumni were encouraged to contributee generously  to their <em>Alma Mater</em>.</p>
<p>The alumni sponsor   Endowment prizes every year during the    Graduation of the Outgoing batch</p>
<p> <em><strong> Given below are  the contributions by the Alumni.</strong></em></p>
<ul style="line-height:30px">
	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Mrs. Eshwari Richard from BA, Class of 2013 instituted a trophy for the 'Best student in Journalism' in memory of her beloved mother</li>
	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i>  Ms. Mohana Priya of B.Com, Class of 2013 instituted a trophy for the 'Best student in Civil Defence ' in honour of Dr. Gayathri, Lecturer in Botany and NSS Co- ordinator</li>
	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms. Naila of BBA, Ms. Noor Siddiqua and Ms. Angel of BA, Class of 2017 and Afshan Tabassum of B.Com, Class of 2017 donated books to the College Library.</li>
	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Mrs. Saadia Rehman, Lecturer in Commerce, and Alumnus- B.Com, Class of 2008 sponsored textbooks for a B.Com Student</li>
	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Mrs. Asma, HOD of Commerce, , and Alumnus- B.Com,  Class of 2003 instituted a trophy for the 'Best Student in Accounts' in honour of her father Mr. Abdul Azeez</li>
	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms. Nikitha Sneha Priya, BBA, Class of 2016 instituted a trophy for the ' Best Dancer' of the outgoing class in honour of Ms. Gracy,Lecturer in Commerce</li>
	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms. Ayesha Tasneem, BA, Class of 2018 instituted a trophy for the ' Best Outstanding Student in Arts ' in honour of her parents Mr. Chand Pasha and Mrs Naseem</li>
	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms. Umme Sufiya , BA, Class of 2018 instituted a trophy for the ' Best Student in Literature ' of the outgoing Class in honour of her nephew Master Monhammed Faizan</li>
	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i>  Ms. Neethu , BA,   Class of 2016 instituted  a cash prize for the 'Best Student in Psychology' </li>

	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms. Noor Siddiqua ,  BA , Class of 2017 donated Cash for the purchase of stop clocks in the Psychology lab.</li>
	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms. Paveena, B.Com class of 2010 donated Rs 10,000/ for the College</li>
	

</ul>
<br>
<img src="images/alumini.jpg" alt="img">
<br>
<br>

                       <h4 style="text-decoration:underline">    Alumni  Occupying Prominent Positions. </h4>
                       <ul style="line-height:30px">
 <li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms.Anusha Sheik, B.Com Class of 2016, based in Goa is one of the leading fashion designers in India. She has hosted many Fashion shows, notable among them is Runway Inspirations- A fashion show with a difference that involved Physically Challenged Children walking the ramp. She has also created her own designer brand ' <strong>Anushas</strong>' dealing with women and kids wear for all occasions</li> 
 <li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> 	Ms. Shama Patel is a bestselling author.</li>
 	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms. Nishat Syed is Founder and owner  at Naina Boutique and Nishat Creations.</li>
 	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms.Priyadarshini Kumerasan    is a Creative Strategist  at Yahoo.</li>
 	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms.Paveen  is a   Senior Analyst at ANZ Australia</li>
 	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms. Trupthi jaikumar is lead Cabin Crew at IndiGo Airlines</li>
 	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms. Cynthia madan is   a lead Cabin Crew at Qatar Airways</li>
 	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms. Krishna priya  is a Research & Media Analyst  at Genpact</li>
 	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms. Sarah jemima  works for  Google.</li>
 	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Mrs. Thilini Glen—is professional counselor & Lecturer at Institute of Psychological Studies ,Sri Lanka</li>
 	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms.  Vasavi -Senior Finance Associate at DXC Technology</li>
 	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i>  Ms. Tasmiya  Begum works as   Make up   Artist</li>
 	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms.Suman .K    is  a Financing Analyst</li>
 	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms. Meetu  Sobhani is a  Travel  Auditor</li>
 	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms Sharon Rebecca   works with  Con –Centrix</li>
 	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Mrs. Lakshmi Thomas is an Asst Prof. In the Dept. of Journalism in Bangalore University</li>
 	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms. R. Gayathri   works at  Fitness Manager at  Power World Fitness.</li>
 	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms. Deepu Sharmila is a teacher at 'Founding years of Learning'</li>
 	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms  Saima ia a teacher at  Wisdom International School.</li>
 	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> Ms Daphni  works as a  teacher  in Spectrum.</li>
</ul>
<br>
   <p> In addition to the   above, many of our Alumni   are pursuing  higher  studies  and work in various corporate sectors.</p>





                  
                        </div>
                       
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
              <div class="col-md-3">
                <!-- start sidebar -->
                <aside class="mu-sidebar">
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                  <!--  <h3>Quick Links</h3>-->
                  <ul class="mu-sidebar-catg">

            <!--  <li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> <a href="graduate.php">Graduation</a></li>
             <li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> <a href="invest.php">Investiture</a></li>
             <li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> <a href="heyah.php">Heyah</a></li>
              	<li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> <a href="rhapsody.php">Rhapsody</a></li>    
    <li>  <i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> <a href="christmas.php">Christmas</a></li>-->
</ul>  
                  </div>
                  <!-- end single sidebar -->
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Latest News</h3>
                    <div class="mu-sidebar-popular-courses">
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/1.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Christmas Day</a></h4>                      
                        
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/2.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Annual day</a></h4>                      
                          
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/3.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Students showcasing talent</a></h4>                      
                         
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end single sidebar -->
                 
                  
                </aside>
                <!-- / end sidebar -->
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
 
 

 
 
 
 <?php include('include/footer.php'); ?>
  
  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>  
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>   
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>  

  <!-- Custom js -->
  <script src="assets/js/custom.js"></script> 

  </body>
</html>