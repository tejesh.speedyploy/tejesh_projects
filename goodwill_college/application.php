<?php include('include/session.php'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Goodwill college |Application</title>

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

  <!-- Font awesome -->
  <link href="assets/css/font-awesome.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <!-- Slick slider -->
  <link rel="stylesheet" type="text/css" href="assets/css/slick.css">
  <!-- Theme color -->
  <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

  <!-- Main style sheet -->
  <link href="assets/css/style.css" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
  <style>
    #targetLayer {
      float: left;
      width: 150px;
      height: 150px;
      text-align: center;
      line-height: 150px;
      font-weight: bold;
      color: #C0C0C0;
      background-color: #F0E8E0;
      border-bottom-left-radius: 4px;
      border-top-left-radius: 4px;
    }

    #uploadFormLayer {
      float: left;
      padding: 20px;
    }
  </style>


  <!-- Google Fonts -->
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
  
  <?php include('include/header.php'); ?>
  <!-- End breadcrumb -->
  <section id="mu-course-content">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="mu-course-content-area">
            <div class="row">
              <div class="col-md-9">
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                        <figure class="mu-latest-course-img">
                          <a href="#"><img src="images/application.jpg" alt="img"></a>

                        </figure>
                        <div class="mu-title">
                          <h2>Application Form</h2>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
                <!-- end course content container -->



              </div>
              <div class="col-md-3">
                <!-- start sidebar -->
                <aside class="mu-sidebar">
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Quick Links</h3>
                    <ul class="mu-sidebar-catg">
                      <li><a href="course.php">Courses</a></li>
                      <li><a href="application.php">Application Form</a></li>
                      <li><a href="procedure.php">Rules & Regulations</a></li>
                      <li><a href="scholarship.php">Scholarship</a></li>
                    </ul>
                  </div>
                  <!-- end single sidebar -->
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Latest News</h3>
                    <div class="mu-sidebar-popular-courses">
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/1.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Christmas Day</a></h4>

                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/2.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Annual day</a></h4>

                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/3.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Students showcasing talent</a></h4>

                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end single sidebar -->


                </aside>
                <!-- / end sidebar -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>




  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="mu-course-content-area">

            <div class="row">


              <div class="col-md-12">

                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">


                        <div class="mu-latest-course-single-content">




                          <form class="form-horizontal" method="post" action="application_submission.php" id="application_form" name="application_form" role="form" enctype="multipart/form-data">

                            <!--- Image Header Name---->
                            <div class="form-group">

                              <img src="images/admissionheader.jpg">

                            </div>
                            <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">Note:</label>
                              <div class="col-sm-9">
                                <label for="Note"><font color="black"><b>All Fields Marked with </b> </font> <font color="red"><b>* </b></font><font color="black"><b>are mandatory to fill-in or application won't be considered/registered with us!!!! </b> <b> Only We accept .jpg,.png,.bmp,.jpeg file extensions for all uploads</label>
                              </div>
                           </div>
                            <?php
                            ini_set('display_errors', 1);
                            ini_set('display_startup_errors', 1);
                            error_reporting(E_ALL);
                            require_once "functions/config.php";
                            $last_id = mysqli_query($conn, "SELECT application_id, YEAR(date) from application ORDER BY id DESC LIMIT 1;");
                            if (mysqli_num_rows($last_id) > 0) {
                                // output data of each row
                                while($last_insert_id = mysqli_fetch_assoc($last_id)) {
                                  //echo "id: '" . $last_insert_id["id"]. "'";
                                  //$increment_value =(int) $last_insert_id['id'];
                                  $application_id = $last_insert_id['application_id'] + 1;
                                  $year = date("Y") . "-" . date('Y', strtotime('+1 years'));
                                  if ($last_insert_id['YEAR(date)'] != date("Y")){
                                    //mysqli_query($conn,"ALTER TABLE registrations AUTO_INCREMENT = 0001;");
                                    $application_id = '0001';
                                    $year = date("Y") . "-" . date('Y', strtotime('+1 years'));
                                  }
                                  //if(mysqli_query($conn,"ALTER TABLE registrations AUTO_INCREMENT =  $increment_value;"));
                              } 
                            }
                            else{
                              $application_id = '0001';
                              $year = date("Y") . "-" . date('Y', strtotime('+1 years'));
                            }
                            //mysqli_close($conn);
                            ?>
                            

                            <h3 class="center" style="color: #008000; font-style:normal;">Important Details</h3>
                            <!--- Upload Photo---->
                            <div class="form-group">

                              <label for="Admission" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Upload photo:
                              </label>
                              <div class="col-sm-3">
                                <input type="file" id="profile_picture" accept="image/*" name="profile_picture" class="form-control" onchange="loadFile(event)" accept=".jpg,.png,.bmp,.jpeg" required>
                              </div>

                              <div id="targetLayer"><img height="150" width="150" id="output" /></div>
                            </div>


                            <!--- Application No---->
                            <div class="form-group">


                              <label for="Admission" class="col-sm-3 control-label">Year of Admission:</label>
                              <div class="col-sm-3">

                                <input type="text" id="admission_year" name="admission_year" value="<?php echo $year;  ?>" class="form-control" readonly="readonly">
                              </div>
                              <div class="col-sm-6">
                                <label for="ApplicationNo" class="col-sm-6 control-label">Application No:</label>
                                <div class="col-sm-3">
                                  <input type="text" id="application_number" name="application_number" value="<?php echo str_pad($application_id, 4, "0", STR_PAD_LEFT); ?>" class="form-control" readonly="readonly">

                                </div>

                              </div>
                            </div>


                            <div class="form-group">

                              <!--- Student Name---->

                              <label for="StudentName" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Student's Name:
                              </label>
                              <div class="col-sm-9">
                                <span class="help-block">(As per certificate in BLOCK LETTERS)</span>
                                <input type="text" id="StudentName" name="StudentName" placeholder="Student's Name" class="form-control" onkeyup="this.value = this.value.toUpperCase();" autofocus required>

                              </div>
                            </div>
                            <hr style="border:3px solid #eee" class="separate">
                            <h3 class="center" style="color: #008000; font-style:normal;">Father Details</h3>
                            <font color="red"><b id="parental_detail"></b></font>
                            <font color="red"><b id="name_error"></b></font>
                            <font color="red"><b id="occupation_error"></b></font>
                            <font color="red"><b id="annual_income_error"></b></font>
                            <font color="red"><b id="mobile_error"></b></font>
                            

                            <!--- Fathers Name---->
                            <div class="form-group">

                              <label for="FathersName" class="col-sm-3 control-label">Father's Name:</label>
                              <div class="col-sm-9">
                                <span class="help-block">(In BLOCK LETTERS)</span>
                                <input type="text" id="FathersName" name="FathersName" placeholder="Father's Name" class="form-control" onkeyup="this.value = this.value.toUpperCase();">
                                <font color="red"><b id="father_name_error"></b></font>
                              </div>
                            </div>





                            <!--- Father's Address---->
                            <div class="form-group">
                              <label for="Occupation" class="col-sm-3 control-label">Occupation:</label>
                              <div class="col-sm-9">

                                <input type="text" id="FatherOccupation" name="FatherOccupation" placeholder="Occupation" class="form-control">

                              </div>

                            </div>
                            <div class="form-group">

                              <label for="Office Address" class="col-sm-3 control-label">Office Address:</label>
                              <div class="col-sm-9">

                                <textarea class="form-control" id="FatherOfficeAddress" name="FatherOfficeAddress" rows="3" placeholder="Do not exceed more than 160 characters" maxlength="160" title="Only 160 Characters!!!"></textarea>

                              </div>

                            </div>


                            <!--- Annual Income---->
                            <div class="form-group">

                              <label for="AnnualIncome" class="col-sm-3 control-label">Annual Income:</label>
                              <div class="col-sm-9">

                                <input type="number" id="FatherAnnualIncome" name="FatherAnnualIncome" placeholder="Annual Income" max="999999999" Title="Only Numbers & in rupees only" class="form-control">

                              </div>
                            </div>
                            <div class="form-group">

                              <label for="AnnualIncome" class="col-sm-3 control-label">Email:</label>
                              <div class="col-sm-9">

                              <input type="email" class="form-control" name="FatherEmail" id="FatherEmail" placeholder="Enter Email">

                              </div>
                            </div>

                            


                            <!--- Mobile No---->
                            <div class="form-group">




                              <label for="MobileNo" class="col-sm-3 control-label">Mobile No:</label>
                              <div class="col-sm-3">

                                <input type="text" id="FatherMobileNo" name="FatherMobileNo" placeholder="Mobile No" class="form-control" pattern="(6|7|8|9)\d{9}" title="Mobile number should only contain 10 numbers & starting with 6,7,8,9 eg:6123456789">

                              </div>



                              <div class="col-sm-6">
                                <label for="TelephoneNo" class="col-sm-3 control-label">Tel No (Office):</label>
                                <div class="col-sm-9">

                                  <input type="text" id="FatherTelNo" name="FatherTelNo" placeholder="Telephone No" class="form-control">

                                </div>

                              </div>
                            </div>


                            <hr style="border:3px solid #eee">
                            <h3 class="center" style="color: #008000; font-style:normal;">Mother Details</h3>


                            <!--- Mothers Name---->
                            <div class="form-group">

                              <label for="MothersName" class="col-sm-3 control-label">Mother's Name:</label>
                              <div class="col-sm-9">
                                <span class="help-block">(In BLOCK LETTERS)</span>
                                <input type="text" id="MothersName" name="MothersName" placeholder="Mother's Name" class="form-control" onkeyup="this.value = this.value.toUpperCase();">
                                <font color="red"><b id="mother_name_error"></b></font>
                              </div>
                            </div>






                            <!---Address---->
                            <div class="form-group">
                              <label for="Occupation" class="col-sm-3 control-label">Occupation:</label>
                              <div class="col-sm-9">

                                <input type="text" id="MotherOccupation" name="MotherOccupation" placeholder="Occupation" class="form-control">

                              </div>

                            </div>

                            <div class="form-group">
                              <label for="Office Address" class="col-sm-3 control-label">Office Address:</label>
                              <div class="col-sm-9">
                                <textarea class="form-control" id="MotherOfficeAddress" name="MotherOfficeAddress" rows="3" placeholder="Do not exceed more than 160 characters" maxlength="160" title="Only 160 Characters!!!"></textarea>
                              </div>
                            </div>



                            <!--- Annual Income---->
                            <div class="form-group">

                              <label for="AnnualIncome" class="col-sm-3 control-label">Annual Income:</label>
                              <div class="col-sm-9">

                                <input type="text" id="MotherAnnualIncome" name="MotherAnnualIncome" placeholder="Annual Income" class="form-control">

                              </div>
                            </div>

                            <div class="form-group">

                              <label for="AnnualIncome" class="col-sm-3 control-label">Email:</label>
                              <div class="col-sm-9">

                              <input type="email" class="form-control" name="MotherEmail" id="MotherEmail" placeholder="Enter Email">

                              </div>
                            </div>


                            <!--- Mobile No---->
                            <div class="form-group">

                              <label for="MobileNo" class="col-sm-3 control-label">Mobile No:</label>
                              <div class="col-sm-3">

                                <input type="text" id="MotherMobileNo" name="MotherMobileNo" placeholder="Mobile No" class="form-control" pattern="(6|7|8|9)\d{9}" title="Mobile number should only contain 10 numbers & starting with 6,7,8,9 eg:6123456789">

                              </div>

                              <div class="col-sm-6">
                                <label for="TelephoneNo" class="col-sm-3 control-label">Tel No (Office):</label>
                                <div class="col-sm-9">

                                  <input type="text" id="MotherTelNo" name="MotherTelNo" placeholder="Telephone No" class="form-control">

                                </div>

                              </div>
                            </div>


                            <hr style="border:3px solid #eee">

                            <h3 class="center" style="color: #008000; font-style:normal;">Guardian Details</h3>

                            <!--- Guardians Name---->
                            <div class="form-group">

                              <label for="GuardiansName" class="col-sm-3 control-label">Guardian's Name:</label>
                              <div class="col-sm-9">
                                <span class="help-block">(In BLOCK LETTERS)</span>
                                <input type="text" id="GuardiansName" name="GuardiansName" placeholder="Guardian's Name" class="form-control" onkeyup="this.value = this.value.toUpperCase();">
                                <font color="red"><b id="guardian_name_error"></b></font>
                              </div>
                            </div>




                            <div class="form-group">
                              <label for="Occupation" class="col-sm-3 control-label">Occupation:</label>
                              <div class="col-sm-9">

                                <input type="text" id="GuardianOccupation" name="GuardianOccupation" placeholder="Occupation" class="form-control">

                              </div>

                            </div>

                            <!---Address---->
                            <div class="form-group">

                              <label for="Office Address" class="col-sm-3 control-label">Office Address:</label>
                              <div class="col-sm-9">

                                <textarea class="form-control" id="GuardianOfficeAddress" name="GuardianOfficeAddress" rows="3" placeholder="Do not exceed more than 160 characters" maxlength="160" title="Only 160 Characters!!!"></textarea>
                              </div>

                            </div>


                            <div class="form-group">

                              <label for="AnnualIncome" class="col-sm-3 control-label">Annual Income:</label>
                              <div class="col-sm-9">

                                <input type="text" id="GuardianAnnualIncome" name="GuardianAnnualIncome" placeholder="Annual Income" class="form-control">

                              </div>
                            </div>

                            <div class="form-group">

                              <label for="AnnualIncome" class="col-sm-3 control-label">Email:</label>
                              <div class="col-sm-9">

                              <input type="email" class="form-control" name="GuardianEmail" id="GuardianEmail" placeholder="Enter Email">

                              </div>
                            </div>


                            <!--- Mobile No---->
                            <div class="form-group">

                              <label for="MobileNo" class="col-sm-3 control-label">Mobile No:</label>
                              <div class="col-sm-3">

                                <input type="text" id="GuardianMobileNo" name="GuardianMobileNo" placeholder="Mobile No" class="form-control" pattern="(6|7|8|9)\d{9}" title="Mobile number should only contain 10 numbers & starting with 6,7,8,9 eg:6123456789">

                              </div>

                              <div class="col-sm-6">
                                <label for="TelephoneNo" class="col-sm-3 control-label">Tel No (Office):</label>
                                <div class="col-sm-9">

                                  <input type="text" id="GuardianTelNo" name="GuardianTelNo" placeholder="Telephone No" class="form-control">

                                </div>

                              </div>
                            </div>

                            <hr style="border:3px solid #eee">
                            <h3 class="center" style="color: #008000; font-style:normal;">Student Details</h3>

                            <!---Address---->
                            <div class="form-group">

                              <label for="Address" class="col-sm-3 control-label"><font color="red"><b>* </b></font>Current/Local Address:</label>
                              <div class="col-sm-9">

                                <textarea class="form-control" id="CurrentAddress" name="CurrentAddress" rows="3" placeholder="Do not exceed more than 160 characters" maxlength="160" title="Only 160 Characters!!!" required></textarea>
                              </div>

                            </div>
                            <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label"><input type="checkbox" name="same_as_current_address" id="same_as_current_address" value="same"></label>
                              <div class="col-sm-9">
                                <label for="Note"> If Permanent Address is same as above Current Address</label>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="Address" class="col-sm-3 control-label"><font color="red"><b>* </b></font>Permanent Address:</label>
                              <div class="col-sm-9">
                                <textarea class="form-control" id="PermanentAddress" name="PermanentAddress" rows="3" placeholder="Do not exceed more than 160 characters" maxlength="160" title="Only 160 Characters!!!"></textarea>
                              </div>
                            </div>

                            <!--- Student Date of Birth--->


                            <div class="form-group">
                              <label for="birthDate" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Date of Birth of Student(As per X Std Certified):
                              </label>
                              <div class="col-sm-9">
                                <div class='input-group date'>
                                  <input type='date' id="DOB" name="DOB" class="form-control" max="<?php echo date('Y-m-d', strtotime('-14 years')); ?>" required>
                                </div>
                              </div>
                            </div>



                            <!--- Nationality Religion--->


                            <div class="form-group">
                              <label for="email" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Nationality:
                              </label>
                              <div class="col-sm-3">
                                <input type="Nationality" id="Nationality" name="Nationality" placeholder="Nationality" class="form-control" required>
                              </div>

                              <label for="email" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Religion:
                              </label>
                              <div class="col-sm-3">
                                <input type="Religion" id="Religion" name="Religion" placeholder="Religion" class="form-control" required>
                              </div>

                            </div>





                            <div class="form-group">
                              <label for="Denomination" class="col-sm-3 control-label">If Christian (mention Denomination):</label>
                              <div class="col-sm-9">
                                <input type="Denomination" id="Denomination" name="Denomination" placeholder="Denomination" class="form-control">
                              </div>
                            </div>

                            <!--- f--->

                            <div class="form-group">
                              <label for="Category" class="col-sm-3 control-label"><font color="red"><b>* </b></font>Category:</label>
                              <div class="col-sm-9">
                                <select id="category" name="category" class="form-control" onchange='DisplayCategoryTextBox(this.value);' required>
                                  <option selected value="">Please Select Category</option>
                                  <option value="SC">SC</option>
                                  <option value="ST">ST</option>
                                  <option value="BC">BC</option>
                                  <option value="Others">Others</option>
                                  <option value="Category 1">Category 1</option>
                                  <option value="2A">2A</option>
                                  <option value="2B">2B</option>
                                  <option value="3A">3A</option>
                                  <option value="3B">3B</option>
                                </select>
                                <font color="red"><b id="category_other_text_error"></b></font>
                                <input type="text" name="category-textbox" id="category-textbox" style='display:none;' placeholder="Others, please specify here" onfocusout='CategoryOtherTextBox(this.value);' class="form-control"/>
                              </div>
                            </div> <!-- /.form-group -->


                            <!-- /.Courses Offered -->


                            <div class="form-group">
                              <label for="Courses Offered" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Select your Course:
                              </label>
                              <div class="col-sm-9">
                                <select id="course-selector" name="course-selector" class="form-control" required>
                                  <option selected value="">Please Select Course</option>
                                  <option value="BA">B.A</option>
                                  <option value="BSc">B.Sc</option>
                                  <option value="BCom">B.Com</option>
                                  <option value="BBA">BBA</option>
                                  <option value="BCA">BCS</option>
                                </select>
                              </div>
                            </div>

                            <div class="form-group">

                              <label for="Courses Offered" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Select Major Subject:
                              </label>
                              <div class="col-sm-9">
                                <select id="majors-selector" name="majors-selector" class="form-control" required>
                                  <option>Please choose from above courses first</option>
                                </select>
                              </div>
                            </div>



                            <!--- second language--->

                            <div class="form-group">
                              <label for="Second Languages Offered" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Select your Second Language:
                              </label>
                              <div class="col-sm-9">
                                <select id="second-language-selector" name="second-language-selector" class="form-control" required>
                                  <option selected value="">Please Select Second Language: </option>
                                  <option value="Kannada">Kannada</option>
                                  <option value="Hindi">Hindi</option>
                                  <option value="Tamil">Tamil</option>
                                  <option value="Urdu">Urdu</option>
                                  <option value="Additional English">Additional English (only if you have previous study records)</option>
                                  <option value="French">French (only if you have previous study records)</option>
                                </select>
                                <font color="brown">Its good to select the previously studied languages for considering the application</font>
                              </div>
                            </div>


                            <hr style="border:3px solid #eee">
                            <h3 class="center" style="color: #008000; font-style:normal;">Student's Profile</h3>



                            <!--- Student Profile--->


                            <div class="form-group">
                              <label class="control-label col-sm-3">
                                <font color="red"><b>* </b></font>Examination Passed:
                              </label>
                              <div class="col-sm-9">
                                <select id="examination-passed" name="examination-passed" class="form-control" onchange='DisplayTextBox(this.value);' required>
                                  <option selected value="">Please Select</option>
                                  <option value="PUC">PUC</option>
                                  <option value="ISC">ISC</option>
                                  <option value="CBSE">CBSE</option>
                                  <option value="Others">Others</option>
                                </select>
                                <font color="red"><b id="exam_other_text_error"></b></font>
                                <input type="text" name="examination-passed-textbox" id="examination-passed-textbox" style='display:none;' placeholder="Others, please specify here" onfocusout="ExamOtherTextBox(this.value);" class="form-control" />
                              </div>
                            </div> <!-- /.form-group -->



                            <div class="form-group">
                              <label for="Marks Sheet" class="col-sm-3 control-label"><font color="red"><b>* </b></font>Fill Your Marks:</label>
                              <div class="col-sm-9">
                              <font color="red"><b id="marks_error"></b></font>

                                <a class="btn btn-primary pull-right add-record" data-added="0"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add Subject</a>
                                <div style="display: none;">
                                  <table id="sample_table">
                                    <tr id="">
                                      <td> <input type="text" name="subject[]" id="subject[]" placeholder="Enter Subject Name" class="form-control" ></td>
                                      <td> <input type="number" name="MaxMarks[]" id="MaxMarks[]" placeholder="Enter Max Marks" class="form-control max_marks" max="125"></td>
                                      <td> <input type="number" name="MarksObtained[]" id="MarksObtained[]" placeholder="Enter Marks Obtained" class="form-control sub_marks" max="125"></td>
                                      <td><a class="btn btn-xs delete-record" data-id="0"><i class="glyphicon glyphicon-trash"></i></a></td>
                                    </tr>
                                  </table>
                                </div>
                                <table class="table table-bordered" id="tbl_posts">
                                  <thead>
                                    <tr>
                                      <th>Subject</th>
                                      <th>Max. Marks</th>
                                      <th>Marks Obtained</th>
                                    </tr>
                                  </thead>
                                  <tbody id="tbl_posts_body">
                                    <div class="field_wrapper">

                                      <!---Table Generates here for Subjects --->
                                    </div>

                                  </tbody>
                                </table>

                              </div>
                            </div>

                            <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">Total Max. Marks (changes as you add subject):</label>
                              <div class="col-sm-9">
                                <input type="text" id="total_max_marks" name="total_max_marks" class="form-control" readonly="readonly">
                              </div>
                            </div>

                            <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">Total Marks (changes as you add subject):</label>
                              <div class="col-sm-9">
                                <input type="text" id="total_marks" name="total_marks" class="form-control" readonly="readonly">
                              </div>
                            </div>


                            <div class="form-group">
                              <label for="Sports Acheivement" class="col-sm-3 control-label">Sports Represented:</label>
                              <div class="col-sm-9">
                                <a class="btn btn-primary pull-right add-sports-record" data-added="0"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add Details:</a>
                                <div style="display: none;">
                                  <table id="sports_table">
                                    <tr id="">
                                      <td> <input type="text" name="sport_name[]" id="sport_name[]" placeholder="Enter Sport Name" class="form-control"></td>
                                      <td> <select id="sports-level[]" name="sports-level[]" class="form-control">
                                          <option selected>Select your Level Acheived</option>
                                          <option value="district">District Level</option>
                                          <option value="state">State Level</option>
                                          <option value="national">National Level</option>
                                        </select></td>
                                      <td> <input type="file" name="certificates[]" id="certificates[]" placeholder="Upload your certificate scan copy" class="form-control" accept=".jpg,.png,.bmp,.jpeg"></td>
                                      <td><a class="btn btn-xs delete-sports-record" data-id="0"><i class="glyphicon glyphicon-trash"></i></a></td>
                                    </tr>
                                  </table>
                                </div>
                                <table class="table table-bordered" id="tbl_sports_posts">
                                  <thead>
                                    <tr>
                                      <th>Sport Name</th>
                                      <th>Level Acheived</th>
                                      <th>Upload Certificate</th>
                                    </tr>
                                  </thead>
                                  <tbody id="tbl_sports_posts_body">
                                    <div class="field_sports_wrapper">

                                      <!---Table Generates here for Subjects --->
                                    </div>

                                  </tbody>
                                </table>

                              </div>
                            </div>



                            <!--- sports---->


                            <div class="form-group">
                              <label for="Courses Offered" class="col-sm-3 control-label">Tick any two extra co-curricular activities you will be involved: </label>
                              <div class="col-sm-9">
                                <table class="table table-bordered">

                                  <tbody>
                                    <tr>
                                      <td>
                                        <div class="checkbox">
                                          <label><input type="checkbox" name="SCM" id="SCM" value="SCM">SCM </label>
                                        </div>
                                      </td>
                                      <td>
                                        <div class="checkbox">
                                          <label><input type="checkbox" name="Choir" id="Choir" value="Choir">Choir </label>
                                        </div>
                                      </td>
                                      <td>
                                        <div class="checkbox">
                                          <label><input type="checkbox" name="Civil_Defence" id="Civil_Defence" value="Civil Defence">Civil defence </label>
                                        </div>
                                      </td>

                                      <td>
                                        <div class="checkbox">
                                          <label><input type="checkbox" name="NSS" id="NSS" value="NSS">NSS </label>
                                        </div>
                                      </td>
                                      <td>
                                        <div class="checkbox">
                                          <label><input type="checkbox" name="Music" id="Music" value="Music">Musical Instruments </label>
                                        </div>
                                      </td>
                                      <td>
                                        <div class="checkbox">
                                          <label><input type="checkbox" name="Sports" id="Sports" value="Sports">Sports</label>
                                        </div>
                                      </td>

                                    </tr>



                                  </tbody>
                                </table>


                              </div>
                            </div>




                            <!---Address---->
                            <div class="form-group">

                              <label for="Office Address" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Name of the School/ Institution last studied:
                              </label>
                              <div class="col-sm-9">
                                <input type="Denomination" id="previous_institute" name="previous_institute" placeholder="Previous Institution Name" class="form-control" required>
                              </div>
                            </div>

                            <div class="form-group">
                              <label for="Office Address" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Address of the School/ Institution last studied:
                              </label>
                              <div class="col-sm-9">
                                <textarea class="form-control" id="PreviousInstituteAddress" name="PreviousInstituteAddress" rows="3" placeholder="Do not exceed more than 160 characters" maxlength="160" title="Only 160 Characters!!!" required></textarea> </div>
                            </div>
                            <div class="form-group">

                              <label for="Office Address" class="col-sm-3 control-label">
                                <font color="red"><b>* </b></font>Year of Passing:
                              </label>
                              <div class="col-sm-9">
                                <input type='number' id="year_of_passing" name="year_of_passing" min="<?php echo date('Y', strtotime('-35 years')); ?>" max="<?php echo date('Y', strtotime('0 years')); ?>" value="<?php echo date('Y', strtotime('0 years')); ?>" class="form-control" required>
                              </div>
                            </div>
                            <hr style="border:3px solid #eee">
                            <h3 class="center" style="color: #008000; font-style:normal;">DECLARATION</h3>
                            
                            <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label"><input type="checkbox" required><strong> <font color="red"><b>* </b></font>Declaration:</label>
                              <div class="col-sm-9">
                                I hereby promise to take responsibility in matters of academics, discipline and attendance for the holistic development of my ward and declare that the particulars furnished above are true and correct to the best of my knowledge.I promise to abide by the rules and regulations of the college.
                                I will accept the decision of the Principal and the Management in all matters of academic as final.</a>
                              </div>
                            </div>


                            
                        

                        <div class="form-group">
                          <div class="checkbox">
                            <div class="col-sm-12">
                              <label>
                                <div class="col-sm-6">
                                  <font color="red"><b>* </b></font>I,<input type="textbox" class="form-control" name="self_declaration" id="self_declaration" placeholder="Declare your name here" required>
                                </div>
                                shall be responsible for adhering minimum attendance of 75% in each subject, to appear for the Bangalore university exam,
                                falling which, I do agree that my daughter/ward will repeat the course (class) for the next academic year <?php echo date('Y', strtotime('+1 years')); ?> .
                                I will also agree that the admission will be cancelled if the furnished details in the application are found to be false or manipulated

                              </label>
                            </div>
                          </div>
                        </div> <!-- /.form-group -->
                        <div class="form-group">
                          <div class="col-sm-12">
                            <div class="col-sm-6">
                              <strong>Date:</strong> <?php echo date('d-m-Y'); ?>
                            </div>
                            <div class="col-sm-6">
                              <strong>
                                <font color="red"><b>* </b></font>Place:
                              </strong> <input type="textbox" class="form-control" name="place_declaration" id="place_declaration" placeholder="Your Place name" required>
                            </div>
                          </div>
                        </div>

                        <hr style="border:3px solid #eee">
                        <h3 class="center" style="color: #008000; font-style:normal;">Upload All Enclosures</h3>
                        <font color="red"><b id="signature_error"></b></font>
                      
                        <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">Father Signature:</label>
                              <div class="col-sm-9">
                                <input type="file" id="father_signature" name="father_signature" class="form-control" accept=".jpg,.png,.bmp,.jpeg">
                              </div>
                        </div>
                        <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">Mother Signature:</label>
                              <div class="col-sm-9">
                                <input type="file" id="mother_signature" name="mother_signature" class="form-control" accept=".jpg,.png,.bmp,.jpeg">
                              </div>
                        </div>
                        <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">Guardian Signature:</label>
                              <div class="col-sm-9">
                                <input type="file" id="guardian_signature" name="guardian_signature" class="form-control" accept=".jpg,.png,.bmp,.jpeg">
                              </div>
                        </div>
                        <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">Student Signature:</label>
                              <div class="col-sm-9">
                                <input type="file" id="student_signature" name="student_signature" class="form-control" accept=".jpg,.png,.bmp,.jpeg">
                              </div>
                        </div>
                        <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">Marks Card:</label>
                              <div class="col-sm-9">
                                <input type="file" id="marks_card" name="marks_card" class="form-control" accept=".jpg,.png,.bmp,.jpeg">
                              </div>
                        </div>
                        <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">TC (Transfer Certificate)/Migration :</label>
                              <div class="col-sm-9">
                                <input type="file" id="transfer_certificate" name="transfer_certificate" class="form-control" accept=".jpg,.png,.bmp,.jpeg">
                              </div>
                        </div>
                        <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">If SC/ST/BC, Please attach certificate here:</label>
                              <div class="col-sm-9">
                                <input type="file" id="obc_certificate" name="obc_certificate" class="form-control" accept=".jpg,.png,.bmp,.jpeg">
                              </div>
                        </div>
                        <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">Eligibility Criteria (Foreign Students):</label>
                              <div class="col-sm-9">
                                <input type="file" id="foreign_eligible" name="foreign_eligible" class="form-control" accept=".jpg,.png,.bmp,.jpeg">
                              </div>
                        </div>
                        <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">If CSI, Membership letter attested by the presbyter in-charge:</label>
                              <div class="col-sm-9">
                                <input type="file" id="csi_membership" name="csi_membership" class="form-control" accept=".jpg,.png,.bmp,.jpeg">
                              </div>
                        </div>
                        <div class="form-group">
                              <label for="Total" class="col-sm-3 control-label">Note:</label>
                              <div class="col-sm-9">
                                <label for="Note">1. Incomplete forms will not be considered for Admission.</label>
                                <label for="Note">2. Fee once paid, will not be refunded.</label>
                                <label for="Note">3. Admission is subject to the approval of bangalore university.</label>
                              </div>
                        </div>

                        
                        <div class="form-group">
                          <div class="col-sm-12">
                          

                            <?php
                            
                            $email_exists = mysqli_query($conn, "SELECT student_email from application where student_email='" . $_SESSION['login_email'] . "';");
                            
                            
                            if (!isset($_SESSION['login_user'])) {
                              echo  '<button type="submit" id="submit_application" class="btn btn-primary btn-block" title="Please register/login" disabled>Login/Signup First</button>';
                            } elseif (mysqli_num_rows($email_exists) > 0) {
                              echo  '<button type="submit" id="submit_application" class="btn btn-primary btn-block" disabled>Already Applied</button>';
                            }
                            else {
                              echo  '<button type="submit" id="submit_application" class="btn btn-primary btn-block">Apply with us!!!</button>';
                            } 
                            mysqli_close($conn);
                            ?>
                          </div>
                        </div>
                        </form>
                      </div> <!-- /.form-group -->


                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- end course content container -->



            </div>

          </div>
        </div>
      </div>
    </div>
    </div>
  </section>




  <?php include('include/footer.php'); ?>
  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>
  <!--<script type="text/javascript" src="assets/js/validate-form-fields.js"></script>-->


  <!-- Custom js -->

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet" />

  <script type="text/javascript" src="assets/js/application-form.js"></script>
</body>

</html>