<?php include('include/session.php'); 
if (!isset($_SESSION['login_user'])) {
  header("Location: index.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Goodwill college |Contact</title>

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

  <!-- Font awesome -->
  <link href="assets/css/font-awesome.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <!-- Slick slider -->
  <link rel="stylesheet" type="text/css" href="assets/css/slick.css">
  <!-- Theme color -->
  <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

  <!-- Main style sheet -->
  <link href="assets/css/style.css" rel="stylesheet">


  <!-- Google Fonts -->
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

  <?php include('include/header.php'); ?>

  <!-- Hero-area -->
  <div class="hero-area section">

    <!-- Backgound Image -->
    <div class="bg-image bg-parallax overlay" style="background-image:url(assets/img/counter-bg.jpg)"></div>
    <!-- /Backgound Image -->

    <div class="container">
      <div class="row">
        <div class="col-md-10 col-md-offset-1 text-center">
          <ul class="hero-area-tree">
            <li><a href="index.php">Home</a></li>
            <li>Your Application Status</li>
          </ul>
          <h1 class="white-text">Your Application Status</h1>

        </div>
      </div>
    </div>

  </div>
  <!-- /Hero-area -->

  <!-- End breadcrumb -->

  <!-- Start contact  -->
  <section id="mu-contact">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="mu-contact-area">
            <!-- start title -->
            <div class="mu-title">
              <h2>Your Application Status:</h2>
            </div>
            <!-- end title -->
            <!-- start contact content -->
            <div class="mu-contact-content">
              <div class="row">
                <div class="col-md-6">
                  <div class="mu-contact-left">
                    <?php

                    ini_set('display_errors', 1);
                    ini_set('display_startup_errors', 1);
                    error_reporting(E_ALL);
                    require_once "functions/config.php";
                    $app_data = mysqli_query($conn, "SELECT application_id, application_status from application where student_email='" . $_SESSION['login_email'] . "';");
                    if (mysqli_num_rows($app_data) > 0) {
                      // output data of each row
                      while ($application_data = mysqli_fetch_assoc($app_data)){
                        //echo "id: '" . $last_insert_id["id"]. "'";
                        //$increment_value =(int) $last_insert_id['id'];
                        $application_id = $application_data['application_id'];
                        $application_status = $application_data['application_status'];
                        echo '<div class="form-group">
                        <label>Application Number: ' . $application_id . ' </label>
                      </div>
                      <div class="form-group">
                        <label>Application Status: ' . $application_status . '</label>
                      </div>';

                      }
                    } else {
                      echo '<div class="form-group">
                        <label>Application Number: No Data Found</label>
                      </div>
                      <div class="form-group">
                        <label>Application Status: Not Applicable</label>
                      </div>';
                    }

                    ?>
                    


                  </div>
                </div>
              </div>
            </div>
            <!-- end contact content -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End contact  -->


  <?php include('include/footer.php'); ?>

  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>

  <!-- Custom js -->
  <script src="assets/js/custom.js"></script>

</body>

</html>