<?php include('include/session.php'); 
if (!isset($_SESSION['login_user'])) {
    header("Location: index.php");
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Goodwill college |Contact</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

    <!-- Font awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">
    <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

    <!-- Main style sheet -->
    <link href="assets/css/style.css" rel="stylesheet">


    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <?php include('include/header.php'); ?>

    <!-- Hero-area -->
    <div class="hero-area section">

        <!-- Backgound Image -->
        <div class="bg-image bg-parallax overlay" style="background-image:url(assets/img/counter-bg.jpg)"></div>
        <!-- /Backgound Image -->

        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 text-center">
                    <ul class="hero-area-tree">
                        <li><a href="index.php">Home</a></li>
                        <li>Your Application Status</li>
                    </ul>
                    <h1 class="white-text">Your Application Status</h1>

                </div>
            </div>
        </div>

    </div>
    <!-- /Hero-area -->

    <!-- End breadcrumb -->

    <!-- Start contact  -->
    <section id="mu-contact">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mu-contact-area">
                        <!-- start title -->
                        <div class="mu-title">
                            <h2>Your Application Status:</h2>
                        </div>
                        <!-- end title -->
                        <!-- start contact content -->
                        <div class="mu-contact-content">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mu-contact-left">
                                        <?php

                                        ini_set('display_errors', 1);
                                        ini_set('display_startup_errors', 1);
                                        error_reporting(E_ALL);
                                        require_once "functions/config.php";

                                        use PHPMailer\PHPMailer\PHPMailer;

                                        require_once('mail/PHPMailer.php');
                                        require_once('mail/SMTP.php');
                                        require_once('mail/Exception.php');
                                        $mail = new PHPMailer();
                                        $mail->CharSet =  "utf-8";
                                        $mail->IsSMTP();
                                        // enable SMTP authentication
                                        $mail->SMTPAuth = true;
                                        // GMAIL username
                                        $mail->Username = "noreply@goodwillchristiancollege.com";
                                        // GMAIL password
                                        $mail->Password = "pxsEksHugD";
                                        $mail->SMTPSecure = "ssl";
                                        // sets GMAIL as the SMTP server
                                        $mail->Host = "mail.goodwillchristiancollege.com";
                                        // set the SMTP port for the GMAIL server
                                        $mail->Port = "465";
                                        $mail->From = 'noreply@goodwillchristiancollege.com';
                                        $mail->FromName = 'Good Will Christian College';
                                        $mail->IsHTML(true);

                                        //print_r($_POST);
                                        //print("<pre>" . print_r($_POST, true) . "</pre>");
                                        //print("<pre>" . print_r($_FILES, true) . "</pre>");

                                        //$email = mysqli_real_escape_string($conn, $_POST['email']);

                                        if ($_SERVER["REQUEST_METHOD"] == "POST") {

                                            $admission_year = mysqli_real_escape_string($conn, $_POST['admission_year']);
                                            $application_number = mysqli_real_escape_string($conn, $_POST['application_number']);
                                            $StudentName = mysqli_real_escape_string($conn, $_POST['StudentName']);
                                            $FathersName = mysqli_real_escape_string($conn, $_POST['FathersName']);
                                            $FatherOccupation = mysqli_real_escape_string($conn, $_POST['FatherOccupation']);
                                            $FatherOfficeAddress = mysqli_real_escape_string($conn, $_POST['FatherOfficeAddress']);
                                            $FatherAnnualIncome = mysqli_real_escape_string($conn, $_POST['FatherAnnualIncome']);
                                            $FatherEmail = mysqli_real_escape_string($conn, $_POST['FatherEmail']);
                                            $FatherMobileNo = mysqli_real_escape_string($conn, $_POST['FatherMobileNo']);
                                            $FatherTelNo = mysqli_real_escape_string($conn, $_POST['FatherTelNo']);
                                            $MothersName = mysqli_real_escape_string($conn, $_POST['MothersName']);
                                            $MotherOccupation = mysqli_real_escape_string($conn, $_POST['MotherOccupation']);
                                            $MotherOfficeAddress = mysqli_real_escape_string($conn, $_POST['MotherOfficeAddress']);
                                            $MotherAnnualIncome = mysqli_real_escape_string($conn, $_POST['MotherAnnualIncome']);
                                            $MotherEmail = mysqli_real_escape_string($conn, $_POST['MotherEmail']);
                                            $MotherMobileNo = mysqli_real_escape_string($conn, $_POST['MotherMobileNo']);
                                            $MotherTelNo = mysqli_real_escape_string($conn, $_POST['MotherTelNo']);
                                            $GuardiansName = mysqli_real_escape_string($conn, $_POST['GuardiansName']);
                                            $GuardianOccupation = mysqli_real_escape_string($conn, $_POST['GuardianOccupation']);
                                            $GuardianOfficeAddress = mysqli_real_escape_string($conn, $_POST['GuardianOfficeAddress']);
                                            $GuardianAnnualIncome = mysqli_real_escape_string($conn, $_POST['GuardianAnnualIncome']);
                                            $GuardianEmail = mysqli_real_escape_string($conn, $_POST['GuardianEmail']);
                                            $GuardianMobileNo = mysqli_real_escape_string($conn, $_POST['GuardianMobileNo']);
                                            $GuardianTelNo = mysqli_real_escape_string($conn, $_POST['GuardianTelNo']);
                                            $CurrentAddress = mysqli_real_escape_string($conn, $_POST['CurrentAddress']);
                                            //$same_as_current_address = mysqli_real_escape_string($conn, $_POST['same_as_current_address']);
                                            if (isset($_POST['same_as_current_address'])){
                                                $PermanentAddress = $CurrentAddress;
                                            } else {
                                                $PermanentAddress = mysqli_real_escape_string($conn, $_POST['PermanentAddress']);
                                            }
                                            $DOB = mysqli_real_escape_string($conn, $_POST['DOB']);
                                            $Nationality = mysqli_real_escape_string($conn, $_POST['Nationality']);
                                            $Religion = mysqli_real_escape_string($conn, $_POST['Religion']);
                                            $Denomination = mysqli_real_escape_string($conn, $_POST['Denomination']);
                                            $category = mysqli_real_escape_string($conn, $_POST['category']);
                                            if ($category == "Others") {
                                                $category_textbox = mysqli_real_escape_string($conn, $_POST['category-textbox']);
                                                $category = $category_textbox;
                                            }

                                            $course_selector = mysqli_real_escape_string($conn, $_POST['course-selector']);
                                            $majors_selector = mysqli_real_escape_string($conn, $_POST['majors-selector']);
                                            $second_language_selector = mysqli_real_escape_string($conn, $_POST['second-language-selector']);
                                            $examination_passed = mysqli_real_escape_string($conn, $_POST['examination-passed']);
                                            if ($examination_passed == "Others") {
                                                $examination_passed_textbox = mysqli_real_escape_string($conn, $_POST['examination-passed-textbox']);
                                                $examination_passed = $examination_passed_textbox;
                                            }
                                            $subjects = $_POST['subject'];
                                            $subject_1 = mysqli_real_escape_string($conn, $subjects[1]);
                                            $subject_2 = mysqli_real_escape_string($conn, $subjects[2]);
                                            $subject_3 = mysqli_real_escape_string($conn, $subjects[3]);
                                            $subject_4 = mysqli_real_escape_string($conn, $subjects[4]);
                                            $subject_5 = mysqli_real_escape_string($conn, $subjects[5]);
                                            $subject_6 = mysqli_real_escape_string($conn, $subjects[6]);
                                            $max_marks = $_POST['MaxMarks'];
                                            $max_marts_1 = mysqli_real_escape_string($conn, $max_marks[1]);
                                            $max_marts_2 = mysqli_real_escape_string($conn, $max_marks[2]);
                                            $max_marts_3 = mysqli_real_escape_string($conn, $max_marks[3]);
                                            $max_marts_4 = mysqli_real_escape_string($conn, $max_marks[4]);
                                            $max_marts_5 = mysqli_real_escape_string($conn, $max_marks[5]);
                                            $max_marts_6 = mysqli_real_escape_string($conn, $max_marks[6]);
                                            $marks_obtained = $_POST['MarksObtained'];
                                            $marks_1 = mysqli_real_escape_string($conn, $marks_obtained[1]);
                                            $marks_2 = mysqli_real_escape_string($conn, $marks_obtained[2]);
                                            $marks_3 = mysqli_real_escape_string($conn, $marks_obtained[3]);
                                            $marks_4 = mysqli_real_escape_string($conn, $marks_obtained[4]);
                                            $marks_5 = mysqli_real_escape_string($conn, $marks_obtained[5]);
                                            $marks_6 = mysqli_real_escape_string($conn, $marks_obtained[6]);
                                            $total_max_marks = mysqli_real_escape_string($conn, $_POST['total_max_marks']);
                                            $total_marks = mysqli_real_escape_string($conn, $_POST['total_marks']);
                                            $sports_name = $_POST['sport_name'];
                                            if (count($sports_name) == 2) {
                                                $sport_name_1 = mysqli_real_escape_string($conn, $sports_name[1]);
                                            } else {
                                                $sport_name_1 = "Sports Record not available";
                                            }
                                            if (count($sports_name) == 3) {
                                                $sport_name_2 = mysqli_real_escape_string($conn, $sports_name[2]);
                                            } else {
                                                $sport_name_2 = "Sports Record not available";
                                            }
                                            if (count($sports_name) == 4) {
                                                $sport_name_3 = mysqli_real_escape_string($conn, $sports_name[3]);
                                            } else {
                                                $sport_name_3 = "Sports Record not available";
                                            }
                                            $sports_levels = $_POST['sports-level'];
                                            if (count($sports_levels) == 2) {
                                                $sport_level_1 = mysqli_real_escape_string($conn, $sports_levels[1]);
                                            } else {
                                                $sport_level_1 = "Sports Record not available";
                                            }
                                            if (count($sports_levels) == 3) {
                                                $sport_level_2 = mysqli_real_escape_string($conn, $sports_levels[2]);
                                            } else {
                                                $sport_level_2 = "Sports Record not available";
                                            }
                                            if (count($sports_levels) == 4) {
                                                $sport_level_3 = mysqli_real_escape_string($conn, $sports_levels[3]);
                                            } else {
                                                $sport_level_3 = "Sports Record not available";
                                            }
                                            if (isset($_POST['SCM'])){
                                            $SCM = mysqli_real_escape_string($conn, $_POST['SCM']);
                                            }
                                            else
                                            {
                                                $SCM = "Not Interested";
                                            }
                                            if (isset($_POST['Choir'])){
                                            $Choir = mysqli_real_escape_string($conn, $_POST['Choir']);
                                            }
                                            else
                                            {
                                                $Choir = "Not Interested";
                                            }
                                            if(isset($_POST['Civil_Defence'])){
                                            $Civil_Defence = mysqli_real_escape_string($conn, $_POST['Civil_Defence']);
                                            }
                                            else
                                            {
                                                $Civil_Defence = "Not Interested";
                                            }
                                            if(isset($_POST['NSS'])){
                                            $NSS = mysqli_real_escape_string($conn, $_POST['NSS']);
                                            }
                                            else{
                                                $NSS = "Not Interested";
                                            }
                                            if(isset($_POST['Music'])){
                                            $Music = mysqli_real_escape_string($conn, $_POST['Music']);
                                            }
                                            else{
                                                $Music = "Not Interested";
                                            }
                                            if(isset($_POST['Sports'])){
                                            $Sports = mysqli_real_escape_string($conn, $_POST['Sports']);
                                            }
                                            else{
                                                $Sports = "Not Interested";
                                            }
                                            $previous_institute = mysqli_real_escape_string($conn, $_POST['previous_institute']);
                                            $PreviousInstituteAddress = mysqli_real_escape_string($conn, $_POST['PreviousInstituteAddress']);
                                            $year_of_passing = mysqli_real_escape_string($conn, $_POST['year_of_passing']);
                                            $self_declaration = mysqli_real_escape_string($conn, $_POST['self_declaration']);
                                            $place_declaration = mysqli_real_escape_string($conn, $_POST['place_declaration']);

                                            //Profile Picture Upload
                                            if ($_FILES["profile_picture"]["name"] != '') {
                                                $extract_profile_name = explode('.', $_FILES["profile_picture"]["name"]);
                                                $profile_ext = end($extract_profile_name);
                                                $profile_picture_name = $StudentName . rand(100, 999) . '.' . $profile_ext;
                                                $profile_location = 'profile_images/' . $profile_picture_name;
                                                move_uploaded_file($_FILES["profile_picture"]["tmp_name"], $profile_location);
                                                $profile_picture_path = mysqli_real_escape_string($conn, $profile_location);
                                            }
                                            if (count($_FILES["certificates"]["name"]) == 2) {
                                                // Sports Certificates Upload
                                                if ($_FILES["certificates"]["name"][1] != '') {
                                                    $extract_sports_certificate_1 = explode('.', $_FILES["certificates"]["name"][1]);
                                                    $sports_certificate_1_ext = end($extract_sports_certificate_1);
                                                    $sports_certificate_1_name = $StudentName . '_' . $sport_name_1 . rand(100, 999) .  '.' . $sports_certificate_1_ext;
                                                    $sports_certificate_1_location = 'sports_certificates/' . $sports_certificate_1_name;
                                                    move_uploaded_file($_FILES["certificates"]["tmp_name"][1], $sports_certificate_1_location);
                                                    $sports_certificate_1_path = mysqli_real_escape_string($conn, $sports_certificate_1_location);
                                                } else {
                                                    $sports_certificate_1_path = "images/empty-image.png";
                                                }
                                            } else {
                                                $sports_certificate_1_path = "images/empty-image.png";
                                            }
                                            if (count($_FILES["certificates"]["name"]) == 3) {
                                                if ($_FILES["certificates"]["name"][2] != '') {
                                                    $extract_sports_certificate_2 = explode('.', $_FILES["certificates"]["name"][2]);
                                                    $sports_certificate_2_ext = end($extract_sports_certificate_2);
                                                    $sports_certificate_2_name = $StudentName . '_' . $sport_name_2 . rand(100, 999) .  '.' . $sports_certificate_2_ext;
                                                    $sports_certificate_2_location = 'sports_certificates/' . $sports_certificate_2_name;
                                                    move_uploaded_file($_FILES["certificates"]["tmp_name"][2], $sports_certificate_2_location);
                                                    $sports_certificate_2_path = mysqli_real_escape_string($conn, $sports_certificate_2_location);
                                                } else {
                                                    $sports_certificate_2_path = "images/empty-image.png";
                                                }
                                            } else {
                                                $sports_certificate_2_path = "images/empty-image.png";
                                            }
                                            if (count($_FILES["certificates"]["name"]) == 4) {


                                                if ($_FILES["certificates"]["name"][3] != '') {
                                                    $extract_sports_certificate_3 = explode('.', $_FILES["certificates"]["name"][3]);
                                                    $sports_certificate_3_ext = end($extract_sports_certificate_3);
                                                    $sports_certificate_3_name = $StudentName . '_' . $sport_name_3 . rand(100, 999) .  '.' . $sports_certificate_3_ext;
                                                    $sports_certificate_3_location = 'sports_certificates/' . $sports_certificate_3_name;
                                                    move_uploaded_file($_FILES["certificates"]["tmp_name"][3], $sports_certificate_3_location);
                                                    $sports_certificate_3_path = mysqli_real_escape_string($conn, $sports_certificate_3_location);
                                                } else {
                                                    $sports_certificate_3_path = "images/empty-image.png";
                                                }
                                            } else {
                                                $sports_certificate_3_path = "images/empty-image.png";
                                            }
                                            // Father Signature Upload
                                            if ($_FILES["father_signature"]["name"] != '') {
                                                $extract_father_signature_name = explode('.', $_FILES["father_signature"]["name"]);
                                                $father_signature_ext = end($extract_father_signature_name);
                                                $father_signature_name = $StudentName . rand(100, 999) . '.' . $father_signature_ext;
                                                $father_signature_location = 'father_signatures/' . $father_signature_name;
                                                move_uploaded_file($_FILES["father_signature"]["tmp_name"], $father_signature_location);
                                                $father_signature_path = mysqli_real_escape_string($conn, $father_signature_location);
                                            } else {
                                                $father_signature_path = "images/empty-image.png";
                                            }
                                            // mother Signature Upload
                                            if ($_FILES["mother_signature"]["name"] != '') {
                                                $extract_mother_signature_name = explode('.', $_FILES["mother_signature"]["name"]);
                                                $mother_signature_ext = end($extract_mother_signature_name);
                                                $mother_signature_name = $StudentName . rand(100, 999) . '.' . $mother_signature_ext;
                                                $mother_signature_location = 'mother_signatures/' . $mother_signature_name;
                                                move_uploaded_file($_FILES["mother_signature"]["tmp_name"], $mother_signature_location);
                                                $mother_signature_path = mysqli_real_escape_string($conn, $mother_signature_location);
                                            } else {
                                                $mother_signature_path = "images/empty-image.png";
                                            }

                                            // guardian Signature Upload
                                            if ($_FILES["guardian_signature"]["name"] != '') {
                                                $extract_guardian_signature_name = explode('.', $_FILES["guardian_signature"]["name"]);
                                                $guardian_signature_ext = end($extract_guardian_signature_name);
                                                $guardian_signature_name = $StudentName . rand(100, 999) . '.' . $guardian_signature_ext;
                                                $guardian_signature_location = 'guardian_signatures/' . $guardian_signature_name;
                                                move_uploaded_file($_FILES["guardian_signature"]["tmp_name"], $guardian_signature_location);
                                                $guardian_signature_path = mysqli_real_escape_string($conn, $guardian_signature_location);
                                            } else {
                                                $guardian_signature_path = "images/empty-image.png";
                                            }
                                            // student Signature Upload
                                            if ($_FILES["student_signature"]["name"] != '') {
                                                $extract_student_signature_name = explode('.', $_FILES["student_signature"]["name"]);
                                                $student_signature_ext = end($extract_student_signature_name);
                                                $student_signature_name = $StudentName . rand(100, 999) . '.' . $student_signature_ext;
                                                $student_signature_location = 'student_signatures/' . $student_signature_name;
                                                move_uploaded_file($_FILES["student_signature"]["tmp_name"], $student_signature_location);
                                                $student_signature_path = mysqli_real_escape_string($conn, $student_signature_location);
                                            } else {
                                                $student_signature_path = "images/empty-image.png";
                                            }
                                            // marks_card Upload
                                            if ($_FILES["marks_card"]["name"] != '') {
                                                $extract_marks_card_name = explode('.', $_FILES["marks_card"]["name"]);
                                                $marks_card_ext = end($extract_marks_card_name);
                                                $marks_card_name = $StudentName . rand(100, 999) . '.' . $marks_card_ext;
                                                $marks_card_location = 'marks_cards/' . $marks_card_name;
                                                move_uploaded_file($_FILES["marks_card"]["tmp_name"], $marks_card_location);
                                                $marks_card_path = mysqli_real_escape_string($conn, $marks_card_location);
                                            } else {
                                                $marks_card_path = "images/empty-image.png";
                                            }
                                            // transfer_certificate Upload
                                            if ($_FILES["transfer_certificate"]["name"] != '') {
                                                $extract_transfer_certificate_name = explode('.', $_FILES["transfer_certificate"]["name"]);
                                                $transfer_certificate_ext = end($extract_transfer_certificate_name);
                                                $transfer_certificate_name = $StudentName . rand(100, 999) . '.' . $transfer_certificate_ext;
                                                $transfer_certificate_location = 'transfer_certificates/' . $transfer_certificate_name;
                                                move_uploaded_file($_FILES["transfer_certificate"]["tmp_name"], $transfer_certificate_location);
                                                $transfer_certificate_path = mysqli_real_escape_string($conn, $transfer_certificate_location);
                                            } else {
                                                $transfer_certificate_path = "images/empty-image.png";
                                            }
                                            // obc_certificate Upload
                                            if ($_FILES["obc_certificate"]["name"] != '') {
                                                $extract_obc_certificate_name = explode('.', $_FILES["obc_certificate"]["name"]);
                                                $obc_certificate_ext = end($extract_obc_certificate_name);
                                                $obc_certificate_name = $StudentName . rand(100, 999) . '.' . $obc_certificate_ext;
                                                $obc_certificate_location = 'obc_certificates/' . $obc_certificate_name;
                                                move_uploaded_file($_FILES["obc_certificate"]["tmp_name"], $obc_certificate_location);
                                                $obc_certificate_path = mysqli_real_escape_string($conn, $obc_certificate_location);
                                            } else {
                                                $obc_certificate_path = "images/empty-image.png";
                                            }
                                            // foreign_eligible Upload
                                            if ($_FILES["foreign_eligible"]["name"] != '') {
                                                $extract_foreign_eligible_name = explode('.', $_FILES["foreign_eligible"]["name"]);
                                                $foreign_eligible_ext = end($extract_foreign_eligible_name);
                                                $foreign_eligible_name = $StudentName . rand(100, 999) . '.' . $foreign_eligible_ext;
                                                $foreign_eligible_location = 'foreign_eligible_certificates/' . $foreign_eligible_name;
                                                move_uploaded_file($_FILES["foreign_eligible"]["tmp_name"], $foreign_eligible_location);
                                                $foreign_eligible_path = mysqli_real_escape_string($conn, $foreign_eligible_location);
                                            } else {
                                                $foreign_eligible_path = "images/empty-image.png";
                                            }
                                            // csi_membership Upload
                                            if ($_FILES["csi_membership"]["name"] != '') {
                                                $extract_csi_membership_name = explode('.', $_FILES["csi_membership"]["name"]);
                                                $csi_membership_ext = end($extract_csi_membership_name);
                                                $csi_membership_name = $StudentName . rand(100, 999) . '.' . $csi_membership_ext;
                                                $csi_membership_location = 'csi_membership_certificates/' . $csi_membership_name;
                                                move_uploaded_file($_FILES["csi_membership"]["tmp_name"], $csi_membership_location);
                                                $csi_membership_path = mysqli_real_escape_string($conn, $csi_membership_location);
                                            } else {
                                                $csi_membership_path = "images/empty-image.png";
                                            }
                                            if (mysqli_query($conn, "INSERT INTO application(application_id, 
                                                                            student_email,
                                                                            student_name, 
                                                                            admission_year, 
                                                                            father_name, 
                                                                            guardian_name,
                                                                            mother_name,
                                                                            father_occupation,
                                                                            mother_occupation,
                                                                            guardian_occupation,
                                                                            father_email,
                                                                            mother_email,
                                                                            guardian_email,
                                                                            father_mobile,
                                                                            mother_mobile,
                                                                            guardian_mobile,
                                                                            father_telephone,
                                                                            mother_telephone,
                                                                            guardian_telephone,
                                                                            father_annual_income,
                                                                            mother_annual_income,
                                                                            guardian_annual_income,
                                                                            father_office_address,
                                                                            mother_office_address, 
                                                                            guardian_office_address,
                                                                            current_address,
                                                                            permanent_address,
                                                                            date_of_birth,
                                                                            nationality,
                                                                            religion,
                                                                            christian_denomination,
                                                                            category,
                                                                            course,
                                                                            majors,
                                                                            second_language,
                                                                            examination_passed,
                                                                            subject_1,
                                                                            subject_2,
                                                                            subject_3,
                                                                            subject_4,
                                                                            subject_5,
                                                                            subject_6,
                                                                            max_marks_1,
                                                                            max_marks_2,
                                                                            max_marks_3,
                                                                            max_marks_4,
                                                                            max_marks_5,
                                                                            max_marks_6,
                                                                            marks_1,
                                                                            marks_2,
                                                                            marks_3,
                                                                            marks_4,
                                                                            marks_5,
                                                                            marks_6,
                                                                            total_max_marks,
                                                                            total_marks,
                                                                            sport_name_1,
                                                                            sport_name_2,
                                                                            sport_name_3,
                                                                            sport_level_1,
                                                                            sport_level_2,
                                                                            sport_level_3,
                                                                            sport_cert_1_path,
                                                                            sport_cert_2_path,
                                                                            sport_cert_3_path,
                                                                            scm_activity,
                                                                            choir_activity,
                                                                            civil_defense_activity,
                                                                            nss_defense_activity,
                                                                            music_activity,
                                                                            sports_activity,
                                                                            previous_institution_name,
                                                                            previous_institution_address,
                                                                            year_of_passing,
                                                                            self_declaration_name,
                                                                            place_of_submission,
                                                                            profile_picture_path,
                                                                            father_signature_picture_path,
                                                                            mother_signature_picture_path,
                                                                            guardian_signature_picture_path,
                                                                            student_signature_picture_path,
                                                                            marks_card_picture_path,
                                                                            transfer_certificate_picture_path,
                                                                            obc_certificate_picture_path,
                                                                            foreign_eligible_picture_path,
                                                                            csi_membership_picture_path,
                                                                            application_status) 
                                                                            VALUES('" . $application_number  . "', 
                                                                                   '" . $_SESSION['login_email'] . "',
                                                                                   '" . $StudentName . "',
                                                                                   '" . $admission_year . "',
                                                                                   '" . $FathersName . "',
                                                                                   '" . $GuardiansName . "',
                                                                                   '" . $MothersName . "',
                                                                                   '" . $FatherOccupation . "',
                                                                                   '" . $MotherOccupation . "',
                                                                                   '" . $GuardianOccupation . "',
                                                                                   '" . $FatherEmail . "',
                                                                                   '" . $MotherEmail . "',
                                                                                   '" . $GuardianEmail . "',
                                                                                   '" . $FatherMobileNo . "',
                                                                                   '" . $MotherMobileNo . "',
                                                                                   '" . $GuardianMobileNo . "',
                                                                                   '" . $FatherTelNo . "',
                                                                                   '" . $MotherTelNo . "',
                                                                                   '" . $GuardianTelNo . "',
                                                                                   '" . $FatherAnnualIncome . "',
                                                                                   '" . $MotherAnnualIncome . "',
                                                                                   '" . $GuardianAnnualIncome . "',
                                                                                   '" . $FatherOfficeAddress . "',
                                                                                   '" . $MotherOfficeAddress . "',
                                                                                   '" . $GuardianOfficeAddress . "',
                                                                                   '" . $CurrentAddress . "',
                                                                                   '" . $PermanentAddress . "',
                                                                                   '" . $DOB . "',
                                                                                   '" . $Nationality . "',
                                                                                   '" . $Religion . "',
                                                                                   '" . $Denomination . "',
                                                                                   '" . $category . "',
                                                                                   '" . $course_selector . "',
                                                                                   '" . $majors_selector . "',
                                                                                   '" . $second_language_selector . "',
                                                                                   '" . $examination_passed . "',
                                                                                   '" . $subject_1 . "',
                                                                                   '" . $subject_2 . "',
                                                                                   '" . $subject_3 . "',
                                                                                   '" . $subject_4 . "',
                                                                                   '" . $subject_5 . "',
                                                                                   '" . $subject_6 . "',
                                                                                   '" . $max_marts_1 . "',
                                                                                   '" . $max_marts_2 . "',
                                                                                   '" . $max_marts_3 . "',
                                                                                   '" . $max_marts_4 . "',
                                                                                   '" . $max_marts_5 . "',
                                                                                   '" . $max_marts_6 . "',
                                                                                   '" . $marks_1 . "',
                                                                                   '" . $marks_2 . "',
                                                                                   '" . $marks_3 . "',
                                                                                   '" . $marks_4 . "',
                                                                                   '" . $marks_5 . "',
                                                                                   '" . $marks_6 . "',
                                                                                   '" . $total_max_marks . "',
                                                                                   '" . $total_marks . "',
                                                                                   '" . $sport_name_1 . "',
                                                                                   '" . $sport_name_2 . "',
                                                                                   '" . $sport_name_3 . "',
                                                                                   '" . $sport_level_1 . "',
                                                                                   '" . $sport_level_2 . "',
                                                                                   '" . $sport_level_3 . "',
                                                                                   '" . $sports_certificate_1_path . "',
                                                                                   '" . $sports_certificate_2_path . "',
                                                                                   '" . $sports_certificate_3_path . "',
                                                                                   '" . $SCM . "',
                                                                                   '" . $Choir . "',
                                                                                   '" . $Civil_Defence . "',
                                                                                   '" . $NSS . "',
                                                                                   '" . $Music . "',
                                                                                   '" . $Sports . "',
                                                                                   '" . $previous_institute . "',
                                                                                   '" . $PreviousInstituteAddress . "',
                                                                                   '" . $year_of_passing . "',
                                                                                   '" . $self_declaration . "',
                                                                                   '" . $place_declaration . "',
                                                                                   '" . $profile_picture_path . "',
                                                                                   '" . $father_signature_path . "',
                                                                                   '" . $mother_signature_path . "',
                                                                                   '" . $guardian_signature_path . "',
                                                                                   '" . $student_signature_path . "',
                                                                                   '" . $marks_card_path . "',
                                                                                   '" . $transfer_certificate_path . "',
                                                                                   '" . $obc_certificate_path . "',
                                                                                   '" . $foreign_eligible_path . "',
                                                                                   '" . $csi_membership_path . "',
                                                                                   'Submitted'
                                                                                   )")) {
                                                echo ' <div class="form-group">
                                <label>Application Number:' . $application_number . '</label>
                            </div>
                            <div class="form-group">
                                <label>Application Status:</label>
                                <span><b> Successfully Submitted and please do check your email (if you do not see email in your inbox, then please check your spam)</b> </span>
                            </div> ';
                                                $mail->AddAddress($_SESSION['login_email'], $StudentName);
                                                $mail->Body = 'Thanks for your Submitting application!!! We will review your application and you can track your application here: ...';
                                                $mail->Send();
                                            } else {
                                                echo ' <div class="form-group">
                                <label>Application Number:' . $application_number . '</label>
                            </div>
                            <div class="form-group">
                                <label>Application Status:</label>
                                <span><b> Oops, Something went wrong!!!. Please do contact us if you are unable to submit application online.</b> </span>
                            </div> ';
                                                echo mysqli_error($conn);
                                            }
                                        }
                                        mysqli_close($conn);
                                        ?>




                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end contact content -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End contact  -->


    <?php include('include/footer.php'); ?>

    <!-- jQuery library -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.js"></script>
    <!-- Slick slider -->
    <script type="text/javascript" src="assets/js/slick.js"></script>
    <!-- Counter -->
    <script type="text/javascript" src="assets/js/waypoints.js"></script>
    <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>

    <!-- Custom js -->
    <script src="assets/js/custom.js"></script>

</body>

</html>