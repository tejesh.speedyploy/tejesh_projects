<?php include('include/session.php');?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Goodwill college | Aqar </title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

    <!-- Font awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">   
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">          
    <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

    <!-- Main style sheet -->
    <link href="assets/css/style.css" rel="stylesheet">    

   
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>

<?php include('include/header.php');?>    
 <!-- End breadcrumb -->
 <section id="mu-course-content">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
              <div class="col-md-9">
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                        <figure class="mu-latest-course-img">
                          <a href="#"><img src="images/annualreport.jpg" alt="img"></a>
                         
                        </figure>
                        
                        <div class="mu-title">
                        <h2>AQAR</h2>
                              




                  
                        </div>
                       
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
              <div class="col-md-3">
                <!-- start sidebar -->
                <aside class="mu-sidebar">
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Quick Links</h3>
                  <ul class="mu-sidebar-catg">

              
              	<li><a href="iqac.php">IQAC</a></li>    
    <li><a href="aquar.php">AQAR</a></li>
</ul>  
                  </div>
                  <!-- end single sidebar -->
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Latest News</h3>
                    <div class="mu-sidebar-popular-courses">
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/1.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Christmas Day</a></h4>                      
                        
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/2.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Annual day</a></h4>                      
                          
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/3.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Students showcasing talent</a></h4>                      
                         
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end single sidebar -->
                 
                  
                </aside>
                <!-- / end sidebar -->
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
 
 
 
  <section>
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
      
            <div class="row">
            
            
              <div class="col-md-12">
              
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                       
                       
                        <div class="mu-latest-course-single-content">
                         
 
<!-- 1st row -->

		<div class="row">
        
        <!-- 1st col -->
        
			<div class="col-sm-6 col-lg-4">
        		<!-- Card Flip -->
				<div class="card-flip">
					<div class="flip">
						<div class="front">
							<!-- front content -->
							<div class="card">
							 
							  <div class="card-block">
                              <div style="text-align:center">
                              <i class="fa fa-file-text fa-3x" style="color:white; text-align:center"></i>
                              </div>
							    <h4 class="card-title" style="text-align:center">AQAR report 2013-2014</h4>
							    <p class="card-text">Click here to see AQAR report 2013-2014</p>
							    
							  </div>
							</div>
						</div>
						<div class="back">
							<!-- back content -->
							<div class="card">
							  <div class="card-block">
							    <h4 class="card-title" style="text-align:center">Download AQAR report 2013-2014</h4>
							  
							  </div>
							  
							  <div class="card-block">
							 
							  <a href="EC_PCA&A_61_19   dated 15th September 2012-Goodwill Christian College for Women, Bangalore (AQAR 2013-2014).pdf" class="btn btn-primary">Download Link</a>
							   
							  </div>
							</div>
						</div>
					</div>
				</div>
        		<!-- End Card Flip -->
			</div>
            
            
            
             <!-- 2nd col -->
          <div class="col-sm-6 col-lg-4">
        		<!-- Card Flip -->
				<div class="card-flip">
					<div class="flip">
						<div class="front">
							<!-- front content -->
							<div class="card">
							 
							  <div class="card-block">
                              <div style="text-align:center">
                              <i class="fa fa-file-text fa-3x" style="color:white; text-align:center"></i>
                              </div>
							    <h4 class="card-title" style="text-align:center">AQAR report 2014-2015</h4>
							    <p class="card-text">Click here to see AQAR report 2014-2015</p>
							    
							  </div>
							</div>
						</div>
						<div class="back">
							<!-- back content -->
							<div class="card">
							  <div class="card-block">
							    <h4 class="card-title" style="text-align:center">Download AQAR report 2014-2015</h4>
							  
							  </div>
							  
							  <div class="card-block">
							 
							  <a href="EC-PCA&A-61-19 15th September 2012.pdf" class="btn btn-primary">Download Link</a>
							   
							  </div>
							</div>
						</div>
					</div>
				</div>
        		<!-- End Card Flip -->
			</div>
            
            
             <!-- 3d col -->
            
            <div class="col-sm-6 col-lg-4">
        		<!-- Card Flip -->
				<div class="card-flip">
					<div class="flip">
						<div class="front">
							<!-- front content -->
							<div class="card">
							 
							  <div class="card-block">
                              <div style="text-align:center">
                              <i class="fa fa-file-text fa-3x" style="color:white; text-align:center"></i>
                              </div>
							    <h4 class="card-title" style="text-align:center">AQAR report 2015-2016</h4>
							    <p class="card-text">Click here to see AQAR report 2015-2016</p>
							    
							  </div>
							</div>
						</div>
						<div class="back">
							<!-- back content -->
							<div class="card">
							  <div class="card-block">
							    <h4 class="card-title" style="text-align:center">Download AQAR report 2015-2016</h4>
							  
							  </div>
							  
							  <div class="card-block">
							 
							  <a href="GOODWILL CHRISTIAN COLLEGE FOR WOMEN AQAR for the year 2015-16.pdf" class="btn btn-primary">Download Link</a>
							   
							  </div>
							</div>
						</div>
					</div>
				</div>
        		<!-- End Card Flip -->
			</div>
            
            
            
            
		</div>

<!-- end  1st row -->
 
 
                          
                        </div>
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
        
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
 
 
 
 
   <section>
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
      
            <div class="row">
            
            
              <div class="col-md-12">
              
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                       
                       
                        <div class="mu-latest-course-single-content">
                         
 
<!-- 1st row -->

		<div class="row">
        
        <!-- 1st col -->
        
			 <div class="col-sm-6 col-lg-4">
        		<!-- Card Flip -->
				<div class="card-flip">
					<div class="flip">
						<div class="front">
							<!-- front content -->
							<div class="card">
							 
							  <div class="card-block">
                              <div style="text-align:center">
                              <i class="fa fa-file-text fa-3x" style="color:white; text-align:center"></i>
                              </div>
							    <h4 class="card-title" style="text-align:center">AQAR report 2016-2017</h4>
							    <p class="card-text">Click here to see AQAR report 2016-2017</p>
							    
							  </div>
							</div>
						</div>
						<div class="back">
							<!-- back content -->
							<div class="card">
							  <div class="card-block">
							    <h4 class="card-title" style="text-align:center">Download AQAR report 2016-2017</h4>
							  
							  </div>
							  
							  <div class="card-block">
							 
							  <a href="AQAR 2016-17.pdf" class="btn btn-primary">Download Link</a>
							   
							  </div>
							</div>
						</div>
					</div>
				</div>
        		<!-- End Card Flip -->
			</div>
            
            
            
              <!-- 2nd col -->
          <div class="col-sm-6 col-lg-4">
        		<!-- Card Flip -->
				<div class="card-flip">
					<div class="flip">
						<div class="front">
							<!-- front content -->
							<div class="card">
							 
							  <div class="card-block">
                              <div style="text-align:center">
                              <i class="fa fa-file-text fa-3x" style="color:white; text-align:center"></i>
                              </div>
							    <h4 class="card-title" style="text-align:center">AQAR report 2017-2018</h4>
							    <p class="card-text">Click here to see AQAR report 2017-2018</p>
							    
							  </div>
							</div>
						</div>
						<div class="back">
							<!-- back content -->
							<div class="card">
							  <div class="card-block">
							    <h4 class="card-title" style="text-align:center">Download AQAR report 2017-2018</h4>
							  
							  </div>
							  
							  <div class="card-block">
							 
							  <a href="aqar 2017-2018.pdf" class="btn btn-primary">Download Link</a>
							   
							  </div>
							</div>
						</div>
					</div>
				</div>
        		<!-- End Card Flip -->
			</div>
            
            
            
            
               <!-- 3d col -->
          <div class="col-sm-6 col-lg-4">
        		<!-- Card Flip -->
				<div class="card-flip">
					<div class="flip">
						<div class="front">
							<!-- front content -->
							<div class="card">
							 
							  <div class="card-block">
                              <div style="text-align:center">
                              <i class="fa fa-file-text fa-3x" style="color:white; text-align:center"></i>
                              </div>
							    <h4 class="card-title" style="text-align:center">AQAR report 2018-2019</h4>
							    <p class="card-text">Click here to see AQAR report 2018-2019</p>
							    
							  </div>
							</div>
						</div>
						<div class="back">
							<!-- back content -->
							<div class="card">
							  <div class="card-block">
							    <h4 class="card-title" style="text-align:center">Download AQAR report 2018-2019</h4>
							  
							  </div>
							  
							  <div class="card-block">
							 
							  <a href="aqar 2018-2019.pdf" class="btn btn-primary">Download Link</a>
							   
							  </div>
							</div>
						</div>
					</div>
				</div>
        		<!-- End Card Flip -->
			</div>
            
           
            
		</div>

<!-- end  1st row -->
 
 
                          
                        </div>
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
        
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
 
 
 
 
 
 <?php include('include/footer.php'); ?>
   
  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>  
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>   
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>  

  <!-- Custom js -->
  <script src="assets/js/custom.js"></script> 

  </body>
</html>