$(window).load(function() {
    setTimeout(function() {
      $('#enquirypopup').modal('show');
    }, 2000);
  });

  // hide messages 
  $("#error").hide();
  $("#show_message").hide();

  // on submit...
  $('#registration_form').submit(function(e) {

    e.preventDefault();


    $("#error").hide();

    //name required
    var name = $("input#first_name").val();
    if (name == "") {
      $("#error").fadeIn().text("Name required.");
      $("input#first_name").focus();
      return false;
    }

    // email required
    var email = $("input#email").val();
    if (email == "") {
      $("#error").fadeIn().text("Email required");
      $("input#email").focus();
      return false;
    }


    // ajax
    $.ajax({
      type: "POST",
      url: "functions/register.php",
      data: $(this).serialize(), // get all form field value in serialize form
      success: function(result) {
        var json = $.parseJSON(result);
        if (json.response.code == "1") {
          $("#show_message").fadeIn();
          setTimeout(function() {
            $("#enquirypopup").modal('toggle');
          }, 3000);
        } else
          $("#error").fadeIn().text(json.response.status + ": " + json.response.message);
          setTimeout(function() {
            $("#error").fadeOut().empty();
          }, 5000);
      }
    });
  });

  $('#login').submit(function(e) {
    e.preventDefault();

    // ajax
    $.ajax({
      type: "POST",
      url: "functions/login_authenticate.php",
      data: $(this).serialize(), // get all form field value in serialize form
      success: function(result) {
        var json = $.parseJSON(result);
        if (json.response.code == "1") {
          $("#show_login").fadeIn().text(json.response.message);
          setTimeout(function() {
            $("#enquirypopup").modal('toggle');
          }, 3000);
          location.reload();
        } else
          $("#error").fadeIn().text(json.response.status + ": " + json.response.message);
          setTimeout(function() {
            $("#error").fadeOut().empty();
          }, 5000);
      }
    });
   });

   jQuery.fn.liScroll = function(settings) {
    settings = jQuery.extend({
      travelocity: 0.02
    }, settings);
    return this.each(function() {
      var $strip = jQuery(this);
      $strip.addClass("newsticker")
      var stripHeight = 1;
      $strip.find("li").each(function(i) {
        stripHeight += jQuery(this, i).outerHeight(true); // thanks to Michael Haszprunar and Fabien Volpi
      });
      var $mask = $strip.wrap("<div class='mask'></div>");
      var $tickercontainer = $strip.parent().wrap("<div class='tickercontainer'></div>");
      var containerHeight = $strip.parent().parent().height(); //a.k.a. 'mask' width 	
      $strip.height(stripHeight);
      var totalTravel = stripHeight;
      var defTiming = totalTravel / settings.travelocity; // thanks to Scott Waye		
      function scrollnews(spazio, tempo) {
        $strip.animate({
          top: '-=' + spazio
        }, tempo, "linear", function() {
          $strip.css("top", containerHeight);
          scrollnews(totalTravel, defTiming);
        });
      }
      scrollnews(totalTravel, defTiming);
      $strip.hover(function() {
          jQuery(this).stop();
        },
        function() {
          var offset = jQuery(this).offset();
          var residualSpace = offset.top + stripHeight;
          var residualTime = residualSpace / settings.travelocity;
          scrollnews(residualSpace, residualTime);
        });
    });
  };

  $(function() {
    $("ul#ticker01").liScroll();
  });