<?php include('include/session.php');?> 
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Goodwill college |BCA </title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

    <!-- Font awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">   
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">          
    <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

    <!-- Main style sheet -->
    <link href="assets/css/style.css" rel="stylesheet">    

   
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>

<?php include('include/header.php');?>   
 <!-- End breadcrumb -->
 <section id="mu-course-content">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
              <div class="col-md-9">
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                        <figure class="mu-latest-course-img">
                          <a href="#"><img src="images/bca1.jpg" alt="img"></a>
                         
                        </figure>
                        
                         <div class="mu-title">
                        <h2>BCA </h2>
                        <p>The Department Of Computer Applications primary objective is to train young students on the growing needs of various dusciplines of Information Technology with adequate technical and management skills to handle computer applications, software problem solving situations and develop analytical skills.</p><p> It also enables the students to be knowledgeable, informative and applicable to omplement through innovative methodologies.</p>
                        
       <img src="images/bca.jpg" alt="img">              

                      
                        </div>
                       
                       
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
              <div class="col-md-3">
                <!-- start sidebar -->
                <aside class="mu-sidebar">
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Quick Links</h3>
                  <ul class="mu-sidebar-catg">
  <li><a href="depart.php">Departments</a></li>
    <li><a href="faculty.php">Faculty Details</a></li>
    <li><a href="academic.php">Academic Calendar</a></li>
    <li><a href="result.php">RESULTS</a></li>        
</ul>  
                  </div>
                  <!-- end single sidebar -->
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Latest News</h3>
                    <div class="mu-sidebar-popular-courses">
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/1.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Christmas Day</a></h4>                      
                        
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/2.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Annual day</a></h4>                      
                          
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/3.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Students showcasing talent</a></h4>                      
                         
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end single sidebar -->
                 
                  
                </aside>
                <!-- / end sidebar -->
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
 
 
 <section>
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
      
            <div class="row">
            
            
              <div class="col-md-12">
              
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                       
                       
                        <div class="mu-title">
                         
  
<h2>Club Activities</h2>
<br>
<h4 style="text-decoration:underline">Department of Computer Application – <strong>TECHNOTONICS</strong></h4>

<P>The Department of Computer Science envisions imparting high eminence skills and imparting the knowledge of current trends in software and technology.</P>
<img src="images/technotonics.jpg">
<br><br>
<p>Fests are organized every year with the objective to provide platform for the students to showcase their talent with a Competitive spirit. Department of Computer Science conducted BCA fest "<strong>Texperia</strong>" on 1st of March 2018. </p>

<img src="images/technotonics2.jpg">
<p>The entire day passed on with the stupendous blast of events such as "<em>Debate, Mad ads, Pictionary, Linkers, Logos, Brain Mappers, collage, Coding and Debugging</em>" which gave the students the much needed break from the daily routine and exposure required to face the challenges of the real world.</p><p> The students of the different departments also actively participated in bringing out their hidden talents as well as boosting up to sharpen their skills.</p>
<p>The events not only helped in exhibiting the talents but also enriching the students with the knowledge of current trends and technology.</p>




          


                        

                          
                        </div>
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
        
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
  
  
 

 <?php include('include/footer.php'); ?>
   
  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>  
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>   
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>  

  <!-- Custom js -->
  <script src="assets/js/custom.js"></script> 

  </body>
</html>