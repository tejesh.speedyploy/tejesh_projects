<?php include('include/session.php');?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Goodwill college |Counselling Cell</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

    <!-- Font awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">   
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">          
    <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

    <!-- Main style sheet -->
    <link href="assets/css/style.css" rel="stylesheet">    

   
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>

<?php include('include/header.php');?>      
  
  <!-- Hero-area -->
		<div class="hero-area section">

			<!-- Backgound Image -->
			<div class="bg-image bg-parallax overlay" style="background-image:url(assets/img/counter-bg.jpg)"></div>
			<!-- /Backgound Image -->

			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1 text-center">
						<ul class="hero-area-tree">
							<li><a href="index.php">Home</a></li>
							<li>Counselling Cell</li>
						</ul>
						<h1 class="white-text">Counselling Cell</h1>

					</div>
				</div>
			</div>

		</div>
		<!-- /Hero-area -->

  

<!--<div class="parallax">dsaasdsda</div>
-->
 <!-- End breadcrumb -->
 <section id="mu-course-content">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
              <div class="col-md-9">
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                        <figure class="mu-latest-course-img">
                          <a href="#"><img src="images/counsel.jpg" alt="img"></a>
                         
                        </figure>
                        
                        <div class="mu-title">
                        <h2>Counselling Cell</h2>
                              
            <p>  Goodwill Christian College has a Student Counselling Centre in place that offers supportive and conducive environment for any student with personal issues or challenges to seek help and guidance from a professional counsellor. </p>
<p><strong>Guidance & Counselling Centre has been established in the college to provide guidance to achieve following objectives</strong>:- </p>
<ul style="line-height:30px">
			<li><i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i>  To provide guidance to the students on various options available in the course of their study. </li>
			 <li><i class="fa fa-angle-double-right" style="color:#690; font-weight:900"> </i>To identifying and developing Students abilities and interests. </li>
			 <li><i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> To help the students to solve their Personal, Educational and Psychological problems. </li>
			 <li><i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> To develop positive attitude and behaviour in order to meet challenges. </li>
			 <li><i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> To create awareness among the students for their future Profession. </li>
			 <li><i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i> To provide information to the students on the scope and relevance of any area irrespective of their field of interest. </li>
			<li><i class="fa fa-angle-double-right" style="color:#690; font-weight:900"> </i> To Recognise their strength overcome and weaknesses.</li>
            </ul>
            <p> The role of a counsellor is to offer support to the students to deal with a wide range of concerns -be it <strong>academic, personal, emotional, family or peer related</strong> through counselling. Our counsellor <strong>Mrs. Susan</strong> is available to provide help and support for students from diverse religious, cultural, ethnic backgrounds. </p>
<p>The counsellor listens to the student's concerns with empathy and responds in a non judgemental way. The process is confidential except in a condition where disclosure is necessary to protect you or another person or abide by the institute policy.</p> 
	<p>The counsellor can be contacted on two days : <strong>TUESDAY & THURSDAY</strong></p>
   <p>  <strong>TIMINGS : 9:00 am - 4 pm</strong></p>
   <p>  Counsellor will meet students and their parents as and when needed..</p> 


                    
                        </div>
                       
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
              <div class="col-md-3">
                <!-- start sidebar -->
                <aside class="mu-sidebar">
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Quick Links</h3>
                  <ul class="mu-sidebar-catg">
<li><a href="library.php">Library</a></li>  
              <li><a href="sports.php">Sports</a></li>   
    <li><a href="counsel.php">Counselling Cell</a></li>
    <li><a href="placement.php">Placement</a></li>
    <li><a href="cafeteria.php">Cafeteria</a></li>
     <li><a href="service.php">Service Programs</a></li>
     <li><a href="study.php">Dr. Ambedkar Study Centre</a></li>    
</ul>  
                  </div>
                  <!-- end single sidebar -->
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Latest News</h3>
                    <div class="mu-sidebar-popular-courses">
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/1.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Christmas Day</a></h4>                      
                        
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/2.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Annual day</a></h4>                      
                          
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/3.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Students showcasing talent</a></h4>                      
                         
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end single sidebar -->
                 
                  
                </aside>
                <!-- / end sidebar -->
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
 
 

 
 
 
 <?php include('include/footer.php'); ?>
   
  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>  
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>   
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>  

  <!-- Custom js -->
  <script src="assets/js/custom.js"></script> 

  </body>
</html>