<?php include('include/session.php');?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Goodwill college |Course</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

    <!-- Font awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">   
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">          
    <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

    <!-- Main style sheet -->
    <link href="assets/css/style.css" rel="stylesheet">    

   
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>

<?php include('include/header.php');?>    
 <!-- End breadcrumb -->
 <section id="mu-course-content">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
              <div class="col-md-9">
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                        <figure class="mu-latest-course-img">
                          <a href="#"><img src="images/course.jpg" alt="img"></a>
                         
                        </figure>
                        
                        <div class="mu-title">
                        <h2>Courses Offered</h2>
                        </div>
                      
                       
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
              <div class="col-md-3">
                <!-- start sidebar -->
                <aside class="mu-sidebar">
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Quick Links</h3>
                  <ul class="mu-sidebar-catg">
 <li><a href="course.php">Courses</a></li>
                 <li><a href="application.php">Application Form</a></li>
    <li><a href="procedure.php">Rules & Regulations</a></li>
     <li><a href="scholarship.php">Scholarship</a></li>
</ul>  
                  </div>
                  <!-- end single sidebar -->
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Latest News</h3>
                    <div class="mu-sidebar-popular-courses">
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/1.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Christmas Day</a></h4>                      
                        
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/2.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Annual day</a></h4>                      
                          
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/3.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Students showcasing talent</a></h4>                      
                         
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end single sidebar -->
                 
                  
                </aside>
                <!-- / end sidebar -->
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
 
 
 
  <section>
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
      
            <div class="row">
            
            
              <div class="col-md-12">
              
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                       
                       
                        <div class="mu-latest-course-single-content">
                         
  

<p>The College works in the affiliation mode and all admissions are made in tune with the rules and regulations framed by the Bangalore North University from time to time. The admissions are made based on the intake given by the Bangalore North University.</p>

<p>The applications are scrutinized, selected and admitted by the committees.</p>

<p>The Principal interviews the students and parents during admission. The College Prospectus caters to the students to get details about the college and courses offered.</p>
<p><strong>The following Under Graduate courses are offered in the University Departments:</strong></p>                         
                        
<table class="table">
  <thead><th>COURSE</th>
  <th>SECTION</th>
 
  </thead>
  
  
  <tbody>
  
  <TR>
  
 <TD>BBA</TD>
 <TD><a href="docs/BBA Syllabus.pdf" target="_blank"><i class="fa fa-download"></i>&nbsp;<mark>Download Syllabus</mark></a></TD>

 </TR>
 
 
  
  <TR>
  
 <TD>B.COM</TD>
 <TD><a href="docs/BCOM SYLLABUS.pdf" target="_blank"><i class="fa fa-download"></i>&nbsp;<mark>Download Syllabus</mark></a></TD>
 
 </TR>
 
  
  <TR>
  
 <TD>BCA</TD>
 <TD><a href="docs/BCA Syllabus.pdf" target="_blank"><i class="fa fa-download"></i>&nbsp;<mark>Download Syllabus</mark></a></TD>
 
 </TR>
  
  
  
  
   
 <TD>B.Sc</TD>
 <TD>Botany,Chemistry,& Microbiology
 <br/>
<a href="docs/BSC Syllabus.pdf" target="_blank"><i class="fa fa-download"></i>&nbsp;<mark>Download Syllabus</mark></a>
 </TD>
 
 </TR>
  
  
   
 <TD>B.A</TD>
 <TD>Psychology, English Literature, Journalism<br>
Psychology, Economics, Journalism<br/>
<a href="docs/BA Syllabus.pdf" target="_blank"><i class="fa fa-download"></i>&nbsp;<mark>Download Syllabus</mark></a>

 </TD>
 
</td>
 </TR>
  
  
   
 
 
 


 
</tbody>    
</table>    


 <ul style="line-height:24px;">
<li><i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i>&nbsp;Students from backward classes will be allotted seats as per roaster system because of the reservation policy. Fees exemption and scholarships are given by many organizations, institutions and departments of the government.</li><br>
<li><i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i>&nbsp;Students who are differently-abled are provided reservations as per the prescription of the Government of Karnataka. The physically challenged students are provided with scholarships. Fee concessions are provided to students who belong to this category.</li><br>
<li><i class="fa fa-angle-double-right" style="color:#690; font-weight:900"></i>&nbsp;Students from economically weaker sections are eligible for fee concessions and can apply for various scholarships provided by both Government and Non-Government agencies, which the college facilitates. The college also facilitates Book-Bank facilities provided by different agencies.</li><br>

  </ul>                           
                        

                          
                        </div>
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
        
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
 
   

 <?php include('include/footer.php'); ?>
  
  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>  
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>   
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>  

  <!-- Custom js -->
  <script src="assets/js/custom.js"></script> 

  </body>
</html>