<?php include('include/session.php');?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Goodwill college |Faculty</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

    <!-- Font awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">   
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">          
    <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

    <!-- Main style sheet -->
    <link href="assets/css/style.css" rel="stylesheet">    

   
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>

<?php include('include/header.php');?>      
  
  <!-- Hero-area -->
		<div class="hero-area section">

			<!-- Backgound Image -->
			<div class="bg-image bg-parallax overlay" style="background-image:url(assets/img/counter-bg.jpg)"></div>
			<!-- /Backgound Image -->

			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1 text-center">
						<ul class="hero-area-tree">
							<li><a href="index.php">Home</a></li>
							<li>Faculty</li>
						</ul>
						<h1 class="white-text">Faculty</h1>

					</div>
				</div>
			</div>

		</div>
		<!-- /Hero-area -->

  

<!--<div class="parallax">dsaasdsda</div>
-->
 <!-- End breadcrumb -->
 <section id="mu-course-content">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
              <div class="col-md-9">
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                        <figure class="mu-latest-course-img">
                          <a href="#"><img src="images/faculty.jpg" alt="img"></a>
                         
                        </figure>
                        
                        <div class="mu-title">
                        <h2>Faculty Details</h2>
                        </div>
                       
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
              <div class="col-md-3">
                <!-- start sidebar -->
                <aside class="mu-sidebar">
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Quick Links</h3>
                  <ul class="mu-sidebar-catg">
     <li><a href="depart.php">Departments</a></li>
    <li><a href="faculty.php">Faculty Details</a></li>
    <li><a href="academic.php">Academic Calendar</a></li>
    <li><a href="result.php">RESULTS</a></li>     
</ul>  
                  </div>
                  <!-- end single sidebar -->
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Latest News</h3>
                    <div class="mu-sidebar-popular-courses">
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/1.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Christmas Day</a></h4>                      
                        
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/2.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Annual day</a></h4>                      
                          
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/3.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Students showcasing talent</a></h4>                      
                         
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end single sidebar -->
                 
                  
                </aside>
                <!-- / end sidebar -->
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
 
 

 
 
 <section>
 
  <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
       
            <div class="row">
            
            
              <div class="col-md-12">
              
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                    
                 
                    
                    <div class="table-responsive">
                    
<h2>Teaching Staff</h2>
 <table class="table">
 
 
  <thead>
  <tr>
  <th>Sl.No</th>
  <th>Name</th>
  <th>Qualification</th>
  <th>Designation</th>
  </tr>
  </thead>
 
 
 
<tbody>


<!--<tr>
<td><p><b>1</b></p></td>
<td><p><b>Dr. Priscila J C J </b></p></td>
<td valign="top" ><p><b>M.Com,M.Phil, Ph.D.</b></p></td>
<td><p><b>PRINCIPAL</b></p></td>
</tr>
-->
<tr>
<td valign="top" ><p><b>2</b></p></td>
<td valign="top" ><p><b>Ms. Shalini A.E</b></p></td>
<td valign="top" ><p><b>M.A, B.Ed, M.Phil,</b></p></td>
<td valign="top" ><p><b>Lect. in English </b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>3</b></p></td>
<td valign="top" ><p><b>Mrs. Talathoti Leena Elizabeth </b></p></td>
<td valign="top" ><p><b>M.A, M.Phil,</b></p></td>
<td valign="top" ><p><b>Lect. in English</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>4</b></p></td>
<td valign="top" ><p><b>Mrs. Lydia Glory.I.</b></p></td>
<td valign="top" ><p><b>M.A, M.PHIL,</b></p></td>
<td valign="top" ><p><b>Lect. in English</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>5</b></p></td>
<td valign="top" ><p><b>Mrs. Mayuramani. B</b></p></td>
<td valign="top" ><p><b>M.A, </b></p></td>
<td valign="top" ><p><b>Lect. in Kannada</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>6</b></p></td>
<td valign="top" ><p><b>Dr. Thejavathi.K.</b></p></td>
<td valign="top" ><p><b>M.A, B.Ed, Ph.D, M.Phil,</b></p></td>
<td valign="top" ><p><b>Lect. in Kannada</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>7</b></p></td>
<td valign="top" ><p><b>Mrs. Pramila Sudharshan</b></p></td>
<td valign="top" ><p><b>M.A, M.Phil, </b></p></td>
<td valign="top" ><p><b>Lect. in Hindi</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>8</b></p></td>
<td valign="top" ><p><b>Ms. Raziya Sultana </b></p></td>
<td valign="top" ><p><b>M.A, B.Ed,</b></p></td>
<td valign="top" ><p><b>Lect. In Hindi </b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>9</b></p></td>
<td valign="top" ><p><b>Mrs. Amudha.S.</b></p></td>
<td valign="top" ><p><b>M.A, B.Ed, </b></p></td>
<td valign="top" ><p><b>Lect. in Tamil</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>10</b></p></td>
<td valign="top" ><p><b>Mrs. Afzalunissa </b></p></td>
<td valign="top" ><p><b>M.A, B.Ed, </b></p></td>
<td valign="top" ><p><b>Lect. In Urdu</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>11</b></p></td>
<td valign="top" ><p><b>Mrs. Geetha.D.</b></p></td>
<td valign="top" ><p><b>M.A,PGDPMIR, RBVP,</b></p></td>
<td valign="top" ><p><b>Lect. in Economics</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>12</b></p></td>
<td valign="top" ><p><b>Dr. Zarine Immanuel </b></p></td>
<td valign="top" ><p><b>M.Sc, M.Phil, Ph.D,</b></p></td>
<td valign="top" ><p><b>Lect. In Psychology</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>13</b></p></td>
<td valign="top" ><p><b>Mrs. Devi.G.</b></p></td>
<td valign="top" ><p><b>M.Sc in Commn., SET, NET,</b></p></td>
<td valign="top" ><p><b>Lect. In Journalism</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>14</b></p></td>
<td valign="top" ><p><b>Mrs. Hazel D'Souza</b></p></td>
<td valign="top" ><p><b>M.Sc, M.Phil, </b></p></td>
<td valign="top" ><p><b>Lect. in Microbiology</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>15</b></p></td>
<td valign="top" ><p><b>Dr. M.S.Gayathri</b></p></td>
<td valign="top" ><p><b>M.Sc, M.Phil, Ph.D,</b></p></td>
<td valign="top" ><p><b>Lect. in Botany</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>16</b></p></td>
<td valign="top" ><p><b>Mrs. Cynthia E Theodore</b></p></td>
<td valign="top" ><p><b>M.Sc, B.Ed, </b></p></td>
<td valign="top" ><p><b>Lect. in Chemistry</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>17</b></p></td>
<td valign="top" ><p><b>Mrs. Asma Azeez </b></p></td>
<td valign="top" ><p><b>M.Com, </b></p></td>
<td valign="top" ><p><b>Lect. in Commerce</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>18</b></p></td>
<td valign="top" ><p><b>Mrs. Anitha.S.</b></p></td>
<td valign="top" ><p><b>M.Com, M.Phil,</b></p></td>
<td valign="top" ><p><b>Lect. in Commerce</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>19</b></p></td>
<td valign="top" ><p><b>Mrs. Susan Priyadharshini.D.</b></p></td>
<td valign="top" ><p><b>M.Com, </b></p></td>
<td valign="top" ><p><b>Lect. in Commerce</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>20</b></p></td>
<td valign="top" ><p><b>Ms. Anitha.H.</b></p></td>
<td valign="top" ><p><b>M.Com, </b></p></td>
<td valign="top" ><p><b>Lect. in Commerce</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>21</b></p></td>
<td valign="top" ><p><b>Mrs. Priya Noel</b></p></td>
<td valign="top" ><p><b>M.Com, </b></p></td>
<td valign="top" ><p><b>Lect. in Commerce</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>22</b></p></td>
<td valign="top" ><p><b>Ms. Gracy.A.</b></p></td>
<td valign="top" ><p><b>M.Com,</b></p></td>
<td valign="top" ><p><b>Lect. in Commerce</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>23</b></p></td>
<td valign="top" ><p><b>Mrs. Sadia Rahman </b></p></td>
<td valign="top" ><p><b>M.Com, PGDHRM,</b></p></td>
<td valign="top" ><p><b>Lect. in Commerce</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>24</b></p></td>
<td valign="top" ><p><b>Mrs. Shyamala.M</b></p></td>
<td valign="top" ><p><b>MBA,</b></p></td>
<td valign="top" ><p><b>Lect. in Business Administration</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>25</b></p></td>
<td valign="top" ><p><b>Mrs. Saria Banu</b></p></td>
<td valign="top" ><p><b>MBA (Finance &amp;Human Resource), NET,</b></p></td>
<td valign="top" ><p><b>Lect. in Business Administration</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>26</b></p></td>
<td valign="top" ><p><b>Mrs. Saadia Tarannum</b></p></td>
<td valign="top" ><p><b>MBA, B.Ed,</b></p></td>
<td valign="top" ><p><b>Lect. in Business Administration</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>27</b></p></td>
<td valign="top" ><p><b>Mrs. Priya Mary Jacob </b></p></td>
<td valign="top" ><p><b>MBA, </b></p></td>
<td valign="top" ><p><b>Lect. in Business Administration</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>28</b></p></td>
<td valign="top" ><p><b>Mrs. Darly Mano </b></p></td>
<td valign="top" ><p><b>MBA, PGDIT,</b></p></td>
<td valign="top" ><p><b>Lect. in Business Administration</b></p></td>
</tr>



<tr>
<td valign="top" ><p><b>29</b></p></td>
<td valign="top" ><p><b>Mrs. Josephine Prapulla.A.</b></p></td>
<td valign="top" ><p><b>MCA,MBA,M.Phil,</b></p></td>
<td valign="top" ><p><b>Lect. In Computer Applications</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>30</b></p></td>
<td valign="top" ><p><b>Mrs. Kavitha Rajalakshmi</b></p></td>
<td valign="top" ><p><b>M.Sc, M.Phil, </b></p></td>
<td valign="top" ><p><b>Lect. In Computer Applications</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>31</b></p></td>
<td valign="top" ><p><b>Mrs. Pankaja Benkal </b></p></td>
<td valign="top" ><p><b>MCA, </b></p></td>
<td valign="top" ><p><b>Lect. In Computer Applications</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>32</b></p></td>
<td valign="top" ><p><b>Ms. Hemavathi.B.H.</b></p></td>
<td valign="top" ><p><b>M.Lisc,</b></p></td>
<td valign="top" ><p><b>Librarian</b></p></td>
</tr>

</tbody>
</table>


<hr>

<h2>Administrative Staff
</h2>
<img src="images/administrativestaff.jpg">
<br>
<br>
<table class="table">
 
 
  <thead>
  <tr>
  <th>Sl.No</th>
  <th>Name</th>
  <th>Qualification</th>
  <th>Designation</th>
  </tr>
  </thead>
<tbody>


<tr>
<td valign="top" ><b> <ol start="1" type="1"><li> </li>
</ol> </b></td>
<td><p><b>Mrs. SARA ELIZABETH</b></p></td>
<td valign="top" ><p><b>B.Com, Diploma in Secretarial Practice</b></p></td>
<td valign="top" ><p><b>Office Superintendant</b></p></td>
</tr>

<tr>
<td valign="top" ><b> <ol start="2" type="1"><li> </li>
</ol> </b></td>
<td><p><b>Mrs. FLORENCE NIRMALA KUMARI</b></p></td>
<td valign="top" ><p><b>B.Sc, Diploma in Secretarial Practice</b></p></td>
<td valign="top" ><p><b>P.A. to Principal</b></p></td>
</tr>

<tr>
<td valign="top" ><b> <ol start="3" type="1"><li> </li>
</ol> </b></td>
<td><p><b>Ms. REKHA.S.</b></p></td>
<td valign="top" ><p><b>B.Com,</b></p></td>
<td valign="top" ><p><b>FDA</b></p></td>
</tr>

<tr>
<td valign="top" ><b> <ol start="4" type="1"><li> </li>
</ol> </b></td>
<td><p><b>Mrs. SONIA.E.</b></p></td>
<td valign="top" ><p><b>B.Com,</b></p></td>
<td valign="top" ><p><b>FDA</b></p></td>
</tr>

<tr>
<td valign="top" ><b> <ol start="5" type="1"><li> </li>
</ol> </b></td>
<td><p><b>Mrs. MARY SHYLA</b></p></td>
<td valign="top" ><p><b>PUC,</b></p></td>
<td valign="top" ><p><b>Office Assistant</b></p></td>
</tr>

<tr>
<td valign="top" ><b> <ol start="6" type="1"><li> </li>
</ol> </b></td>
<td><p><b>Mrs. MARY CATHERINE</b></p></td>
<td valign="top" ><p><b>PUC,</b></p></td>
<td valign="top" ><p><b>Library Assistant</b></p></td>
</tr>

</tbody>
</table>

<hr>

<h2>Sub-Staff</h2>

<table class="table">
 
 
  <thead>
  <tr>
  <th>Sl.No</th>
  <th>Name</th>
  <th>Qualification</th>
  <th>Designation</th>
  </tr>
  </thead>
<tbody>


<tr>
<td valign="top" ><p><b>1</b></p></td>
<td><p><b>Mrs. GRACY.M.</b></p></td>
<td valign="top" ><p><b>8th std</b></p></td>
<td valign="top" ><p><b>Attender</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>2</b></p></td>
<td><p><b>Mr. HUSSAIN</b></p></td>
<td><p><b>7<sup>th</sup> std</b></p></td>
<td><p><b>Attender cum Driver</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>3</b></p></td>
<td><p><b>Mrs. RATHNA</b></p></td>
<td valign="top" ><p><b>6<sup>th</sup> std</b></p></td>
<td valign="top" ><p><b>Attender</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>4</b></p></td>
<td><p><b>Mrs. MARY JANET</b></p></td>
<td valign="top" ><p><b>(10th Std) </b></p></td>
<td valign="top" ><p><b>Attender</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>5</b></p></td>
<td><p><b>Mr. NAVEEN</b></p></td>
<td valign="top" ><p><b>(10th Std) </b></p></td>
<td valign="top" ><p><b>Attender</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>6</b></p></td>
<td><p><b>Mrs. ANTHU MARY JOYCE </b></p></td>
<td valign="top" ><p><b>(PUC)</b></p></td>
<td valign="top" ><p><b>Attender</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>7</b></p></td>
<td><p><b>Mr. BHARATH</b></p></td>
<td valign="top" ><p><b>5<sup>th</sup> std</b></p></td>
<td valign="top" ><p><b>Day Security Guard</b></p></td>
</tr>

<tr>
<td valign="top" ><p><b>8</b></p></td>
<td><p><b>Mr. RAMU </b></p></td>
<td valign="top" > </td>
<td valign="top" ><p><b>Night Security Guard</b></p></td>
</tr>

</tbody>
</table>


                    
 </div>                   
                    
                    
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
              
              
              
              
             
             
             
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>

 <?php include('include/footer.php'); ?>
   
  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>  
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>   
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>  

  <!-- Custom js -->
  <script src="assets/js/custom.js"></script> 

  </body>
</html>