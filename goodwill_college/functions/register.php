<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    require_once "config.php";
    require_once "mail_config.php";

    if($_SERVER["REQUEST_METHOD"] == "POST") {
 
    $first_name = mysqli_real_escape_string($conn, $_POST['first_name']);
    $last_name = mysqli_real_escape_string($conn, $_POST['last_name']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $mobile = mysqli_real_escape_string($conn, $_POST['mobile']);
    
    $mail->AddAddress($email, $first_name);
 
    if(mysqli_query($conn, "INSERT INTO registrations(first_name, last_name, email, mobile) VALUES('" . $first_name . "', '" . $last_name . "', '" . $email . "', '" . $mobile . "')")) {
        $result = array(
            'response' => array(
              'status' => 'success',
              'code' => '1', // whatever you want
              'message' => 'Success!!!'
            )
        );
        $mail->Subject  =  'Registration Confirmed';
        $mail->Body = 'Thanks for your registration!!! We will get back to you soon...';
        $mail->Send(); 
    } else {
        $result = array(
            'response' => array(
              'status' => 'Failed',
              'code' => '2', // whatever you want
              'message' => mysqli_error($conn)
            )
        );
        $last_id = mysqli_query($conn, "SELECT MAX(id) from registrations;");
        if (mysqli_num_rows($last_id) > 0) {
            // output data of each row
            while($last_insert_id = mysqli_fetch_assoc($last_id)) {
              //echo "id: '" . $last_insert_id["id"]. "'";
              $increment_value =(int) $last_insert_id['MAX(id)'];
              if(mysqli_query($conn,"ALTER TABLE registrations AUTO_INCREMENT =  $increment_value;"));
          } 
        }
        $mail->Subject  =  'You are already registered!!!';
        $mail->Body = 'Thanks for your interest again!!! Our College Department will contact you soon...';
        $mail->Send();
    }
    mysqli_close($conn);
    echo json_encode($result);
  }

?>