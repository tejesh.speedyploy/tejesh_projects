<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    require_once "config.php";
    require_once "mail_config.php";

    if($_SERVER["REQUEST_METHOD"] == "POST") {
 
    $first_name = mysqli_real_escape_string($conn, $_POST['signup_first_name']);
    $last_name = mysqli_real_escape_string($conn, $_POST['signup_last_name']);
    $email = mysqli_real_escape_string($conn, $_POST['signup_email']);
    $mobile = mysqli_real_escape_string($conn, $_POST['signup_mobile']);
    $password = mysqli_real_escape_string($conn, $_POST['signup_password']);
 
    if(mysqli_query($conn, "INSERT INTO signup(first_name, last_name, email, mobile, password) VALUES('" . $first_name . "', '" . $last_name . "', '" . $email . "', '" . $mobile . "', '" . md5($password) ."')")) {
        $result = array(
            'response' => array(
              'status' => 'success',
              'code' => '1', // whatever you want
              'message' => 'Success!!!, please check confirmation mail in your spam if you do not see mail from us in your inbox.'
            )
        );
        $mail->AddAddress($email, $first_name);
        $mail->Subject  =  'Account Creation';
        $mail->Body = 'Thanks for creating an account with us... Please do login at http://www.goodwillchristiancollege.com/';
        $mail->Send();  
    } else {
        $result = array(
            'response' => array(
              'status' => 'Failed',
              'code' => '2', // whatever you want
              'message' => mysqli_error($conn)
            )
        );
        $last_id = mysqli_query($conn, "SELECT MAX(id) from signup;");
        if (mysqli_num_rows($last_id) > 0) {
            // output data of each row
            while($last_insert_id = mysqli_fetch_assoc($last_id)) {
              //echo "id: '" . $last_insert_id["id"]. "'";
              $increment_value =(int) $last_insert_id['MAX(id)'];
              if(mysqli_query($conn,"ALTER TABLE signup AUTO_INCREMENT =  $increment_value;"));
          } 
        }
        $mail->AddAddress($email, $first_name);
        $mail->Subject  =  'Account Exists';
        $mail->Body = 'You have an account already with us... In case, if you have forgot the password then please do reset at http://www.goodwillchristiancollege.com/forgot_password.php';
        $mail->Send();
    }
 
    mysqli_close($conn);
    echo json_encode($result); 
    }
?>