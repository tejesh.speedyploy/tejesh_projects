<?php include('include/session.php');?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Goodwill college | History</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

    <!-- Font awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">   
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">          
    <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

    <!-- Main style sheet -->
    <link href="assets/css/style.css" rel="stylesheet">    

   
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>
 
<?php include('include/header.php');?>   
  
  <!-- Hero-area -->
		<div class="hero-area section">

			<!-- Backgound Image -->
			<div class="bg-image bg-parallax overlay" style="background-image:url(assets/img/counter-bg.jpg)"></div>
			<!-- /Backgound Image -->

			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1 text-center">
						<ul class="hero-area-tree">
							<li><a href="index.php">Home</a></li>
							<li>History</li>
						</ul>
						<h1 class="white-text">History</h1>

					</div>
				</div>
			</div>

		</div>
		<!-- /Hero-area -->

  

<!--<div class="parallax">dsaasdsda</div>
-->
 <!-- End breadcrumb -->
 <section id="mu-course-content">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
              <div class="col-md-9">
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                        <figure class="mu-latest-course-img">
                          <a href="#"><img src="assets/img/gud_28.jpg" alt="img"></a>
                         
                        </figure>
                        <div class="mu-title">
                          <h2>History</h2>
                          <h5>THE STALWARTS OF GOODWILL INSTITUTIONS</h5>
                          
                        
                         
                        
                          
                        </div>
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
              <div class="col-md-3">
                <!-- start sidebar -->
                <aside class="mu-sidebar">
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Quick Links</h3>
                    <ul class="mu-sidebar-catg">
                   <li><a href="history.php">History</a></li>    
    <li><a href="principal.php">Principal's Message</a></li>
    <li><a href="infra.php">Infrastructure</a></li>
    <li><a href="index.php">Accreditation</a></li>    
    <li><a href="index.php">MOU'S</a></li>         
                     
                    </ul>
                  </div>
                  <!-- end single sidebar -->
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Latest News</h3>
                    <div class="mu-sidebar-popular-courses">
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/1.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Christmas Day</a></h4>                      
                        
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/2.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Annual day</a></h4>                      
                          
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/3.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Students showcasing talent</a></h4>                      
                         
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end single sidebar -->
                 
                  
                </aside>
                <!-- / end sidebar -->
             </div>
           </div>
         </div>
       </div>
     </div>
     
     <div class="row">
     <div class="col-md-12">
    
     
      <div class="row">
      
            <div class="col-lg-3 col-sm-3">
                <img class="img-rounded" src="images/found1 (1).jpg">
                <h4>
                    <small>Late Rev.F.Goodwill</small>
                </h4>
               
            </div>
            
            <div class="col-lg-3 col-sm-3">
                <img class="img-rounded" src="images/found1 (2).jpg">
                <h4>
                    <small>Late Miss.R.Sisterson</small>
                 </h4>
               
            </div>
            
            <div class="col-lg-3 col-sm-3">
                <img class="img-rounded" src="images/found1 (3).jpg">
               <h4>
                    <small>Late Miss.Roper</small>
                </h4>
               
            </div>
            
            <div class="col-lg-3 col-sm-3">
                <img class="img-rounded" src="images/found1 (4).jpg">
               <h4>
                    <small>Miss.Rogerson</small>
                </h4>
               
            </div>
            
            <div class="col-lg-3 col-sm-3">
                <img class="img-rounded" src="images/found1 (5).jpg">
               <h4>
                    <small>Late Miss.P.Krishnappa</small>
                </h4>
               
            </div>
            
             <div class="col-lg-3 col-sm-3">
                <img class="img-rounded" src="images/found1 (6).jpg">
               <h4>
                    <small>Late Miss.Adiappa</small>
                </h4>
               
            </div>
            
             <div class="col-lg-3 col-sm-3">
                <img class="img-rounded" src="images/found1 (7).jpg">
                <h4>
                    <small>Miss.Jothi Parker</small>
                </h4>
               
            </div>
            
             <div class="col-lg-3 col-sm-3">
                <img class="img-rounded" src="images/found1 (8).jpg">
                <h4>
                    <small>Miss.Phillips</small>
                 </h4>
               
            </div>
            
             <div class="col-lg-3 col-sm-3">
                <img class="img-rounded" src="images/found1 (9).jpg">
               <h4>
                    <small>Miss.Samson</small>
                </h4>
               
            </div>
            
             <div class="col-lg-3 col-sm-3">
                <img class="img-rounded" src="images/found1 (10).jpg">
                <h4>
                    <small>Miss.Ranthanjili Nicodemus</small>
                </h4>
               
            </div>
            
             <div class="col-lg-3 col-sm-3">
                <img class="img-rounded" src="images/shobha.jpg">
                <h4>
                    <small>Miss.Shobha</small>
                </h4>
               
            </div>
           
           
          
          
           
           
           
           
        </div>
     
     
     </div>
     
     </div>
     
   </div>
 </section>

 <?php include('include/footer.php'); ?>
  
  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>  
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>   
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>  

  <!-- Custom js -->
  <script src="assets/js/custom.js"></script> 

  </body>
</html>