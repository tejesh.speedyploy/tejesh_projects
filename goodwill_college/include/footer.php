<!-- Start footer -->
<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
<script src="assets/js/login-signup.js"></script>
<footer id="mu-footer">
    <!-- start footer top -->
    <div class="mu-footer-top">
      <div class="container">
        <div class="mu-footer-top-area">
          <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3">
              <div class="mu-footer-widget">
                <h4>Information</h4>
                <ul>
                  <li><a href="about.php">About Us</a></li>
                  <li><a href="application.php">Application</a></li>
                  <li><a href="course.php">Course</a></li>
                  <li><a href="contact.php">Feedback</a></li>

                </ul>
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
              <div class="mu-footer-widget">
                <h4>Departments</h4>
                <ul>
                  <li><a href="bba.php">BBA</a></li>
                  <li><a href="bca.php">BCA</a></li>
                  <li> <a href="bcom.php">BCOM</a></li>
                  <li> <a href="bsc.php">BSC</a></li>
                  <li><a href="ba.php">BA</a></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
              <div class="mu-footer-widget">
                <h4>Social Media</h4>
                <div class="fb-page" data-href="https://www.facebook.com/Goodwill-Christian-College-For-Women-1789667074602918/" data-tabs="timeline" data-height="200px" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                  <blockquote cite="https://www.facebook.com/Goodwill-Christian-College-For-Women-1789667074602918/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Goodwill-Christian-College-For-Women-1789667074602918/">Goodwill Christian College For Women</a></blockquote>
                </div>

              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
              <div class="mu-footer-widget">
                <h4>Contact</h4>
                <address>
                  <p>10, Promenade Road,
                    Frazer Town Bengaluru,
                    Karnataka 560005</p>
                  <p>Phone: 080-25567177, 08041100422</p>
                  <p>Fax: 080-41516793</p>
                  <p>Email:goodwillchristiancollege@gmail.com</p>
                </address>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- end footer top -->
    <!-- start footer bottom -->
    <div class="mu-footer-bottom">
      <div class="container">
        <div class="mu-footer-bottom-area">
          <p><?php echo date('Y');?> &copy; All Right Reserved. <a href="http://www.gooddwillchristiancollege.com/" rel="nofollow">Goodwill Christian College</a></p>
        </div>
      </div>
    </div>
    <!-- end footer bottom -->
  </footer>
  <!-- End footer -->
