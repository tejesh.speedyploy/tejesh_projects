
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.0';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <a class="scrollToTop" href="#">
      <i class="fa fa-angle-up"></i>      
    </a>
  <!-- END SCROLL TOP BUTTON -->

<header id="header">
<div class="container">

    <div class="navbar-header">
        <!-- Logo -->
        <div class="navbar-brand">
            <a class="logo" href="index.php">
                <img src="assets/img/logo.png" alt="logo">
            </a>
        </div>
    </div>
    <div class="admission">
    <a class="glow_button" href="application.php">Admission for the year <?php echo date("Y") . "-" . date('Y', strtotime('+1 years')); ?></a>

    </div>
</div>
</header>

<section id="mu-menu">
<nav class="navbar navbar-default navbar-expand-lg" role="navigation">  

<div class="navbar-header">
<!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>

</div>
<div id="navbar" class="navbar-collapse collapse">
<ul id="top-menu" class="nav navbar-nav navbar-right main-nav">
<li class="active"><a href="index.php">Home</a></li>   
  <li class="dropdown">
 <a href="#" class="dropdown-toggle" data-toggle="dropdown">About <span class="fa fa-angle-down"></span></a>
  <ul class="dropdown-menu" role="menu">
    <li><a href="about.php">About Goodwill</a></li>
<li><a href="history.php">History</a></li>    
<li><a href="principal.php">Principal's Message</a></li>
<li><a href="infra.php">Infrastructure</a></li>
<li><a href="index.php">Accreditation</a></li>    
<li><a href="index.php">MOU'S</a></li>         
  </ul>
  </li>     
  
   <li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">Admission <span class="fa fa-angle-down"></span></a>
 <ul class="dropdown-menu" role="menu">
    <li><a href="course.php">Courses</a></li>
     <li class="menu-item dropdown dropdown-submenu"><a href="application.php">Application Form</a>
     <ul class="dropdown-menu">
          <li>
          <a href="application.php">Apply Online</a>                      
          <a href="application_offline.php">Apply Offline</a>
        </li>
      </ul>
    </li>
<li><a href="procedure.php">Rules & Regulations</a></li>
<li><a href="scholarship.php">Scholarship</a></li>
    
  </ul>
</li>     
<li class="dropdown">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Academics <span class="fa fa-angle-down"></span></a>
  <ul class="dropdown-menu" role="menu">
    <li class="menu-item dropdown dropdown-submenu">
                        <a href="depart.php" class="dropdown-toggle" data-toggle="">Departments</a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="bba.php">BBA</a>
                                <a href="bca.php">BCA</a>
                                <a href="bcom.php">BCOM</a>
                                <a href="bsc.php">BSC</a>
                                <a href="ba.php">BA</a>
                            </li>
                        </ul>
                    </li>
<li><a href="faculty.php">Faculty Details</a></li>
<li><a href="workshop.php">Workshop & Seminar</a></li>
<li><a href="industry.php">Industrial Visits</a></li>
<li><a href="academic.php">Academic Calendar</a></li>
<li><a href="result.php">RESULTS</a></li>           
  </ul>
</li>           

<li class="dropdown">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Campus <span class="fa fa-angle-down"></span></a>
  <ul class="dropdown-menu" role="menu">
  <li><a href="library.php">Library</a></li>  
  <li><a href="sports.php">Sports</a></li>   
<li><a href="counsel.php">Counselling Cell</a></li>
<li><a href="placement.php">Placement</a></li>
<li><a href="cafeteria.php">Cafeteria</a></li>
<li><a href="service.php">Service Programs</a></li>
<li><a href="study.php">Dr. Ambedkar Study Centre</a></li>    

  
  </ul>
</li>  

<li class="dropdown">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Student World <span class="fa fa-angle-down"></span></a>
  <ul class="dropdown-menu" role="menu">
  <li><a href="orientation.php">Orientation</a></li>  
  <li><a href="talent.php">Talents Week</a></li>   
<li><a href="fresher.php">Freshers Day</a></li>
<li><a href="student.php">Student Elections</a></li>
<li><a href="annual.php">Annual Prize day</a></li> 
<li><a href="social.php">Socials</a></li>      

  </ul>
</li>  

 


      
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">Events<span class="fa fa-angle-down"></span></a>
 <ul class="dropdown-menu" role="menu">
  <li><a href="graduate.php">Graduation</a></li>
 <li><a href="invest.php">Investiture</a></li>
 <li><a href="heyah.php">Heyah</a></li>
      <li><a href="rhapsody.php">Rhapsody</a></li>    
<li><a href="christmas.php">Christmas</a></li>
<li><a href="ekta.php">EKTA</a></li>
     </ul>
 </li>    
 
     
 <li><a href="alumini.php">Alumni</a></li>
  <li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">IQAC<span class="fa fa-angle-down"></span></a>
 <ul class="dropdown-menu" role="menu">
  <li><a href="aquar.php">AQAR</a></li><li><a href="iqac.php">IQAC INTRO</a></li>

     </ul>
 </li>   
  


<li><a href="contact.php">Feedback</a></li>     

<?php if(!isset($_SESSION['login_user'])){
echo'
<ul class="nav navbar-nav navbar-right ml-auto">			
  <li class="nav-item">
    <a data-toggle="dropdown" class="nav-link dropdown-toggle" href="#">Login</a>
    <ul class="dropdown-menu form-wrapper">					
      <li>
      <span id="header_error" style="display: none; color:red;"></span>
      <span id="show_header" style="display: none; color:#17B6BB;"></span>
        <form action="" method="post" id="header_login">
          <div class="form-group">
            <input type="text" name="header_login_email" id="header_login_email" class="form-control" placeholder="Email Address" required="required">
          </div>
          <div class="form-group">
            <input type="password" name="header_login_password" id="header_login_password" class="form-control" placeholder="Password" required="required">
          </div>
          <input type="submit" class="btn btn-customize center-block" value="Login">
          <div class="form-footer">
            <a href="forgot_password.php">Forgot Your password?</a>
          </div>
        </form>
      </li>
    </ul>
  </li>
  <li class="nav-item">
    <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle get-started-btn mt-1 mb-1">Sign up</a>
    <ul class="dropdown-menu form-wrapper">					
      <li>
        <form action="" method="post" id="signup_form">
          <p class="hint-text">Fill in this form to create your account!</p>
          <p id="show_message_signup" style="display: none; color:#17B6BB;">Thanks you for signing up!!!. Please do login </p>
 
          <span id="error_signup" style="display: none; color:red"></span>
          <div class="form-group">
            <input type="text" class="form-control" placeholder="First Name" id="signup_first_name" name="signup_first_name" required="required">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Last Name" id="signup_last_name" name="signup_last_name" required="required">
          </div>
          <div class="form-group">
							<input type="text" class="form-control" name="signup_mobile" id="signup_mobile" placeholder="Enter Mobile" pattern="(6|7|8|9)\d{9}" title="Mobile number should only contain 10 numbers & starting with 6,7,8,9 eg:6123456789">
					</div>
          <div class="form-group">
							<input type="email" class="form-control" name="signup_email" id="signup_email" placeholder="Enter Email" required>
					</div>
          <div class="form-group">
            <input type="password" id="signup_password" name="signup_password" class="form-control" placeholder="Password" required="required" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" onchange="if(this.checkValidity()) form.signup_confirm_password.pattern = this.value;" title="The string must contain at least 1 lowercase alphabetical character, 1 uppercase alphabetical character, 1 numeric character, one special character & eight characters or longer">
          </div>
          <div class="form-group">
            <input type="password" id="signup_confirm_password" name="signup_confirm_password" class="form-control" placeholder="Confirm Password" required="required" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="Match the above password">
          </div>
          <div class="form-group">
          <input type="submit" class="btn btn-customize" name="signup" id="signup" value="Sign up">
          <input type="reset" class="btn btn-customize" value="Reset">
          </div>
        </form>
      </li>
    </ul>
  </li>
</ul>';
}
else{
echo ' 			
  <li class="nav-item">
    <a data-toggle="dropdown" class="nav-link dropdown-toggle" href="#">Hi, ' . $_SESSION['login_user'] . '</a>
    <ul class="dropdown-menu" role="menu">					  
    <li><a href="profile.php">Your Profile</a></li>   
    <li><a href="application_status.php">Application Status</a></li>
    <li><a href="functions/sign_out.php">Sign Out</a></li>
    </ul>
  </li>';
}
?>

</div><!--/.nav-collapse -->        

</nav>
</section>

<!-- End menu -->
