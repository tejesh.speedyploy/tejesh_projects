<?php include('include/session.php'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Goodwill college | Home</title>

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

  <!-- Font awesome -->
  <link href="assets/css/font-awesome.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <!-- Slick slider -->
  <link rel="stylesheet" type="text/css" href="assets/css/slick.css">
  <!-- Theme color -->
  <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

  <!-- Main style sheet -->
  <link href="assets/css/style.css" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-3.4.1.js"></script>

  <!-- Google Fonts -->
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>

</head>

<body>
  
  <?php include('include/header.php'); ?>

  <!--START SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#">
    <i class="fa fa-angle-up"></i>
  </a>
  <!-- END SCROLL TOP BUTTON -->
  <?php if (!isset($_SESSION['login_user'])) {
    echo '<div id="enquirypopup" class="modal fade in" role="dialog">
		<div class="modal-dialog">
			
			<!-- Modal content-->
			<div class="modal-content row">
				<div class="modal-header custom-modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Admissions Open</h4>
				</div>
				<div class="modal-body">
        <p id="show_message" style="display: none; color:#17B6BB;">Thanks for your registration. We will contact you shortly!!! </p>
 
        <span id="error" style="display: none; color: red;"></span>
        <span id="show_login" style="display: none; color:#17B6BB;"></span>
          <div class="row">
					<form name="info_form" class="form-inline" action="" method="post" id="registration_form">
						<div class="form-group col-sm-12">
							<input type="text" class="form-control" name="first_name" id="first_name" placeholder="Enter First Name" required autofocus>
						</div>
                        
            <div class="form-group col-sm-12">
							<input type="text" class="form-control" name="last_name" id="last_name" placeholder="Enter Last Name">
						</div> 
                        
						<div class="form-group col-sm-12">
							<input type="email" class="form-control" name="email" id="email" placeholder="Enter Email" required>
						</div>
						
						
						<div class="form-group col-sm-12">
							<input type="text" class="form-control" name="mobile" id="mobile" placeholder="Enter Mobile" pattern="(6|7|8|9)\d{9}" title="Mobile number should only contain 10 numbers & starting with 6,7,8,9 eg:6123456789">
						</div>   
						
						<div class="form-group col-sm-12">
							<button type="submit" id="register_button" class="btn btn-customize center-block">Call Back?</button>
						</div>
          </form> 
        </div> 
        <div class="or-seperator"><b>or</b></div>
         <div class="row">
                 <form name="info_form" class="form-inline" action="" method="post" id="login">
						<div class="form-group col-sm-12">
							<input type="text" class="form-control" name="login_email" id="login_email" placeholder="Email Address" required>
						</div>
                        
            <div class="form-group col-sm-12">
							<input type="password" class="form-control" name="login_password" id="login_password" placeholder="Password" required>
						</div> 
						<div class="form-group col-sm-12">
              <button type="submit" class="btn btn-customize center-block">LOGIN</button>
            </div>
          </form>
        </div>
        <div class="modal-footer"> <a href="forgot_password.php">Forgot Your password?</a></div>
				</div>
			</div>
			
		</div>
	</div>
  ';
  } ?>
  <!-- End pop up-->

  <!--Header File Included here-->

  <section style="padding-top:40px;">
    <div class="row">
      <div class="col-md-12">

        <div class="col-md-3">

          <div class="welcome-box vision">
            <span class="fa fa-send fa-2x"></span>
            <h2>Mission</h2>
            <p>To empower young women to face the challenges of life.</p>
            <br>

            <span class="fa fa-eye fa-2x"></span>
            <h2>Vision</h2>

            <p>To inculcate in our students a passion for excellence through value-based holistic education.</p>
          </div>
        </div>

        <div class="col-md-6">

          <!-- Start Slider -->
          <section id="mu-slider">


            <!-- Start single slider item -->




            <div class="mu-slider-single">
              <div class="mu-slider-img">
                <figure>
                  <img src="assets/img/slider/1.jpg" alt="img">
                </figure>
              </div>

            </div>
            <!-- Start single slider item -->
            <!-- Start single slider item -->
            <div class="mu-slider-single">
              <div class="mu-slider-img">
                <figure>
                  <img src="assets/img/slider/2.jpg" alt="img">
                </figure>
              </div>

            </div>
            <!-- Start single slider item -->
            <!-- Start single slider item -->
            <div class="mu-slider-single">
              <div class="mu-slider-img">
                <figure>
                  <img src="assets/img/slider/3.jpg" alt="img">
                </figure>
              </div>

            </div>


            <div class="mu-slider-single">
              <div class="mu-slider-img">
                <figure>
                  <img src="assets/img/slider/4.jpg" alt="img">
                </figure>
              </div>

            </div>

            <div class="mu-slider-single">
              <div class="mu-slider-img">
                <figure>
                  <img src="assets/img/slider/5.jpg" alt="img">
                </figure>
              </div>

            </div>



            <!-- Start single slider item -->



          </section>
          <!-- End Slider -->

        </div>

        <div class="col-md-3">
          <h2><i class="fa fa-newspaper-o"></i> News</h2>

          <div class="welcome-box">

            <div class="holder">
              <ul id="ticker01">
                <li><span>08/02/2020</span><a href="">&nbsp;Sports day Blood donation camp </a></li>
                <li><span>13/02/2019</span><a href="">&nbsp;Blood donation & voluntary <br>eye checkup </a></li>
                <li><span>23/02/2019</span><a href="">&nbsp;Sports day </a></li>
                <li><span>13/10/2018</span><a href="docs/Goodwill.pdf" target="_blank">&nbsp;One day Faculty development programme on Quality Enhancement in teaching</a></li>
                <li><span>26/06/2018</span><a href="#">&nbsp;Admissions Open for 2018<br> Batch at Goodwill Christian College for Women</a></li>

                <li><span>14/04/2018</span><a href="study.php">&nbsp;Dr.B.R.Ambedkar Birth Anniversary</a></li>
                <li><span>28/04/2018</span><a href="alumini.php">&nbsp;Alumni Meet</a></li>
                <li><span>10/05/2018</span><a href="graduate.php">&nbsp;Graduation Ceremony</a></li>
                <li><span>02/03/2018</span><a href="bca.php">&nbsp;Club Activities "Technotonics"</a></li>
                <li><span>21/03/2018</span><a href="#">&nbsp;One day Guest Lecture</a></li>
                <li><span>09/04/2018</span><a href="ba.php">&nbsp;Department of Arts -<br>Artist's Hub</a></li>
                <li><span>07/04/2018</span><a href="bcom.php">&nbsp;Department of Commerce -Comvision</a></li>
                <li><span>06/04/2018</span><a href="#">&nbsp;Special Guest Lecture</a></li>
                <li><span>23/02/2018</span><a href="#">&nbsp;One day National Conference</a></li>

              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Start about us -->
  <section id="mu-about-us-green">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="mu-about-us-area">
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="mu-about-us-right">
                  <!-- Start Title -->
                  <div class="mu-title">
                    <h2>Welcome</h2>


                  </div>
                  <!-- End Title -->
                  <i class="fa fa-quote-left" aria-hidden="true" style="color:#01a2a6"></i>
                  <em> Education is the most powerful weapon we can use to change the world <i class="fa fa-quote-right" aria-hidden="true" style="color:#01a2a6"></i></em>

                  <p style="text-align:right">- <strong>Nelson Mandela</strong></p>
                  <p>
                    Goodwill Christian College for Women has continuously catered to the educational needs of women. In a span of 20 years the institution has grown in stature, providing quality education and facilitating the holistic development of students.</p>
                  <p>Our college is affiliated to Bengaluru North University and has well planned infrastructure, competent faculty and amenities to encourage curricular, co-curricular and extra-curricular activities.</p>
                  <p> It offers a number of undergraduate courses in Science, Commerce, Business Administration, Humanities and Computer Applications along with several certificate courses.
                  </p>
                  <p> <a href="docs/Goodwill.pdf" target="_blank" class="btn btn-primary btn-success"><span class="glyphicon glyphicon-download"></span> Download Brochure</a></p>
                </div>
              </div>
              <!-- <div class="col-lg-5 col-md-5 pull-right">
                <div class="mu-about-us-left">  
                
                
                  <div class="mu-title">
                    <h2>Principal's Message</h2>
                 
                          
                  </div>
                
                  <p>
               <i class="fa fa-quote-left" aria-hidden="true" style="color:#01a2a6"></i>
Quality Education is every individual's birthright, more so for a woman of civilized society.</p><p> With this motto, I appreciate Goodwill Christian College for Women that was established in the year 1999 and has successfully run for the last 18 years catering to the women community of Bangalore, and Bangalore East in particular.
                  </p>                          
               
                </div>
                
                  <div class="singCourse_author">
                      <img src="assets/img/principal.jpg" alt="img">
                      <p>Dr. Priscila J C J </p>
                      <p><strong>Principal</strong></p>
                    </div>
              </div>-->

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End about us -->




  <!-- Start about us -->
  <section id="mu-about-us">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="mu-about-us-area">
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="mu-title">
                  <h2>Video</h2>

                </div>
                <div class="mu-about-us-right">


                  <a class="about-video" href="https://www.youtube.com/embed/x4eKDCWXRfM">
                    <img src="assets/img/about-us.jpg" alt="img">
                    <i class="play-icon fa fa-play"></i>
                  </a>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="mu-about-us-left">
                  <div class="container">
                    <div class="mu-title">
                      <h2>Events</h2>


                    </div>
                    <div class="row">
                      <div class='col-md-6'>

                        <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                          <!-- Bottom Carousel Indicators -->
                          <ol class="carousel-indicators">
                            <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#quote-carousel" data-slide-to="1"></li>
                            <li data-target="#quote-carousel" data-slide-to="2"></li>
                            <li data-target="#quote-carousel" data-slide-to="3"></li>
                            <li data-target="#quote-carousel" data-slide-to="4"></li>
                            <li data-target="#quote-carousel" data-slide-to="5"></li>
                          </ol>

                          <!-- Carousel Slides / Quotes -->
                          <div class="carousel-inner">

                            <!-- Quote 1 -->
                            <div class="item active">

                              <div class="row">
                                <div class="col-md-12 text-center">

                                  <a href="#"><img src="assets/img/courses/1.jpg" alt="img"></a>

                                </div>

                              </div>

                            </div>

                            <!-- Quote 2 -->
                            <div class="item">

                              <div class="row">
                                <div class="col-md-12 text-center">

                                  <a href="#"><img src="assets/img/courses/2.jpg" alt="img"></a>

                                </div>

                              </div>

                            </div>

                            <!-- Quote 3 -->
                            <div class="item">

                              <div class="row">
                                <div class="col-md-12 text-center">
                                  <a href="#"><img src="assets/img/courses/3.jpg" alt="img"></a>
                                </div>
                              </div>

                            </div>



                            <!-- Quote 6 -->
                            <div class="item">

                              <div class="row">
                                <div class="col-md-12 text-center">
                                  <a href="#"><img src="assets/img/courses/6.jpg" alt="img"></a>
                                </div>
                              </div>

                            </div>




                          </div>

                          <!-- end Carousel inner -->

                        </div>

                        <!-- end Carousel slide -->

                      </div>

                      <!-- end Column -->


                    </div>
                    <!-- endrow -->

                  </div>
                  <!-- end container -->


                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End about us -->



  <!-- Why Choose section Start -->

  <section class="why-choose">
    <div class="container">
      <div class="row">

      </div>
    </div>
  </section>
  <!-- Why Choose section End -->


  <!-- CEO BIOGRAPHY -->
  <section class="about-ceo">
    <div class="row">

      <div class="col-md-5"> <img class="img-responsive" src="images/features1.jpg" draggable="false" alt=" "> </div>

      <div class="col-md-7 about-ceo-padding">
        <h3>Special Features</h3>



        <div class="row">
          <div class="col-md-6">
            <div class="why-wrap">
              <div class="icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="why-text">
                <h4><a href="#">Quality education at affordable cost</a></h4>

              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="why-wrap">
              <div class="icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="why-text">
                <h4><a href="#">Students are University Rank Holders</a></h4>

              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="why-wrap">
              <div class="icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="why-text">
                <h4><a href="#">Awards for academic achievements</a></h4>

              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="why-wrap">
              <div class="icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="why-text">
                <h4><a href="#">Well Equipped Laboratories</a></h4>

              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="why-wrap">
              <div class="icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="why-text">
                <h4><a href="#">Club Activities</a></h4>

              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="why-wrap">
              <div class="icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="why-text">
                <h4><a href="#">Literary Society</a></h4>

              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="why-wrap">
              <div class="icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="why-text">
                <h4><a href="#">Value Education</a></h4>

              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="why-wrap">
              <div class="icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="why-text">
                <h4><a href="#">Work shops and seminars</a></h4>

              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="why-wrap">
              <div class="icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="why-text">
                <h4><a href="#">Industrial, Educational and eco trips</a></h4>

              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="why-wrap">
              <div class="icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="why-text">
                <h4><a href="#">Women's desk</a></h4>

              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="why-wrap">
              <div class="icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="why-text">
                <h4><a href="#">Internship</a></h4>

              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="why-wrap">
              <div class="icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="why-text">
                <h4><a href="#">Secured environment</a></h4>

              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="why-wrap">
              <div class="icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="why-text">
                <h4><a href="#">Out reach programmes /Student Christian movement</a></h4>

              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="why-wrap">
              <div class="icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="why-text">
                <h4><a href="#">Extension activities- NSS, Civil defense /Red Cross</a></h4>

              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="why-wrap">
              <div class="icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="why-text">
                <h4><a href="#">Placement cell/ Campus Recruitment</a></h4>

              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="why-wrap">
              <div class="icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="why-text">
                <h4><a href="#">Career Guidance and counseling</a></h4>

              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="why-wrap">
              <div class="icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="why-text">
                <h4><a href="#">Mentoring students</a></h4>

              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="why-wrap">
              <div class="icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="why-text">
                <h4><a href="#">Medical check up</a></h4>

              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="why-wrap">
              <div class="icon">
                <i class="fa fa-check"></i>
              </div>
              <div class="why-text">
                <h4><a href="#">Sports facilities</a></h4>

              </div>
            </div>
          </div>


        </div>





      </div>

    </div>
  </section>
  <!-- CEO BIOGRAPHY END -->




  <?php include('include/footer.php'); ?>
  
  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>

  <!-- Custom js -->
  <script src="assets/js/custom.js"></script>


  <script src="assets/js/index-page.js"></script>
</body>

</html>