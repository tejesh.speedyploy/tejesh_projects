<?php include('include/session.php');?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Goodwill college |Industrial visit </title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

    <!-- Font awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">   
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">          
    <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

    <!-- Main style sheet -->
    <link href="assets/css/style.css" rel="stylesheet">    

   
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>
 
<?php include('include/header.php');?>   
  
  <!-- Hero-area -->
		<div class="hero-area section">

			<!-- Backgound Image -->
			<div class="bg-image bg-parallax overlay" style="background-image:url(assets/img/counter-bg.jpg)"></div>
			<!-- /Backgound Image -->

			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1 text-center">
						<ul class="hero-area-tree">
							<li><a href="index.php">Home</a></li>
							<li>Industrial visit </li>
						</ul>
						<h1 class="white-text">Industrial visit </h1>

					</div>
				</div>
			</div>

		</div>
		<!-- /Hero-area -->

  

<!--<div class="parallax">dsaasdsda</div>
-->
 <!-- End breadcrumb -->
 <section id="mu-course-content">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
              <div class="col-md-9">
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                        <figure class="mu-latest-course-img">
                          <a href="#"><img src="images/industry4.jpg" alt="img"></a>
                         
                        </figure>
                        
                         <div class="mu-title">
                        <h2>Industrial visit </h2>
                      
                        </div>
                       
                       
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
              <div class="col-md-3">
                <!-- start sidebar -->
                <aside class="mu-sidebar">
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Quick Links</h3>
                  <ul class="mu-sidebar-catg">
  <li><a href="depart.php">Departments</a></li>
    <li><a href="faculty.php">Faculty Details</a></li>
    <li><a href="academic.php">Academic Calendar</a></li>
    <li><a href="result.php">RESULTS</a></li>        
</ul>  
                  </div>
                  <!-- end single sidebar -->
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Latest News</h3>
                    <div class="mu-sidebar-popular-courses">
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/1.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Christmas Day</a></h4>                      
                        
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/2.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Annual day</a></h4>                      
                          
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/3.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Students showcasing talent</a></h4>                      
                         
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end single sidebar -->
                 
                  
                </aside>
                <!-- / end sidebar -->
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
 
 
 <section>
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
      
            <div class="row">
            
            
              <div class="col-md-12">
              
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                       
                       
                        <div class="mu-title">
                         
  

<p>Industrial visit is considered as one of the most tactical methods of teaching. It helps students to learn and understand the industry thoroughly which would help them to choose their career. We at <strong>Goodwill Christian College For Women</strong>, provide an incremental learning experience, exposing our students to different industrial <em>ideas, information and Knowledge</em>.</p>
<img src="images/bbaindustry.jpg" alt="img">
<p> This further helps our students seek for more knowledge through various sources that enlightens them with clearer goal-setting and directional thoughts. In addition to industrial exposure and knowledge, this will increase the internship and placement opportunities.</p>
<p>Our visits sensitize the students to the practical challenges that organizations face in the business world. They also give greater clarity about various management concepts for students as they can practically see how these concepts are put into action</p>
<img src="images/industry.jpg" alt="img">
<p>All the streams give this exposure from academic point of view and enhance their practical working environment and  awareness of various Industrial sectors.</p>

<p><strong>The Department of Botany has</strong> organised industrial visits to "<em><strong>Azyme Biosciences</strong></em>". Azyme Biosciences provides <em>HPLC training</em> in Food biotechnology, Biotechnology Instrumentation training, Biotechnology Projects for students and Biotechnology Corporate training programmes in Bangalore. </p>
<img src="images/industry3.jpg" alt="img">
<p>The Final years of the <strong>Microbiology Department</strong> visited the prestigious <em>Karnataka Antibiotics and Pharmaceutical limited</em> on to get an insight to the manufacturing protocols and experience the Industrial operation in the field of Industrial Microbiology. </p>

 <a href="#"><img src="images/industry1.jpg" alt="img"></a>
 <p>A visit to the Ice cream factory and Mysore silk factory was organized by the <strong>Commerce department</strong> to improve the industrial knowledge of the students. The students were taken to the silk factory and were shown the different process of manufacturing silk sarees. The process starts with the breeding of the silkworms, spinning the cocoon, sorting and softening the cocoon, forming silk yarn, degumming thrown yarn, finishing the silk fabrics and finally weaving it into sarees. The visit was very educative and interesting. Students were exposed to manufacturing process practically.</p>




          


                        

                          
                        </div>
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
        
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
  
  
 

 <?php include('include/footer.php'); ?>
  
  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>  
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>   
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>  

  <!-- Custom js -->
  <script src="assets/js/custom.js"></script> 

  </body>
</html>