<?php include('include/session.php');?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Goodwill college | Infrastructure</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

    <!-- Font awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">   
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">          
    <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

    <!-- Main style sheet -->
    <link href="assets/css/style.css" rel="stylesheet">    

   
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>
 
<?php include('include/header.php');?>   
  
  <!-- Hero-area -->
		<div class="hero-area section">

			<!-- Backgound Image -->
			<div class="bg-image bg-parallax overlay" style="background-image:url(assets/img/counter-bg.jpg)"></div>
			<!-- /Backgound Image -->

			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1 text-center">
						<ul class="hero-area-tree">
							<li><a href="index.php">Home</a></li>
							<li>Infrastructure</li>
						</ul>
						<h1 class="white-text">Infrastructure</h1>

					</div>
				</div>
			</div>

		</div>
		<!-- /Hero-area -->

  

<!--<div class="parallax">dsaasdsda</div>
-->
 <!-- End breadcrumb -->
 <section id="mu-course-content">
 
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
              <div class="col-md-9">
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                        <figure class="mu-latest-course-img">
                          <a href="#"><img src="assets/img/gud_05.jpg" alt="img"></a>
                         
                        </figure>
                        
                         <div class="mu-title">
                         
<h2>Infrastructure</h2>

</div>
                        
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
              <div class="col-md-3">
                <!-- start sidebar -->
                <aside class="mu-sidebar">
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Quick Links</h3>
                   
                       <ul class="mu-sidebar-catg">
                   <li><a href="history.php">History</a></li>    
    <li><a href="principal.php">Principal's Message</a></li>
    <li><a href="infra.php">Infrastructure</a></li>
    <li><a href="index.php">Accreditation</a></li>    
    <li><a href="index.php">MOU'S</a></li>         
                     
                    </ul>
                  </div>
                  <!-- end single sidebar -->
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Latest News</h3>
                    <div class="mu-sidebar-popular-courses">
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/1.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Christmas Day</a></h4>                      
                        
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/2.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Annual day</a></h4>                      
                          
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/3.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Students showcasing talent</a></h4>                      
                         
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end single sidebar -->
                 
                  
                </aside>
                <!-- / end sidebar -->
             </div>
             
           </div>
         </div>
       </div>
     </div>
     
    
   </div>
 </section>
 
 
 
 
 
  <section id="mu-course-content">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
            
            
              <div class="col-md-6">
              
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                      
                       <div class="pdf-thumb-box"> <a href="#2013-Katalog">
                <div class="pdf-thumb-box-overlay"><span class="fa-stack fa-lg">
    <i class="fa fa-square-o fa-stack-2x pdf-thumb-square"></i>
    <i class="fa fa-eye fa-stack-1x pdf-thumb-eye"></i>
</span></div>
                <img class="img-responsive" src="images/sports.jpg" alt="2013 Genel Katalog">
            </a>

        </div>
                      
                        <div class="mu-title">
                          <h2>Sports</h2>
                          
                        


<p>

At Goodwill Christian College for Women the sporting infrastructure is second to none. Our Physical Education staff are highly committed to fitness and wellness. They have vast experience in drawing out the best sporting instincts in students.  Teamwork and Sportsmanship skills are at the heart of the programme.
</p>

                         
                        

                          
                        </div>
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
              
              
              
              
              <div class="col-md-6">
              
               <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                      
                       <div class="pdf-thumb-box"> <a href="#2013-Katalog">
                <div class="pdf-thumb-box-overlay"><span class="fa-stack fa-lg">
    <i class="fa fa-square-o fa-stack-2x pdf-thumb-square"></i>
    <i class="fa fa-eye fa-stack-1x pdf-thumb-eye"></i>
</span></div>
                <img class="img-responsive" src="images/lab.jpg" alt="2013 Genel Katalog">
            </a>

        </div>
                     
                       <div class="mu-title">
                          <h2>Laboratories</h2>
                          
                        


<p>Science is taught as an experimental subject at Goodwill Christian College for Women. Students spend most of their time in the labs actively experiencing science, rather than passively having it demonstrated to them by the teachers. In our three brand-new state-of-the-art laboratories for Chemistry, Biology and Microbiology, all students learn how to design and execute their own experiments, record their findings and draw up scientific reports. The labs are equipped with the latest technological devices in order to support cutting-edge experimentation. The college has a computer laboratory equipped with over 50 computers and internet connectivity. The Psychology lab is equipped with facilities for behavioral research. It has a large collection of Psychological tests and batteries.</p>

                         
                        

                          
                        </div>
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
               
             </div>
             
             
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
 

   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            
         </div>
       </div>
     </div>
   </div>
   
   
   
   
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
            
            
              <div class="col-md-6">
              
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details overlay">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                      
                     <div class="pdf-thumb-box"> <a href="#2013-Katalog">
                <div class="pdf-thumb-box-overlay"><span class="fa-stack fa-lg">
    <i class="fa fa-square-o fa-stack-2x pdf-thumb-square"></i>
    <i class="fa fa-eye fa-stack-1x pdf-thumb-eye"></i>
</span></div>
                <img class="img-responsive" src="images/seminar.jpg" alt="2013 Genel Katalog">
            </a>

        </div>
                      
                      
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
              
              
              
              
              <div class="col-md-6">
              
               <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                        
                       <div class="mu-title">
                          <h2>Seminar Hall</h2>
                          
                        


<p>

The Seminar Hall is fully equipped with all audio visual equipment's including multimedia projectors. It has enormous seating capacity. The seminar hall is used for delivering expert lectures, demonstration lectures and general purpose presentations. The Hall gives utmost importance to the function of dissemination of information amongst the students. The facility is also used for holding training workshops and Faculty Development Programmes.
</p>

                         
                        

                          
                        </div>
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
               
             </div>
             
             
           </div>
         </div>
       </div>
     </div>
   </div>
   
   
   
   
   
   
   
   <section id="mu-course-content">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
            
            
            
              
              
  
                
                
           
              
              
              
              
              
             
             
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
 
 

 <?php include('include/footer.php'); ?>
   
  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>  
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>   
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>  

  <!-- Custom js -->
  <script src="assets/js/custom.js"></script> 

  </body>
</html>