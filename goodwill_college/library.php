<?php include('include/session.php');?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Goodwill college |Library</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

    <!-- Font awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">   
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">          
    <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

    <!-- Main style sheet -->
    <link href="assets/css/style.css" rel="stylesheet">    

   
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>
 
<?php include('include/header.php');?>   
  
  <!-- Hero-area -->
		<div class="hero-area section">

			<!-- Backgound Image -->
			<div class="bg-image bg-parallax overlay" style="background-image:url(assets/img/counter-bg.jpg)"></div>
			<!-- /Backgound Image -->

			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1 text-center">
						<ul class="hero-area-tree">
							<li><a href="index.php">Home</a></li>
							<li>Library</li>
						</ul>
						<h1 class="white-text">Library</h1>

					</div>
				</div>
			</div>

		</div>
		<!-- /Hero-area -->

  

<!--<div class="parallax">dsaasdsda</div>
-->
 <!-- End breadcrumb -->
 <section id="mu-course-content">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
              <div class="col-md-9">
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                        <figure class="mu-latest-course-img">
                          <a href="#"><img src="images/library.jpg" alt="img"></a>
                         
                        </figure>
                        
                        <div class="mu-title">
                        <h2>Library</h2>
                              
                 <p><i class="fa fa-2x fa-quote-left" style="color:#F66"></i>&nbsp;<mark>Good reading and cultivation of arts is indeed the hall mark of good life</mark>&nbsp;<i class="fa fa-2x fa-quote-right" style="color:#F66"></i></p>
                 
                 <p>The college has a well equipped library with a robust collection of around <em>12545</em> books covering many disciplines in <strong>Commerce, Management , Science and Arts</strong>.</p>
                <p>The main aim of the library is to provide information services and e-resources to support the informational needs of the student community.</p><p> The fully computerized Goodwillian library is well equipped with modern facilities and resources in the form of books, printed and electronic journals, CD -Roms, On-line databases, Audio Video cassettes, Project reports etc.</p>
                <p> Open access system is being followed to access the books and journals. Our academic achievements are mainly effected through the provision of vast number of books and reference materials in the library.</p>
                    
                        </div>
                       
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
    
    
    
    
    
                  <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                       
                        <div class="mu-latest-course-single-content">
                         
                         <figure class="mu-latest-course-img">
                          <a href="#"><img src="images/librarybanner.jpg" alt="img"></a>
                         
                        </figure> 
                        



 <div class="table-responsive">
                            <table class="table">
                            <thead>
                              <tr>
                                <th> Newspapers </th>
                                <th> Magazines </th>
                                <th> Journals </th>
                                <th> E-books </th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td> 1. Times of India </td>
                                <td> 1. Competition Success </td>
                                <td> 1. Journal of genetics </td>
                                <td> 1. Microbiology </td>
                              </tr>
                              <tr>
                                <td> 2. Indian Express </td>
                                <td> 2. PratiyogitaDarpan (English)</td>
                                <td> 2. Current science </td>
                                <td> 2. Access to british library </td>
                              </tr>
                              <tr>
                                <td> 3. Daily Excelsior </td>
                                <td> 3. PratiyogitaDarpan (Hindi) </td>
                                <td> 3. Resonance </td>
                                <td>  </td>
                              </tr>
                              <tr>
                                <td> 4. GLimpses of Future </td>
                                <td> 4. Education Wish </td>
                                <td> 4. Journal of bioscience</td>
                                <td> </td>
                              </tr>
                              <tr>
                                <td>5. The Hindu </td>
                                <td> 5. Sports star </td>
                                <td> 5. Indian journal of economics </td>
                                <td>  </td>
                              </tr>
                              <tr>
                                <td> 6. DainikJagran </td>
                                <td> 6. Education Digest </td>
                                <td> 6. International journal of economic issues </td>
                                <td>  </td>
                              </tr>
                              <tr>
                                <td> 7. Prajavani </td>
                                <td> 7.Junior Science Refresher </td>
                                <td> 7. Prabhanbdam journal of management </td>
                                <td>  </td>
                              </tr>
                              <tr>
                                <td> 8. Dinadanti </td>
                                <td> 8. Readers's Digest </td>
                                <td> 8. Literary verse </td>
                                <td>  </td>
                              </tr>
                              <tr>
                                <td>  </td>
                                <td>9. Frontline </td>
                                <td> </td>
                                <td>  </td>
                              </tr>
                              <tr>
                                <td>  </td>
                                <td> 10. Outlook</td>
                                <td> </td>
                                <td>  </td>
                              </tr>
                              <tr>
                                <td>  </td>
                                <td> 11. India Today </td>
                                <td> </td>
                                <td>  </td>
                              </tr>
                            </tbody>
                          </table>
                          </div>
                      
                         
                        

                          
                        </div>
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
                      
                
                
              </div>
              <div class="col-md-3">
                <!-- start sidebar -->
                <aside class="mu-sidebar">
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Quick Links</h3>
                  <ul class="mu-sidebar-catg">
<li><a href="library.php">Library</a></li>  
              <li><a href="sports.php">Sports</a></li>   
    <li><a href="counsel.php">Counselling Cell</a></li>
    <li><a href="placement.php">Placement</a></li>
    <li><a href="cafeteria.php">Cafeteria</a></li>
     <li><a href="service.php">Service Programs</a></li>
     <li><a href="study.php">Dr. Ambedkar Study Centre</a></li>    
</ul>  
                  </div>
                  <!-- end single sidebar -->
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Latest News</h3>
                    <div class="mu-sidebar-popular-courses">
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/1.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Christmas Day</a></h4>                      
                        
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/2.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Annual day</a></h4>                      
                          
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/3.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Students showcasing talent</a></h4>                      
                         
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end single sidebar -->
                 
                  
                </aside>
                <!-- / end sidebar -->
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
 
 

 
 
 
 <?php include('include/footer.php'); ?>
   
  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>  
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>   
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>  

  <!-- Custom js -->
  <script src="assets/js/custom.js"></script> 

  </body>
</html>