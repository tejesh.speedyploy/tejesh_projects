<?php include('include/session.php');?> 
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Goodwill college |CIA</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

    <!-- Font awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">   
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">          
    <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

    <!-- Main style sheet -->
    <link href="assets/css/style.css" rel="stylesheet">    

   
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>

<?php include('include/header.php');?>   
  
  <!-- Hero-area -->
		<div class="hero-area section">

			<!-- Backgound Image -->
			<div class="bg-image bg-parallax overlay" style="background-image:url(assets/img/counter-bg.jpg)"></div>
			<!-- /Backgound Image -->

			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1 text-center">
						<ul class="hero-area-tree">
							<li><a href="index.php">Home</a></li>
							<li>RESULTS</li>
						</ul>
						<h1 class="white-text">RESULTS</h1>

					</div>
				</div>
			</div>

		</div>
		<!-- /Hero-area -->

  

<!--<div class="parallax">dsaasdsda</div>
-->
 <!-- End breadcrumb -->
 <section id="mu-course-content">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
              <div class="col-md-9">
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                        <figure class="mu-latest-course-img">
                          <a href="#"><img src="images/results.jpg" alt="img"></a>
                         
                        </figure>
                        
                        <div class="mu-title">
                        <h2>RESULTS</h2>
                        </div>
                       
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
              <div class="col-md-3">
                <!-- start sidebar -->
                <aside class="mu-sidebar">
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Quick Links</h3>
                  <ul class="mu-sidebar-catg">
 <li><a href="depart.php">Departments</a></li>
    <li><a href="faculty.php">Faculty Details</a></li>
    <li><a href="academic.php">Academic Calendar</a></li>
    <li><a href="result.php">RESULTS</a></li>     </ul>  
                  </div>
                  <!-- end single sidebar -->
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Latest News</h3>
                    <div class="mu-sidebar-popular-courses">
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/1.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Christmas Day</a></h4>                      
                        
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/2.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Annual day</a></h4>                      
                          
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/3.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Students showcasing talent</a></h4>                      
                         
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end single sidebar -->
                 
                  
                </aside>
                <!-- / end sidebar -->
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
 
 
 
  <section>
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
      
            <div class="row">
            
            
              <div class="col-md-12">
              
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                       
                       
                        <div class="mu-latest-course-single-content">
                         
                          
                        
<table class="table">
  <thead><th>COURSE</th>
  <th>RESULT</th>
 
  
  </thead>
  
  
  <tbody>
  
  <TR>
  
 <TD>BBM</TD>
 <TD>98%</TD>

 </TR>
 <TR>
 <TD>BCA</TD>
 <TD>100%</TD>
 
   <TR>
  
 <TD>BCOM</TD>
 <TD>96%</TD>

 </TR>
 
 
  <TR>
  
 <TD>BSC</TD>
 <TD>100%</TD>

 </TR>
 
 
  <TR>
  
 <TD>BA</TD>
 <TD>95%</TD>

 </TR>

  
 
  
  

 
</tbody>    
</table>  



                         
                        

                          
                        </div>
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
        
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
 
   <section>
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
            
            
              
              
              
    
    
    <div class="col-md-12">
              
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                       
                        <div class="mu-latest-course-single-content">
                        
                          
                        

<p>
<strong>Janani B. of BCA</strong> was awarded Rank in the 54th annual convocation of Bangalore University held during May/June 2018 for securing highest marks.
</p>
<p>

<strong>Amreen Taj of Bcom</strong> has secured cent percent in <em>Income Tax</em> in the Bangalore University Examination.
<strong>Saira Banu of BCA</strong> has secured cent percent in <em>VP,NALP</em> in th Bangalore University Examination.
</p>
<a href="#"><img src="images/result1.jpg" alt="img"></a>
                         
                        

                          
                        </div>
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
                        
              
              
             
             
             
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
 
 
 <section>
   <div class="container">
     <div class="row">
     
       <div class="col-md-12">
         <div class="mu-course-content-area">
         <div class="mu-title">
         
       <h2>  Exam Attendance & Revaluation Of Results:</h2>
<ul>
  <li> 1. <strong>75% ATTENDANCE</strong> IS THE STIPULATED REQUIREMENT OF BANGALORE UNIVERSITY TO QUALIFY FOR ALL UNIVERSITY SEMESTER EXAMINATIONS AND THOSE WHO DO NOT HAVE 75% ATTENDANCE WILL NOT BE ALLOWED TO DO THEIR EXAMINATIONS. MARKS ARE ALLOTTED FOR ATTENDANCE ACCORDING TO THE BANGALORE UNIVERSITY REGULATIONS FOR INTERNAL ASSESSMENT.</li><br>
     <li> 2. STUDENTS WHO HAVE SECURED <strong>15% OF MAXIMUM MARKS</strong> SHOULD APPLY FOR THE PHOTOCOPY OF THE ANSWER SCRIPT IMMEDIATELY AFTER THE ANNOUNCEMENT OF THE RESULT AND ENCLOSE IT FOR REVALUATION.</li><br>
     <li> 3. . ANSWER SCRIPT FOR RE-VALUATION CAN BE OBTAINED FROM BANGALORE ONE CENTRES</li><br>
   <li>   4. RE-VALUATION FEE AND PHOTOCOPY OF ANSWER SCRIPT AS PER BANGALORE UNIVERSITY NOTIFICATIONS</li><br>
    <li>  5. STUDENTS WHO DO NOT PAY THE EXAM FEE AND THOSE WITH SHORTAGE OF ATTENDANCE HAVE TO REPEAT THE CLASS.</li><br>
</ul>
         
       </div>  
         
         </div>
         </div>
         

         
         
         
         </div>
         </div>
         </section>
 
 
 

<?php include('include/footer.php'); ?>
  
  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>  
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>   
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>  

  <!-- Custom js -->
  <script src="assets/js/custom.js"></script> 

  </body>
</html>