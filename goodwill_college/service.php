<?php include('include/session.php');?> 
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Goodwill college |Service Programs</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

    <!-- Font awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">   
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">          
    <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

    <!-- Main style sheet -->
    <link href="assets/css/style.css" rel="stylesheet">    

   
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>

<?php include('include/header.php');?>   
  
  <!-- Hero-area -->
		<div class="hero-area section">

			<!-- Backgound Image -->
			<div class="bg-image bg-parallax overlay" style="background-image:url(assets/img/counter-bg.jpg)"></div>
			<!-- /Backgound Image -->

			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1 text-center">
						<ul class="hero-area-tree">
							<li><a href="index.php">Home</a></li>
							<li>Service Programs</li>
						</ul>
						<h1 class="white-text">Service Programs</h1>

					</div>
				</div>
			</div>

		</div>
		<!-- /Hero-area -->

  

<!--<div class="parallax">dsaasdsda</div>
-->
 <!-- End breadcrumb -->
 
 <section id="mu-course-content">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
              <div class="col-md-9">
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                        <figure class="mu-latest-course-img">
                          <a href="#"><img src="images/nss1.jpg" alt="img"></a>
                         
                        </figure>
                        
                        <div class="mu-title">
                        <h2>Civil defence Programs</h2>
                              


                    
                        </div>
                       
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
              <div class="col-md-3">
                <!-- start sidebar -->
                <aside class="mu-sidebar">
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Quick Links</h3>
                  <ul class="mu-sidebar-catg">
<li><a href="library.php">Library</a></li>  
              <li><a href="sports.php">Sports</a></li>   
    <li><a href="counsel.php">Counselling Cell</a></li>
    <li><a href="placement.php">Placement</a></li>
    <li><a href="cafeteria.php">Cafeteria</a></li>
     <li><a href="service.php">Service Programs</a></li>
     <li><a href="study.php">Dr. Ambedkar Study Centre</a></li>    
</ul>  
                  </div>
                  <!-- end single sidebar -->
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Latest News</h3>
                    <div class="mu-sidebar-popular-courses">
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/1.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Christmas Day</a></h4>                      
                        
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/2.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Annual day</a></h4>                      
                          
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/3.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Students showcasing talent</a></h4>                      
                         
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end single sidebar -->
                 
                  
                </aside>
                <!-- / end sidebar -->
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
 
  <section id="mu-course-content">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
            
            
              <div class="col-md-6">
              
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                      
                       <div class="pdf-thumb-box"> <a href="#2013-Katalog">
                <div class="pdf-thumb-box-overlay"><span class="fa-stack fa-lg">
    <i class="fa fa-square-o fa-stack-2x pdf-thumb-square"></i>
    <i class="fa fa-eye fa-stack-1x pdf-thumb-eye"></i>
</span></div>
                <img class="img-responsive" src="images/nss2.jpg" alt="2013 Genel Katalog">
            </a>

        </div>
                      
                        <div class="mu-title">
                          <h2>NSS</h2>
                          
                        


<p>
<strong>(MOTTO- NOT ME BUT YOU)</strong></p>
<p>
The National Service Scheme (<strong>NSS)</strong> is an Indian government-sponsored public service program conducted by the <em>Department of Youth Affairs and Sports</em> of the Government of India.</p>
<p>The NSS was launched in <strong>INDIA</strong> on <em>24th September 1969</em>, to mark the birth centenary year of "<strong>Father of our Nation</strong>", <em>Mohandas Karamchand Gandhi</em>. NSS unit is started in our college from 2004, since then as per regulation of Bangalore University our college sub – unit actively involving in various programmes like - Annual NSS Camps, Cleaning, Afforestation, Blood donation camp, Awareness programme, Rallies etc</p>

</p>

                         
                        

                          
                        </div>
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
              
              
              
              
              <div class="col-md-6">
              
               <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                      
                       <div class="pdf-thumb-box"> <a href="#2013-Katalog">
                <div class="pdf-thumb-box-overlay"><span class="fa-stack fa-lg">
    <i class="fa fa-square-o fa-stack-2x pdf-thumb-square"></i>
    <i class="fa fa-eye fa-stack-1x pdf-thumb-eye"></i>
</span></div>
                <img class="img-responsive" src="images/rrc.jpg" alt="2013 Genel Katalog">
            </a>

        </div>
                     
                       <div class="mu-title">
                          <h2>Red Ribbon Club</h2>
                          
                        


<p>It was established about four years ago (2009) by <strong>NACO</strong> (National AIDS Control Organization). The club is run by student volunteers and aims to serve various purposes like awareness against <em>drug abuse, substance abuse, and anti-AIDS campaign, inspires blood donation</em> etc. Another chief purpose is the empowerment and overall development of youth. '<strong>Getting to zero</strong>', in terms of HIV prevalence is our ultimate goal. This club is another feather of NSS cap, enforced from 2012.</p>

                         
                        

                          
                        </div>
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
               
             </div>
             
             
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>

 
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
            
            
              <div class="col-md-6">
              
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                      
                       <div class="pdf-thumb-box"> <a href="#2013-Katalog">
                <div class="pdf-thumb-box-overlay"><span class="fa-stack fa-lg">
    <i class="fa fa-square-o fa-stack-2x pdf-thumb-square"></i>
    <i class="fa fa-eye fa-stack-1x pdf-thumb-eye"></i>
</span></div>
                <img class="img-responsive" src="images/kcd.jpg" alt="2013 Genel Katalog">
            </a>

        </div>
                      
                      
                        <div class="mu-title">
                          <h2>Civil Defence</h2>
                          
                        


<p>



The Civil Defence is an Indian government (<strong>MHA</strong>) initiative designed to deal with immediate emergency conditions, protect the public and restore vital services and facilities that have been destroyed or damaged by disaster. We are joined the organisation in 2012 with 125 members.</p><p> Every year fresh members are joining with us to serve the society.</p>
<p> Every year we take part in <em>Independence</em> Day and <em>Republic</em> Day parade which held at Manikshaw parade ground, Bengaluru and constantly we are winning <em>second</em> place in group</p>
<p><strong>Dr.M.S. Gayathri</strong> Divisional Warden won <em>Chief Minister gold meda</em>l (2014-15), received on April 20, 2016. Dr. G.Parameswara Honourable Home Minister of Karnataka at Mudkul ground, Bengaluru. </p>

</p>

                         
                        

                          
                        </div>
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
              
              
              
              
              <div class="col-md-6">
              
               <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                      
                      
                       <div class="pdf-thumb-box"> <a href="#2013-Katalog">
                <div class="pdf-thumb-box-overlay"><span class="fa-stack fa-lg">
    <i class="fa fa-square-o fa-stack-2x pdf-thumb-square"></i>
    <i class="fa fa-eye fa-stack-1x pdf-thumb-eye"></i>
</span></div>
                <img class="img-responsive" src="images/youth.jpg" alt="2013 Genel Katalog">
            </a>

        </div>
                      
                      
                       <div class="mu-title">
                          <h2>Youth Red Cross Wing</h2>
                          
                        


<p>The Youth Red Cross unit of our college started working actively from 2014. </p>
<p>The unit conducts a mega blood donation campaign every year to reiterate its core belief that donating blood is tantamount to giving life. The YRC also conducts crash courses in first aid for the benefit of our students as well as the staff.<p> It conducts medical check-up camps for staff and students</p>

                         
                        

                          
                        </div>
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
               
             </div>
             
             
           </div>
         </div>
       </div>
     </div>
   </div>
 
 
   <?php include('include/footer.php'); ?>
     
  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>  
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>   
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>  

  <!-- Custom js -->
  <script src="assets/js/custom.js"></script> 

  </body>
</html>