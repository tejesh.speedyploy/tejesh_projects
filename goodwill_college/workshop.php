<?php include('include/session.php');?> 
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Goodwill college |Workshop & Seminar</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">

    <!-- Font awesome -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">   
    <!-- Slick slider -->
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">          
    <!-- Theme color -->
    <link id="switcher" href="assets/css/theme-color/default-theme.css" rel="stylesheet">

    <!-- Main style sheet -->
    <link href="assets/css/style.css" rel="stylesheet">    

   
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,700' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>

<?php include('include/header.php');?>   
  
  <!-- Hero-area -->
		<div class="hero-area section">

			<!-- Backgound Image -->
			<div class="bg-image bg-parallax overlay" style="background-image:url(assets/img/counter-bg.jpg)"></div>
			<!-- /Backgound Image -->

			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1 text-center">
						<ul class="hero-area-tree">
							<li><a href="index.php">Home</a></li>
							<li>Workshop & Seminar</li>
						</ul>
						<h2 class="white-text">Workshop & Seminar</h2>

					</div>
				</div>
			</div>

		</div>
		<!-- /Hero-area -->

  

<!--<div class="parallax">dsaasdsda</div>
-->
 <!-- End breadcrumb -->
 <section id="mu-course-content">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
            <div class="row">
              <div class="col-md-9">
                <!-- start course content container -->
                <div class="mu-course-container mu-course-details">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="mu-latest-course-single">
                        <figure class="mu-latest-course-img">
                          <a href="#"><img src="images/seminar.jpg" alt="img"></a>
                         
                        </figure>
                        
                        <div class="mu-title">
                      <h4> IMMERSIVE EXPOSURE TO LEARN THROUGH WORKSHOPS AND SEMINARS ORGANISED BY OUR COLLEGE FOR 2018-2019</h4>
                      
                      <p>Various <em>Guest lecture, seminar and conferences</em> were conducted by various dept to help students stay informed on the basics, and familiarize them with various topics. <strong>Workshops and seminars</strong> widen the student's acquaintance to their subjects for further learning.  These kind of exposure helps students befriend their research work in future.</p>
                        </div>
                       
                      </div> 
                    </div>                                   
                  </div>
                </div>
                <!-- end course content container -->
             
                
                
              </div>
              <div class="col-md-3">
                <!-- start sidebar -->
                <aside class="mu-sidebar">
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Quick Links</h3>
                  <ul class="mu-sidebar-catg">
<li><a href="depart.php">Departments</a></li>
    <li><a href="faculty.php">Faculty Details</a></li>
    <li><a href="academic.php">Academic Calendar</a></li>
    <li><a href="result.php">RESULTS</a></li>     
</ul>  
                  </div>
                  <!-- end single sidebar -->
                  <!-- start single sidebar -->
                  <div class="mu-single-sidebar">
                    <h3>Latest News</h3>
                    <div class="mu-sidebar-popular-courses">
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/1.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Christmas Day</a></h4>                      
                        
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/2.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Annual day</a></h4>                      
                          
                        </div>
                      </div>
                      <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object" src="assets/img/courses/3.jpg" alt="img">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading"><a href="#">Students showcasing talent</a></h4>                      
                         
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end single sidebar -->
                 
                  
                </aside>
                <!-- / end sidebar -->
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 
 
 
 
  <section>
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="mu-course-content-area">
         
          <div class="mu-latest-course-single">
           <div class="mu-title">
         
        <h2> Department Of Arts</h2>
        
        <div class="pdf-thumb-box"> <a href="#2013-Katalog">
                <div class="pdf-thumb-box-overlay"><span class="fa-stack fa-lg">
    <i class="fa fa-square-o fa-stack-2x pdf-thumb-square"></i>
    <i class="fa fa-eye fa-stack-1x pdf-thumb-eye"></i>
</span></div>
                <img class="img-responsive" src="images/artsdept.jpg" alt="2013 Genel Katalog">
            </a>

        </div>
      
       
           <h4>	Department Of Hindi</h4>
            <img src="images/hindidivas.jpg">
	<p>On <em>14/03/2018</em> one day seminar by organized by Dept of Hindi on "<em>FILMO MEI  HINDI SAHITHYAKAYOGDAM</em>". Mr. Vinay Kumar Yadav , H.O.D of Hindi, Bishop Cotton Women's Christian College was the resources person  for the day. No. of participants who attended the seminar were <em>155</em>.</p>
    <br>
	<h4>Department Of Kannada</h4>
    <img src="images/kannada.jpg">
	<p>On <em>29/9/2017</em> Guest lecture was organized by Dept. of Kannada on "<em>BHAASHE,SAAMAAJIKA JAVAABDHAARI MATTHU PAREEKSHE</em>". Dr. Manjunath, Dept. of Kannada from Maharani Arts & Commerce College was the speaker for the day. No. of participants who attended the seminar were <em>200</em>.</p>
    <br>
	<h4>Department Of Tamil</h4>
	<p>On <em>06/3/2018</em> Guest lecture was organized by Dept. of Tamil on "<em>TAMIL ILLAKIYATHIL PENNIYAN</em>". Prof Dr. G. Vanangamudi, Chairman & Correspondent of SATHYAM College of Arts & Science College, Chengam- Tamilnadu. was the speaker for the day. No. of participants who attended the seminar were <em>120</em>.</p>
<br>

	<h4>Department OF Psychology</h4>
    <img src="images/psychology.jpg">
	<p>On <em>22/9/2017</em> Guest lecture was organized by Dept. of Psychology on "<em>A BALANCE OF EMOTIONS</em>". Mrs. Swarnalathaiyer, MD CEO OF Seshan's Academy Infinity-Bangalore. was the speaker for the day. No. of participants who attended the seminar were <em>80</em>.</p>
    <br>
	<h4>Department Of Journalism</h4>
     <img src="images/journalism.jpg">
	<p>One day workshop was organized by Dept. of Journalism in association with Deccan Herald  on "<em>EFFECTIVE STUDY SKILLS, MEMORY TECHNIQUES</em>", "<em>PUBLIC SPEAKING SKILLS</em>", "<em>THE POWER OF CONCENTRATION</em>". Mr. Mallikarjuna K H, Director-Diginet Online India Pvt Ltd, Bangalore  was the speaker for the day. No. of participants who attended the seminar were <em>80</em>.</p>
    <br>
	<h4>Department Of English</h4>
     <img src="images/english.jpg">
	<p>On <em>07/3/2019</em> Guest lecture was organized by Dept. of English on "<em>FILMS AND LITERATURE</em>". Mr.Sudharshan, Dept of English, ArcharyaPatashala College-Bangalore Was the speaker for the day. No. of participants who attended the seminar were <em>80</em>.


<hr style="border:1px solid #ddd">



 <h2> Department Of Commerce</h2>

<div class="pdf-thumb-box"> <a href="#2013-Katalog">
                <div class="pdf-thumb-box-overlay"><span class="fa-stack fa-lg">
    <i class="fa fa-square-o fa-stack-2x pdf-thumb-square"></i>
    <i class="fa fa-eye fa-stack-1x pdf-thumb-eye"></i>
</span></div>
                <img class="img-responsive" src="images/commerce.jpg" alt="2013 Genel Katalog">
            </a>

        </div>

	<p>On <em>16/10/2018</em> one day workshop was organized by Dept. of Commerce on "<em>Goods and service act</em>". Mrs. Ruheena Ahmed,Director Cocoon's Training Solutions, Bangalore was the speaker for the day. No. of participants who attended the seminar were 150.</p>
	<p>On <em>16/10/2018</em> one day workshop was organized by Dept. of Commerce on "<em>One Nation one Tax</em>". Mrs. Ruheena Ahmed, Director Cocoon's Training Solutions, Bangalore was the speaker for the day. No. of participants who attended the seminar were 142.</p>


<hr style="border:1px solid #ddd">

<h2>Department Of Science</h2>
<div class="pdf-thumb-box"> <a href="#2013-Katalog">
                <div class="pdf-thumb-box-overlay"><span class="fa-stack fa-lg">
    <i class="fa fa-square-o fa-stack-2x pdf-thumb-square"></i>
    <i class="fa fa-eye fa-stack-1x pdf-thumb-eye"></i>
</span></div>
                <img class="img-responsive" src="images/science.jpg" alt="2013 Genel Katalog">
            </a>

        </div>

<h4>Department Of Botany</h4>
<p>	On <em>21/3/2018</em> Guest lecture was organized by Dept. of Botany  on "<em>GREEN AUDITING</em>". Dr. Ramachandra, Scientist from Indian Institute of Science, Bangalore.Was the speaker for the day. No. of participants who attended the seminar were 60.</p><br>
	<h4>Department Of Chemistry</h4>
	<p>On <em>21/3/2018</em> Guest lecture was organized by Dept. of Chemistry  on "<em>Changing Trends in Chemistry</em>". Dr.N.C.Subramanyam, Associate Prof. of Chemistry in Archarya pathasala college of Arts and Science, Bangalore. Was the speaker for the day. No. of participants who attended the seminar were 60.</p><br>
	<h4>Department Of Microbiology</h4>
	<p>On <em>21/3/2018</em> Guest lecture was organized by Dept. of Microbiology on "<em>Importance of Microbiology to the society</em>". Dr. Bhuvana, Karnataka Antibiotics Peenaya, Bangalore. Was the speaker for the day. No. of participants who attended the seminar were 60.</p>

</div>

</div>
           
         </div>
       </div>
     </div>
   </div>
 </section>
 
 
 

 <?php include('include/footer.php'); ?>
  
  <!-- jQuery library -->
  <script src="assets/js/jquery.min.js"></script>  
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.js"></script>   
  <!-- Slick slider -->
  <script type="text/javascript" src="assets/js/slick.js"></script>
  <!-- Counter -->
  <script type="text/javascript" src="assets/js/waypoints.js"></script>
  <script type="text/javascript" src="assets/js/jquery.counterup.js"></script>  

  <!-- Custom js -->
  <script src="assets/js/custom.js"></script> 

  </body>
</html>